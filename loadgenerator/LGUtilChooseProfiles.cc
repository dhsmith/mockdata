/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "LGUtil.hh"
#include "GetterContext.hh"

void LGUtil::ChooseProfiles(GetterContext *gc, const LGMessages::req_s &req, const struct Profiles::fp_s **fpp, const struct Profiles::ap_s **app) {
  // match a file profile
  *fpp = 0;
  *app = 0;
  int pidx=0;
  for(const auto &fpx: gc->profiles_.getFileProfiles()) {
    GetterContext::pcre2_regex_holder_s &reginfo = gc->profile_pcre2_regexs_[pidx++];
    pcre2_match_data *md = pcre2_match_data_create_from_pattern(reginfo.re, 0);
//std::cerr << "In chooseprofiles doing match against " << req.filename << " for pattern " << fpx.reg << std::endl;
    int mrc = pcre2_match(reginfo.re, (PCRE2_SPTR8)req.filename.c_str(), req.filename.length(), 0, 0, md, 0);
    pcre2_match_data_free(md);
    if (mrc >= 0) {
//std::cerr << "matched" << std::endl;
      *fpp = &fpx;
      break;
    }
  }
  if (!*fpp) {
    throw std::string("Could not match a file profile");
  }
  const struct Profiles::fp_s &fp = **fpp;

  std::unique_ptr<float[]> csum(new float[fp.matches.size()]);
  float tot = 0.0, sum = 0.0;
  for(const auto &m: fp.matches) {
    tot += m.prob;
  }
  for(size_t i=0;i<fp.matches.size();i++) {
    sum += fp.matches[i].prob;
    csum[i] = sum / tot;
  }
  float r = LGUtil::randdouble();
  std::string apname;
  for(size_t i=0;i<fp.matches.size();i++) {
//std::cerr << "Comparing r=" << r << " and csum[" << i << "]=" << csum[i] << " for fp.matches[i].ap=" << fp.matches[i].ap << std::endl;
    if (r<=csum[i]) {
      apname = fp.matches[i].ap;
      break;
    }
  }
  if (apname.empty()) {
    throw std::string("The file profile " + fp.name + " did not include access profile names");
  }
  for(const auto &apx: gc->profiles_.getAccessProfiles()) {
//std::cerr << " Comparing apx.name=" << apx.name << " against apname=" << apname << std::endl;
    if (apx.name == apname) {
      *app = &apx;
      break;
    }
  }
  if (!*app) {
    throw std::string("The file access profile named " + apname + " matched from file profile " + fp.name + " was not found");
  }
}
