#
# This module detects if yaml-cpp is installed and determines where the
# include files and libraries are.
#
# This code sets the following variables:
#
# YAML_CPP_INCLUDE_DIR = include dir to be used when using the yaml-cpp library
# YAML_CPP_LIBRARIES   = full path to the yaml-cpp library
# YAML_CPP_FOUND       = set to true if yaml-cpp was found successfully
#

# -----------------------------------------------------
# YAML_CPP Libraries
# -----------------------------------------------------
find_library(YAML_CPP_LIBRARIES
  NAMES libyaml-cpp.a
  DOC "The yaml-cpp static library"
)

# -----------------------------------------------------
# YAML_CPP Include Directories
# -----------------------------------------------------
find_path(YAML_CPP_INCLUDE_DIR 
  NAMES yaml-cpp/yaml.h
  DOC "The yaml-cpp include directory"
)
if(YAML_CPP_INCLUDE_DIR)
  message(STATUS "yamp-cpp includes found in ${YAML_CPP_INCLUDE_DIR}")
endif()

# -----------------------------------------------------
# handle the QUIETLY and REQUIRED arguments and set YAML_CPP_FOUND to TRUE if
# all listed variables are TRUE
# -----------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(yaml_cpp DEFAULT_MSG YAML_CPP_LIBRARIES YAML_CPP_INCLUDE_DIR)
mark_as_advanced(YAML_CPP_LIBRARIES YAML_CPP_INCLUDE_DIR)
