/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "LGUtil.hh"
#include "LGMessages.hh"
#include "common/PseudoDataGenerator.hh"

#include <cstdint>
#include <memory>
#include <mutex>

#include <XrdCl/XrdClFileSystem.hh>
#include <XrdCl/XrdClFile.hh>
#include <XrdCl/XrdClURL.hh>
#include <XrdCl/XrdClEnv.hh>
#include <XrdCl/XrdClDefaultEnv.hh>
#include <XrdCl/XrdClConstants.hh>
#include <XrdCl/XrdClXRootDResponses.hh>

// forward declaration for the handler
class MyResponseHandler;
struct outstandingIo_s {
  enum Type { OPEN, READ, VREAD, CLOSE, STAT };
  outstandingIo_s(Type t, LGMessages::wake_clientllop_s *w, std::shared_ptr<XrdCl::File> &fp, const size_t ndata);
  ~outstandingIo_s();
  void DeleteHandler();

  void incReads() {
    std::lock_guard<std::mutex> lck(countLck);
    ++globalIoOutstandingReads;
  }

  void decReads() {
    std::lock_guard<std::mutex> lck(countLck);
    --globalIoOutstandingReads;
  }

  static size_t getReads() {
    std::lock_guard<std::mutex> lck(countLck);
    return globalIoOutstandingReads;
  }

  static size_t getReadBytes(uint64_t *ms) {
    std::lock_guard<std::mutex> lck(countLck);
    if (ms) {
      struct timespec ts;
      clock_gettime(CLOCK_MONOTONIC,&ts);
      *ms = ts.tv_sec * 1000 + ts.tv_nsec / 1000000;
    }
    return globalIoReadBytes;
  }

  void setChunkBytesRead(const size_t nread) {
    XrdClChunkBytesRead = nread;
    std::lock_guard<std::mutex> lck(countLck);
    globalIoReadBytes += nread;
  }

  uint64_t ioid;
  uint8_t *datap() const;

  bool usingmockoss;

  std::mutex outstandingDoneMtx;
  std::mutex outstandingAttachedMtx;
  MyResponseHandler *handler;
  int done;
  XrdCl::XRootDStatus XrdClStatus;
  std::string IOExtraErrorMessage;
  uint64_t statGetSize;
  uint32_t XrdClChunkBytesRead;
//  XrdCl::ChunkList chunks;
  Type type;
  uint64_t offset;
  uint64_t len;
  struct timespec iostart,ioend;
  std::vector<uint8_t> seed;
  LGMessages::wake_clientllop_s *w;

  static std::mutex countLck;
  static size_t globalIoOutstandingReads;
  static size_t globalIoReadBytes;

private:
  outstandingIo_s(const outstandingIo_s& ) = delete; // non copy constructable
  outstandingIo_s& operator=(const outstandingIo_s&) = delete; // non copyable
};

class MyResponseHandler : public XrdCl::ResponseHandler {
public:
  MyResponseHandler(struct outstandingIo_s *io, std::shared_ptr<XrdCl::File> &fp, const size_t ndata) :
        io(io), done(false), fp(fp), dp(new uint8_t[ndata]) { clock_gettime(CLOCK_MONOTONIC,&io->iostart); }
  virtual ~MyResponseHandler() { }
  virtual void HandleResponse( XrdCl::XRootDStatus *status,
                               XrdCl::AnyObject    *response ) {
    std::lock_guard<std::mutex> lck0(handlerMtx);
    done = 1;
    if (!io) {
      delete status;
      delete response;
      delete this;
      return;
    }
    clock_gettime(CLOCK_MONOTONIC,&io->ioend);
    io->XrdClStatus = XrdCl::XRootDStatus(*status);
    delete status;
    if (io->type == outstandingIo_s::Type::STAT) {
      if (io->XrdClStatus.IsOK()) {
       XrdCl::StatInfo *statInfo;
        response->Get(statInfo);
        response->Set((int*)0);
        delete response;
        io->statGetSize = statInfo->GetSize();
        delete statInfo;
      }
    }
    if (io->type == outstandingIo_s::Type::READ) {
      io->decReads();
      if (io->XrdClStatus.IsOK()) {
        XrdCl::ChunkInfo *chunkInfo = 0;
        response->Get(chunkInfo);
        response->Set((int*)0);
        delete response;
        io->setChunkBytesRead(chunkInfo->length);
        delete chunkInfo;
        const bool badlen = (io->XrdClChunkBytesRead != io->len);
        if (io->usingmockoss) {
          // check bytes
          uint8_t *buf2 = new uint8_t[io->XrdClChunkBytesRead];
          genData(buf2, io->XrdClChunkBytesRead, io->offset, &io->seed[0]);
          if (memcmp(datap(), buf2, io->XrdClChunkBytesRead)) {
            io->XrdClStatus = XrdCl::XRootDStatus(XrdCl::stError, XrdCl::errDataError);
            io->IOExtraErrorMessage = "Corruption during scalar read: " + LGUtil::ByteMismatchInfo(io->offset, datap(), buf2, io->XrdClChunkBytesRead);
          }
          delete[] buf2;
        }
        if (badlen) {
          io->XrdClStatus = XrdCl::XRootDStatus(XrdCl::stError, XrdCl::errDataError);
          const std::string msg = "Wrong length of data returned by read: bytes-read " +
                 std::to_string(io->XrdClChunkBytesRead) + " expected " + std::to_string(io->len) +
                 " offset=" + std::to_string(io->offset);
          if (!io->IOExtraErrorMessage.empty()) {
            io->IOExtraErrorMessage = msg + ": " + io->IOExtraErrorMessage;
          } else {
            io->IOExtraErrorMessage = msg;
          }
        }
      }
    }
    if (io->type == outstandingIo_s::Type::VREAD) {
      io->decReads();
      if (io->XrdClStatus.IsOK()) {
        XrdCl::VectorReadInfo *vrInfo = 0;
        response->Get(vrInfo);
        response->Set((int*)0);
        delete response;
        io->setChunkBytesRead(vrInfo->GetSize());
        size_t chunksum=0;
        for(const auto c: vrInfo->GetChunks()) {
          chunksum += c.length;
        }
        const bool badlen = (io->XrdClChunkBytesRead != io->len || chunksum != io->len);
        if (io->usingmockoss) {
          // check bytes
          uint8_t *buf2 = new uint8_t[chunksum];
          size_t pidx = 0, ccount = 0;
          for(const auto c: vrInfo->GetChunks()) {
            genData(&buf2[pidx], c.length, c.offset, &io->seed[0]);
            ++ccount;
            if (memcmp(c.buffer, &buf2[pidx], c.length)) {
              io->XrdClStatus = XrdCl::XRootDStatus(XrdCl::stError, XrdCl::errDataError);
              io->IOExtraErrorMessage = "Corruption during vector read chunk index " +
                   std::to_string(ccount) + "/" + std::to_string(vrInfo->GetChunks().size()) +
                   ": " + LGUtil::ByteMismatchInfo(c.offset, c.buffer, &buf2[pidx], c.length);
            }
            pidx += c.length;
          }
          delete[] buf2;
        }
        if (badlen) {
          io->XrdClStatus = XrdCl::XRootDStatus(XrdCl::stError, XrdCl::errDataError);
          const std::string msg = "Wrong length of data returned by vector read: overall read byte-sum " +
               std::to_string(io->XrdClChunkBytesRead) + " returned chunk byte-sum " + std::to_string(chunksum) +
               " expected " + std::to_string(io->len);
          if (!io->IOExtraErrorMessage.empty()) {
            io->IOExtraErrorMessage = msg + ": " + io->IOExtraErrorMessage;
          } else {
            io->IOExtraErrorMessage = msg;
          }
        }
        delete vrInfo;
      } else {
        // dump chunks
//        for(auto &c: io->chunks) {
//          std::cerr << "chunk offset=" << c.offset << " len " << c.length << std::endl;
//        }
      }
    }
//std::cerr << "Got a callback io=" << io << " going to get outstandintmtx" << std::endl;
    std::unique_lock<std::mutex> lck(io->outstandingDoneMtx);
//std::cerr << "Got a callback io=" << io << " type=" << io->type << std::endl;
    io->done = 1;
    lck.unlock();
    // signal waitingMtx
    LGMessages::wake_clientllop_s *w = io->w;
    std::lock_guard<std::mutex> lck2(w->waitingMtx);
    w->event = true;
    w->waitingCond.notify_all();
  }
  void DetachIO() {
    bool destroy = false;
    {
      std::lock_guard<std::mutex> lck(handlerMtx);
      io = 0;
      if (done) destroy = true;
    }
    if (destroy) delete this;
  }

  uint8_t *datap() const {
    return dp.get();
  }

private:
  struct outstandingIo_s *io;
  bool done;
  std::shared_ptr<XrdCl::File> fp;
  std::unique_ptr<uint8_t[]> dp;
  std::mutex handlerMtx;
};

