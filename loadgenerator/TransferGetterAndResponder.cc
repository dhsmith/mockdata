/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "LGUtil.hh"
#include "TransferGetterAndResponder.hh"
#include "LGMessages.hh"
#include "TimeMacros.hh"

#include "common/MDLog.hh"

#include <thread>

#include <XrdCl/XrdClEnv.hh>
#include <XrdCl/XrdClConstants.hh>
#include <XrdCl/XrdClDefaultEnv.hh>

#define MAX_STARTDELAY 60
#define MAX_UNASSIGNED_OFFER_AGE 60

void TransferGetterAndResponder::getterReadWatcher(GetterContext *gc, struct readinfo_s *r, LGMessages::wake_clientllop_s *winf) {
  while(!r->finish && r->rw->Read(&r->reply)) {
    // waitingMtx before readinfo_s mutex
//std::cerr << "Did Read in getterReadWatcher" << std::endl;
    std::unique_lock<std::mutex> lck(winf->waitingMtx);
    std::unique_lock<std::mutex> lck2(r->mtx);
    r->rret = true;
    r->newres = true;
    winf->event = true;
    winf->waitingCond.notify_all();
//std::cerr << "Did Notify in getterReadWatcher" << std::endl;
    lck.unlock();
    while (r->newres) {
      r->cv.wait(lck2);
    }
  }
  // waitingMtex before readinfo_s mutex
  std::lock_guard<std::mutex> lck(winf->waitingMtx);
  std::lock_guard<std::mutex> lck2(r->mtx);
  r->rret = false;
  r->newres = true;
  winf->event = true;
  winf->waitingCond.notify_all();
}

void TransferGetterAndResponder::transferGetterProcessRead(GetterContext *gc, TransferReply &reply, std::map<uint64_t, LGMessages::req_s> &offered, grpc::ClientReaderWriter<TransferReport, TransferReply> *rw, bool &finish) {
  if (reply.status() == TransferReply::OFFER) {

    const float sd = reply.startdelay();
    if (sd > MAX_STARTDELAY) {
      MDLog::Log(MDLog::WARN, "Skipping offer because the start delay is too long, id=" + std::to_string(reply.id()) + " sd=" + std::to_string(sd));
      return;
    }

    LGMessages::req_s r{};
    r.id = reply.id();
    r.fsize = reply.fsize();
    r.filename = reply.filename();
    clock_gettime(CLOCK_MONOTONIC, &r.timeoffered);

    // startdelay is a float and may be negative
    r.desiredstarttime = r.timeoffered;
    if (sd>0) {
      ts_timeraddfloat(&r.desiredstarttime, sd);
    }

    try {
      LGUtil::ChooseProfiles(gc, r, &r.fileprofile, &r.accessprofile);
    } catch(const std::string &reason) {
      MDLog::Log(MDLog::WARN, "Could not choose profiles for offer id=" + std::to_string(r.id) + " filename=" + r.filename + " reason:" + reason);
      return;
    }

    offered[r.id] = r;
    {
      std::string msg = "Got an offer for id=" + std::to_string(r.id) + " filename=" + r.filename + " desiredstarttime.tv_sec=" + std::to_string(r.desiredstarttime.tv_sec) + " startdelay=" + std::to_string(reply.startdelay());
      MDLog::Log(MDLog::INFO, msg);
    }

    float duration = 0.0f;
    if (r.accessprofile->fDuration != 0.0) {
      duration = r.accessprofile->fDuration;
    } else {
      if (gc->usingmockoss_ || r.fsize) {
        // we know the amount of data that will be read
        duration = (r.accessprofile->fDurationPerMByte * r.fsize * r.accessprofile->fBytesRead/1E6);
      }
    }

    TransferReport report;
    report.set_id(r.id);

    LGMessages::FillFileResult_Claim(report, r.id, duration, r.fsize * r.accessprofile->fBytesRead, r.fileprofile, r.accessprofile);
    LGMessages::FillLGInfo(report, gc);
    report.set_startdelay(sd);
    report.set_status(TransferReport::CLAIM);
    rw->Write(report);
  }
  if (reply.status() == TransferReply::TERMINATE) {
    finish = true;
  }
  if (reply.status() == TransferReply::ASSIGNED) {
    {
      std::string msg = "Got an assigned for id=" + std::to_string(reply.id());
      MDLog::Log(MDLog::INFO, msg);
    }
    if (offered.find(reply.id()) != offered.end()) {
      {
//std::cerr << "Going to put request in assigned in transferGetterProcessRead" << std::endl;
        {
          std::lock_guard<std::mutex> lck(gc->assignedMtx_);
          gc->assigned_[reply.id()] = offered[reply.id()];
          gc->assignedInserted_ = true;
        }
//std::cerr << "X1" << std::endl;
        gc->assignedInsertedCB_.CallbackInterested();
//std::cerr << "Put request in assigned in transferGetterProcessRead" << std::endl;
      }
      offered.erase(reply.id());
    }
  }
}

void TransferGetterAndResponder::Run(GetterContext *gc) {
  ClientContext context;

  std::unique_ptr<grpc::ClientReaderWriter<TransferReport, TransferReply> > rw(gc->stub_->GetTransfers(&context));

  {
    TransferReport report;
    report.set_status(TransferReport::JOINING);

    XrdCl::Env *env = XrdCl::DefaultEnv::GetEnv();
    int streams = XrdCl::DefaultSubStreamsPerChannel;
    env->GetInt( "SubStreamsPerChannel", streams );
    if( streams < 1 ) streams = 1;
    LGMessages::FillFileResult_Joining(report, streams);
    LGMessages::FillLGInfo(report, gc);

    rw->Write(report);
  }

  LGMessages::wake_clientllop_s waitinfo{};

  // new inserts for terminated requests to trigger waitinfo
  Callback::HandlerFn h1 = [&waitinfo]() {
//std::cerr << "In transferGetterAndResponder handler func before lock" << std::endl;
    std::lock_guard<std::mutex> lck(waitinfo.waitingMtx);
//std::cerr << "In transferGetterAndResponder handler func after lock" << std::endl;
    waitinfo.event = true;
    waitinfo.waitingCond.notify_all();
  };
  Callback cb(gc->terminatedInsertedCB_, h1);

  TransferReply reply;
  struct readinfo_s rinf(rw.get(), reply);

  // new network activity to trigger waitinfo
  std::thread wthr(getterReadWatcher, gc, &rinf, &waitinfo);

  std::map<uint64_t, LGMessages::req_s> offered;
  bool lastrret = true;

  do {
    std::vector<uint64_t> offer2delete;
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    for(const auto &ofm: offered) {
      const uint64_t id = ofm.first;
      const LGMessages::req_s &req = ofm.second;
      struct timespec tsdiff;
      ts_timersub(&now,&req.timeoffered,&tsdiff);
      float tdiff;
      ts_timer2float(&tsdiff, &tdiff);
      if (ts_timercmp(&req.timeoffered,&now,>) || tdiff > MAX_UNASSIGNED_OFFER_AGE) {
        offer2delete.push_back(id);
      }
    }
    for(const auto &id: offer2delete) {
      offered.erase(id);
    }

    if (lastrret) {
      std::lock_guard<std::mutex> lck2(rinf.mtx);
      bool readresavail = rinf.newres;
      if (readresavail) {
        lastrret = rinf.rret;
        if (lastrret) {
          transferGetterProcessRead(gc, reply, offered, rw.get(), rinf.finish);
          rinf.cv.notify_all();
        }
        rinf.newres = false;
      }
    }
    //
    {
      std::unique_lock<std::mutex> lck(gc->terminatedMtx_);
      if (gc->terminatedInserted_) {
        for(const auto &termm: gc->terminated_) {
          const uint64_t id = termm.first;
          const std::pair<LGMessages::req_s, LGMessages::req_result_s> &reqres = termm.second;
          TransferReport report;
          report.set_id(id);
          LGMessages::FillFileResult_Finished(report, reqres.second.fileresult);
          if (reqres.second.error) {
            report.set_status(TransferReport::FAILED);
            report.set_message(reqres.second.errorstring);
            report.set_rc(1);
            std::string msg = "Setting report to FAILED message=" + report.message() + " / Summary " + reqres.second.fileresult.msgString();
            MDLog::Log(MDLog::WARN, msg);
          } else {
            report.set_rc(0);
            report.set_status(TransferReport::DONE);
            std::string msg = "Setting report to DONE / Summary " + reqres.second.fileresult.msgString();
            MDLog::Log(MDLog::INFO, msg);
          }

          LGMessages::FillLGInfo(report, gc);

          // grpc docs say Write is thread-safe with respect to the Read which
          // may be going on in getterReadWatcher
          rw->Write(report);
        }
        gc->terminated_.clear();
        gc->terminatedInserted_ = false;
      }
    }

    std::unique_lock<std::mutex> waitlck(waitinfo.waitingMtx);

    while(!waitinfo.event && lastrret) {
      waitinfo.waitingCond.wait(waitlck);
    }
    waitinfo.event = false;
    
  } while(lastrret);

//std::cerr << "In transferGetterAndResponder waiting for readWatcher thread" << std::endl;
  wthr.join();
//std::cerr << "In transferGetterAndResponder exiting func" << std::endl;

  if (rinf.finish) {
    gc->finishRequested_ = true;
  }
}
