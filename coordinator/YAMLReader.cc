/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "CoordConfig.hh"

#include <yaml-cpp/yaml.h>
#include <iostream>

static void ReadYaml0(CoordConfig &cc) {
  // Config file containg the file profiles and access profiles
  YAML::Node config = YAML::LoadFile(cc.configfn_);

  if (!config["targethost"]) {
    throw std::string("Missing targethost key");
  }
  const std::string targethost = config["targethost"].as<std::string>();

  if (!config["pathprefix"]) {
    throw std::string("Missing pathprefix key");
  }
  const std::string pathprefix = config["pathprefix"].as<std::string>();

  if (!config["substreams"]) {
    throw std::string("Missing substreams key");
  }
  const int nsubstreams = config["substreams"].as<int>();

  if (!config["usingmockoss"]) {
    throw std::string("Missing usingmockoss key");
  }
  const bool usingmockoss = config["usingmockoss"].as<bool>();

  if (!config["accessmapsize"]) {
    throw std::string("Missing accessmapsize key");
  }
  const int accessmapsize = config["accessmapsize"].as<int>();

  cc.profiles.setTargetHost(targethost);
  cc.profiles.setPathPrefix(pathprefix);
  cc.profiles.setUsingMockOss(usingmockoss);
  cc.profiles.setReqSubstreams(nsubstreams);
  cc.profiles.setAccessMapSize(accessmapsize);

  // read all the file profiles and add them to the config
  if (!config["file_profile"]) {
    throw std::string("No file_profile keys");
  }
  YAML::Node fileProfiles = config["file_profile"];
  size_t fpidx=0;
  for(YAML::const_iterator fpitr=fileProfiles.begin();fpitr!=fileProfiles.end();++fpitr,++fpidx) {
    if (!((*fpitr)["name"])) {
      throw std::string("No name key in file profile idx#") + std::to_string(fpidx);
    }
    const std::string name = (*fpitr)["name"].as<std::string>();
    if (!((*fpitr)["re"])) {
      throw std::string("No re key in file profile ") + name;
    }
    const std::string reg = (*fpitr)["re"].as<std::string>();
    if (!((*fpitr)["latency"])) {
      throw std::string("No latency key in file profile ") + name;
    }
    const unsigned int latency = (*fpitr)["latency"].as<unsigned int>();
    if (!((*fpitr)["matches"])) {
      throw std::string("No matches key in file profile ") + name;
    }
    YAML::Node matches = (*fpitr)["matches"];
    std::vector<std::pair<float, std::string> > m;
    size_t midx=0;
    for(YAML::const_iterator mitr=matches.begin();mitr!=matches.end();++mitr,++midx) {
      if (!((*mitr)["frac"])) {
        throw std::string("No frac key in file profile ") + name + " matches section idx#" + std::to_string(midx);
      }
      const float frac = (*mitr)["frac"].as<float>();
      if (!((*mitr)["accessprofile"])) {
        throw std::string("No accessprofile key in file profile ") + name + " matches section idx#" + std::to_string(midx);
      }
      const std::string ap = (*mitr)["accessprofile"].as<std::string>();
      m.push_back(std::make_pair(frac,ap));
    }
    cc.profiles.AddToFileProfile(name, reg, latency, m);
  }

  // read allt he access profiles and add them to the config
  if (!config["access_profile"]) {
    throw std::string("No access_profile keys");
  }
  YAML::Node accessProfiles = config["access_profile"];
  size_t apidx = 0;
  for(YAML::const_iterator apitr=accessProfiles.begin();apitr!=accessProfiles.end();++apitr,++apidx) {
    if (!((*apitr)["name"])) {
      throw std::string("No name key in access profile idx#") + std::to_string(apidx);
    }
    const std::string name = (*apitr)["name"].as<std::string>();
    if (!((*apitr)["proto"])) {
      throw std::string("No proto key in access profile ") + name;
    }
    const std::string proto = (*apitr)["proto"].as<std::string>();
    if (!((*apitr)["frac_bytes_read"])) {
      throw std::string("No frac_bytes_read key in access profile ") + name;
    }
    const double fBytesRead = (*apitr)["frac_bytes_read"].as<double>();
    if (!((*apitr)["duration"])) {
      throw std::string("No duration key in access profile ") + name;
    }
    const float fDuration = (*apitr)["duration"].as<float>();
    if (!((*apitr)["duration_per_mb"])) {
      throw std::string("No duration_per_mb key in access profile ") + name;
    }
    const float fDurationPerMByte = (*apitr)["duration_per_mb"].as<float>();
    if (!((*apitr)["seekspermbissued"])) {
      throw std::string("No seekspermbissued key in access profile ") + name;
    }
    const float fSeeksPerMBIssued = (*apitr)["seekspermbissued"].as<float>();
    if (!((*apitr)["seekedfracperseek"])) {
      throw std::string("No seekedfracperseek key in access profile ") + name;
    }
    const float fSeekedFracPerSeek = (*apitr)["seekedfracperseek"].as<float>();
    if (!((*apitr)["nsimthreads"])) {
      throw std::string("No nsimthreads key in access profile ") + name;
    }
    const int nsimthreads = (*apitr)["nsimthreads"].as<int>();
    if (!((*apitr)["probvecread"])) {
      throw std::string("No probvecread key in access profile ") + name;
    }
    const float fProbVecRead = (*apitr)["probvecread"].as<float>();
    if (!((*apitr)["overrunfactor"])) {
      throw std::string("No overrunfactor key in access profile ") + name;
    }
    const float fOverrunFactor = (*apitr)["overrunfactor"].as<float>();
    if (!((*apitr)["overrunabs"])) {
      throw std::string("No overrunabs key in access profile ") + name;
    }
    const float fOverrunAbs = (*apitr)["overrunabs"].as<float>();
    if (!((*apitr)["vecreadsize"])) {
      throw std::string("No vecreadsize key in access profile ") + name;
    }
    const int ivecreadsize = (*apitr)["vecreadsize"].as<int>();
    if (!((*apitr)["maxioqueue"])) {
      throw std::string("No maxioqueue key in access profile ") + name;
    }
    const int maxioqueue = (*apitr)["maxioqueue"].as<int>();
    if (!((*apitr)["filesperlogin"])) {
      throw std::string("No filesperlogin key in access profile ") + name;
    }
    const int filesperlogin = (*apitr)["filesperlogin"].as<int>();
    if (!((*apitr)["pdf"])) {
      throw std::string("No pdf key in access profile ") + name;
    }
    YAML::Node pdfs = (*apitr)["pdf"];
    if (!(pdfs["readsize"])) {
      throw std::string("No readsize key in pdf section of access profile ") + name;
    }
    YAML::Node readSize = pdfs["readsize"];
    if (!(readSize["range"])) {
      throw std::string("No range key in pdf/readsize section of access profile ") + name;
    }
    YAML::Node range = readSize["range"];
    if (range.size() != 2) {
      throw std::string("range array has the wrong size in pdf/readsize section of access profile ") + name;
    }
    const float fReadSizeLow = range[0].as<float>();
    const float fReadSizeHigh = range[1].as<float>();
    if (!(readSize["bins"])) {
      throw std::string("No bins key in pdf/readsize section of access profile ") + name;
    }
    YAML::Node readSizeBins = readSize["bins"];
    if (!(pdfs["seekdistance"])) {
      throw std::string("No seekdistance key in pdf section of access profile ") + name;
    }
    YAML::Node seekDistance = pdfs["seekdistance"];
    if (!(seekDistance["bins"])) {
      throw std::string("No bins key in pdf/seekdistance section of access profile ") + name;
    }
    YAML::Node seekDistanceBins = seekDistance["bins"];
    std::vector<float> rs, sd;
    for(YAML::const_iterator pitr = readSizeBins.begin(); pitr != readSizeBins.end(); ++pitr) {
      rs.push_back((*pitr).as<float>());
    }
    for(YAML::const_iterator pitr = seekDistanceBins.begin(); pitr != seekDistanceBins.end(); ++pitr) {
      sd.push_back((*pitr).as<float>());
    }
    cc.profiles.AddToAccessProfile(name, proto, fBytesRead, fDuration, fDurationPerMByte, fOverrunFactor, fOverrunAbs, fSeeksPerMBIssued, fSeekedFracPerSeek, fProbVecRead, ivecreadsize, maxioqueue, filesperlogin, nsimthreads, fReadSizeLow, fReadSizeHigh, rs, sd);
  }
  
}

void ReadYaml(CoordConfig &cc) {
  try {
    ReadYaml0(cc);
  } catch(YAML::ParserException &e) {
    throw std::string(e.what());
  } catch(YAML::BadFile &e) {
    throw std::string(e.what());
  }
}

#ifdef DEBUG
int main() {
  CoordConfig coordConfig;

  ReadYaml(coordConfig);
}
#endif
