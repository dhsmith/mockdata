/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "GlobalRateEstimate.hh"
#include <iostream>

#include <map>
#include <deque>
#include <algorithm>
#include <set>

struct FidInfo_s {
  FidInfo_s() : start(0) { }
  ~FidInfo_s()  { }
  std::string lgName;
  time_t start;
};

struct ByteReport_s {
  ByteReport_s() : received(0), originTs(0), count(0) { }
  ~ByteReport_s() { }
  time_t received;
  uint64_t originTs;
  uint64_t count;
};

struct GELGInfo_s {
  GELGInfo_s() { }
  ~GELGInfo_s() { }
  std::string lgName;
  std::map <uint64_t, FidInfo_s> fids;
  std::deque<ByteReport_s> byteReports;
};

std::map<std::string, GELGInfo_s> gLG;
std::map<uint64_t, std::string> gFid2lgName;
time_t gLastClean;

static void clearOldRecordsLGs(const time_t now) {

  // don't spend resoures cleaning too often
  if (now < gLastClean + 30) return;
  gLastClean = now;

  // for clearing old bytereports/LG infos
  // find oldest tracked fid
  time_t oldest=now;
  for(auto lgm: gLG) {
    GELGInfo_s &lg = lgm.second;
    for(auto f: lg.fids) {
      if (f.second.start < oldest) {
        oldest = f.second.start;
      }
    }
  }
  // remove records more the 60s older than oldest fid or current time if no fids
  std::set<std::string> lg2delete;
  for(auto lgm: gLG) {
    const std::string id = lgm.first;
    GELGInfo_s &lgi = lgm.second;
    ByteReport_s r{};
    r.received = oldest-60;
    auto lb = std::lower_bound(lgi.byteReports.begin(), lgi.byteReports.end(), r,
            [](const ByteReport_s &lhs, const ByteReport_s &rhs) {
              return lhs.received < rhs.received;
            });
    lgi.byteReports.erase(lgi.byteReports.begin(), lb);
    if (lgi.byteReports.empty() && lgi.fids.empty()) {
      lg2delete.insert(id);
    }
  }
  for(auto name: lg2delete) {
    gLG.erase(name);
  }
}

static void clearFid(const uint64_t EndingFid, const time_t now) {
  if (gFid2lgName.find(EndingFid) == gFid2lgName.end()) return;
  std::string lgName = gFid2lgName[EndingFid];
  gFid2lgName.erase(EndingFid);
  GELGInfo_s &lg=gLG[lgName];
  lg.fids.erase(EndingFid);
}

void GRENotifyAssumeLostFid(const uint64_t EndingFid) {
  const time_t now=time(0);
  clearFid(EndingFid,now);
}

void GRENotifyAssignedFid(const std::string &lgIdStr, const uint64_t Fid, const time_t aproxStart) {
  if (gLG.find(lgIdStr) == gLG.end()) return;
  GELGInfo_s &lg = gLG[lgIdStr];
  FidInfo_s fi{};
  fi.lgName = lgIdStr;
  fi.start = aproxStart;
  lg.fids[Fid] = fi;
  gFid2lgName[Fid] = lgIdStr;
}

void GREMakeEstimateForEndingFid(const uint64_t EndingFid, const LGInfo &li, float &rateEstimate) {
  const time_t now=time(0);
  rateEstimate = 0.0;
  if (gFid2lgName.find(EndingFid) == gFid2lgName.end()) return;

  // so we don't use a very old record
  clearOldRecordsLGs(now);

  const std::string &lgName = gFid2lgName[EndingFid];

  GELGInfo_s &flgi = gLG[lgName];
  FidInfo_s &fi = flgi.fids[EndingFid];
  const time_t starttime = fi.start;

  // add in ending measurement
  GRENotifyLGRBytes(li);

  // look for closest measurements in all LGs
  size_t nmeasurements = 0;
  for(auto lgm: gLG) {
    GELGInfo_s &lgi = lgm.second;
    if (lgi.byteReports.size() <= 1) continue;

    // look for closest points to range
    ByteReport_s r{};
    r.received = starttime;
    auto lb = std::lower_bound(lgi.byteReports.begin(), lgi.byteReports.end(), r,
            [](const ByteReport_s &lhs, const ByteReport_s &rhs) {
              return lhs.received < rhs.received;
            });
    // the last report should be the most suitable for the upper end
    auto ub = lgi.byteReports.begin() + (lgi.byteReports.size()-1);

    // if most recenet is older than 60 seconds before our file's start time ignore
    // this LG. (It should be more recent as each LG should have sent a record along
    // with its claim).
    if ((ub->received < starttime) && (starttime - ub->received) > 60) continue;

    if (lb == lgi.byteReports.end()) {
      lb = ub;
    }
    while(lb != lgi.byteReports.begin()) {
     if (ub->originTs - lb->originTs >= 1000) break;
      --lb;
    }
    if (ub->originTs - lb->originTs < 1000) continue;

    // count is bytes, originTs is milliseconds
    // rate will be MB/s
    double rate = double(ub->count - lb->count) / (ub->originTs - lb->originTs) / 1E3;

    rateEstimate += rate;
    ++nmeasurements;
  }
  clearFid(EndingFid,now);
}

void GRENotifyLGRBytes(const LGInfo &li) {
  const std::string &lgName = li.lgidstr_;
  if (gLG.find(lgName) == gLG.end()) {
    GELGInfo_s x{};
    x.lgName = lgName;
    gLG[lgName] = x;
  }
  GELGInfo_s &x = gLG[lgName];
  ByteReport_s nb{};
  nb.received = time(0);
  nb.originTs = li.lgbreadms_;
  nb.count = li.lgbread_;
  x.byteReports.push_back(nb);
}
