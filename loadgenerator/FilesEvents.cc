/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FilesEvents.hh"
#include "LGUtil.hh"
#include "common/MDLog.hh"

#include <unordered_map>

using namespace LGMessages::enums;

#define XRDCL_OP_TIMEOUT 0
#define MAX_IO_QUEUE_DURATION 2.0

void FilesEvents::fillFileResult(FileResult &fileresult, const struct timespec &now, const FileStatus &st) {
  fileresult.id_ = st.id;

  struct timespec tv;
  ts_timersub(&now,&st.tvstart,&tv);
  ts_timer2float(&tv,&fileresult.duration_);

  float sum=0;
  if (st.seekFeedback.seekFeedbackHistSum>0) {
    for(size_t idx=0;idx<st.seekFeedback.seekFeedbackHist.size();++idx) {
      float diff = st.accessprofile->pdfSeekDistance[idx] - ((float)st.seekFeedback.seekFeedbackHist[idx])/st.seekFeedback.seekFeedbackHistSum;
      sum += diff*diff;
    }
  }
  if (st.seekFeedback.seekFeedbackHist.size()>0) {
    sum /= st.seekFeedback.seekFeedbackHist.size();
  }
  fileresult.residue_ = sum;
  fileresult.url_ = st.fullfilename;
  fileresult.nbissued_ = st.nbissued;
  fileresult.nbscalarread_ = st.nbscalarread;
  fileresult.nbvecread_ = st.nbvecread;
  fileresult.accessmap_ = st.results_stats_offsetbytes;
  fileresult.nseekbytes_ = st.results_nseekbytes;
  fileresult.nseeks_ = st.results_nseeks;
  fileresult.nvecreadcalls_ = st.results_nvecreadcalls;
  fileresult.nreadcalls_ = st.results_nreadcalls;
  fileresult.nvecreadchunks_ = st.results_nvecreadchunks;
  fileresult.fsize_ = st.filesize;
  fileresult.lastserver_ = st.dataserver;
  fileresult.xrdreqcallbacktime_ = st.totalXrdClientTime.GetSum();
  fileresult.xrdreqcallbacktsum_ = st.totalXrdClientTime.GetSumWithOverlap();
  fileresult.accessprofilename_ = st.accessprofile->name;
  fileresult.fileprofilename_ = st.fileprofile->name;
  fileresult.overrunfactor_ = st.accessprofile->fOverrunFactor;
  fileresult.overrunabs_ = st.accessprofile->fOverrunAbs;
  fileresult.nsubstreams_ = st.accessprofile->maxioqueue;
  fileresult.intendedduration_ = st.intendedduration;
  fileresult.nsimthreads_ = st.nsimthreads;
  fileresult.lengthfactor_ = st.accessprofile->fBytesRead;
  fileresult.nbtoread_ = st.nbtoread;
}

void FilesEvents::errorLongRunning(const struct timespec &now) {
  for(auto &stm: stmap_) {
    FileStatus &st = stm.second;
    if (!st.open || st.donereadissuing || st.result.error) {
      continue;
    }
    if (ts_timercmp(&st.tvtargetend, &now, <)) {
      struct timespec tv;
      ts_timersub(&now,&st.tvtargetend,&tv);
      float overrun;
      ts_timer2float(&tv,&overrun);
      if (overrun <= st.accessprofile->fOverrunAbs) {
        continue;
      }
      ts_timersub(&st.tvtargetend,&st.tvstart,&tv);
      float target;
      ts_timer2float(&tv,&target);
      if (target>0 && overrun/target < st.accessprofile->fOverrunFactor) {
        continue;
      }
      throw FileError(st, "Transfer has overrun by " + std::to_string(overrun) + " seconds, nread=" + std::to_string(st.nread) + " nremain=" + std::to_string(st.nremain));
    }
  }
}

void FilesEvents::errorCoordinatorGone(const bool doneassigning) {
  for(auto &stm: stmap_) {
    FileStatus &st = stm.second;
    if (!st.open || st.donereadissuing || st.result.error) {
      continue;
    }
    if (doneassigning) {
      throw FileError(st, "Connection to coordinator gone. Stopping transfer.");
    }
  }
}

void FilesEvents::moveToTerminated(GetterContext *gc, const struct timespec &now) {
  std::vector<uint64_t> reqdonevec;
  for(auto &stm: stmap_) {
    const uint64_t id = stm.first;
    FileStatus &st = stm.second;
    if (!(st.status == CLOSED || (st.result.error && !st.open))) {
      continue;
    }
    if (st.opening) {
      continue;
    }
    st.totalXrdClientTime.Summarise();
    reqdonevec.push_back(id);
  }
  if (reqdonevec.size()>0) {
    // terminatedMtx before assignedMtx
    {
      std::lock_guard<std::mutex> lck1(gc->terminatedMtx_);
      std::lock_guard<std::mutex> lck2(gc->assignedMtx_);
      for(const auto &id: reqdonevec) {
        fillFileResult(stmap_[id].result.fileresult, now, stmap_[id]);

        gc->terminated_[id] = std::make_pair(gc->assigned_[id],stmap_[id].result);
        stmap_.erase(id);
        gc->assigned_.erase(id);
      }
      gc->terminatedInserted_ = true;
    }
    gc->terminatedInsertedCB_.CallbackInterested();
  }
}

void FilesEvents::CompleteClose() {
  for(auto &stm: stmap_) {
    FileStatus &st = stm.second;
    if (st.status != CLOSE_SUBMITTED || !st.closing) {
      continue;
    }
    struct outstandingIo_s *io = st.closing.get();
//std::cerr << "Checking closing, got one done/inprogress st=" << &st << " io=" << io << std::endl;
    {
      std::lock_guard<std::mutex> lck(io->outstandingDoneMtx);
      if (!io->done) {
        continue;
      }
    }
    std::unique_ptr<struct outstandingIo_s> op = std::move(st.closing);
    if (!io->XrdClStatus.IsOK()) {
      MDLog::Log(MDLog::ERROR,"Error result (ignoring) from close: " + io->XrdClStatus.ToString());
    }

//std::cerr << "its done closing" << std::endl;
    st.open = 0;
    st.status = CLOSED;
    // not checking for errors at close
  }
}

void FilesEvents::SubmitClose(LGMessages::wake_clientllop_s &waitinfo) {
  for(auto &stm: stmap_) {
    FileStatus &st = stm.second;
    if (st.status != READ_DRAIN && st.status != READ_READY) {
      continue;
    }
    if (st.status == READ_DRAIN) {
      //
      // read_Drain may be entered after an error; should still wait for read to complete
      //
      int noutstanding = 0;
      for(int n=0;n<st.nsimthreads;++n) {
        for(auto &m: st.th[n].outstanding) {
          if (m.second) {
            ++noutstanding;
          }
        }
      }
      if (noutstanding != 0) {
        continue;
      }
    }
    if (st.status == READ_READY && !st.donereading) {
      continue;
    }
    // start to close
    std::unique_ptr<struct outstandingIo_s> io(new struct outstandingIo_s(outstandingIo_s::Type::CLOSE, &waitinfo, st.clFile, 0));
    st.status = CLOSE_SUBMIT;
    XrdCl::XRootDStatus res = st.clFile->Close(io->handler, XRDCL_OP_TIMEOUT);

    {
      char buf[256];
      sprintf(buf,"%p",(void*)st.clFile.get());
      MDLog::Log(MDLog::DEBUG,"Called close on clFile " + std::string(buf) + " for id=" + std::to_string(st.id) + " fn=" + st.fullfilename);
    }

    if (!res.IsOK()) {
      MDLog::Log(MDLog::ERROR,"Error from clFile->Close: " + res.ToString());
      io->DeleteHandler();
      throw FileError(st, res.ToString());
    }
    st.closing = std::move(io);
    st.status = CLOSE_SUBMITTED;
  }
}

size_t FilesEvents::StartVectorRead(std::unique_ptr<struct outstandingIo_s> &io, FileStatus &st, int nth, const struct timespec &now, LGMessages::wake_clientllop_s &waitinfo) {
  XrdCl::ChunkList chunks;
  std::vector<uint64_t> lower;
  std::vector<uint64_t> upper;
  lower.reserve(st.accessprofile->ivecreadsize);
  upper.reserve(st.accessprofile->ivecreadsize);

  do {
    uint64_t n2readnext;
    XrdCl::ChunkInfo c;
    LGUtil::CalcNextReadSizeAndTime(n2readnext, st.nread, st.nbissuedremain, (st.nread+st.nremain), st.filesize, st.totalXrdClientTime.GetSum(), now, st.tvnext, st.tvstart, st.tvtargetend, st.accessprofile);
    float seekbytesperseek = 0.0f;
    if (st.results_nseeks>0) {
      seekbytesperseek = (float)st.results_nseekbytes / st.results_nseeks;
    }
    float seekperissuedbyte = 0.0f;
    if (st.nbissued>0) {
      seekperissuedbyte = (float)st.results_nseeks/st.nbissued;
    }
    uint64_t NextOffset;
    LGUtil::CalcNextOffset(NextOffset, st.th[nth].issueOffset, st.filesize, n2readnext, seekperissuedbyte, seekbytesperseek, st.results_nseeks, st.nbissued, st.nbissued+st.nbissuedremain, st.gcd, st.accessprofile, st.seekFeedback);
    if (LGUtil::shorten_to_avoid_overlap(NextOffset, n2readnext, lower, upper)<0) {
      // we can't put a range starting with this offset in the current vector read
      // give up: a range starting here will go in the next read or set of vread
      // (have already counted this range in the feedback information, ignore this)
      break;
    }
    c.length = n2readnext;
    c.offset = NextOffset;
    st.results_nseekbytes += (NextOffset > st.th[nth].issueOffset) ? (NextOffset - st.th[nth].issueOffset) : (st.th[nth].issueOffset - NextOffset);
    if (NextOffset != st.th[nth].issueOffset) {
      st.results_nseeks++;
    }
    st.th[nth].issueOffset = NextOffset + n2readnext;
    st.nbissuedremain -= n2readnext;
    st.nbissued += n2readnext;
    if (st.nbissuedremain == 0) {
      st.donereadissuing=1;
    }
    chunks.push_back(c);
  } while(!st.donereadissuing && chunks.size() < size_t(st.accessprofile->ivecreadsize));

  uint64_t totread=0;
  for(const auto &c: chunks) {
    totread += c.length;
  }

  io.reset(new struct outstandingIo_s(outstandingIo_s::Type::VREAD, &waitinfo, st.clFile, totread));
  io->ioid = st.nextIoId++;
  io->usingmockoss = st.usingmockoss;
  if (io->usingmockoss) {
    io->seed = st.out_seed;
  }
//io->chunks = chunks;
  io->len = totread;

  XrdCl::XRootDStatus res=st.clFile->VectorRead(chunks, io->datap(), io->handler, XRDCL_OP_TIMEOUT);
  if (!res.IsOK()) {
    MDLog::Log(MDLog::ERROR,"Error from clFile->VectorRead: " + res.ToString());
    io->DeleteHandler();
    throw FileError(st, res.ToString());
  }

  for(const auto &c: chunks) {
    // count offset hits for reporting stats
    LGUtil::CountFileAccessStats(st.results_stats_offsetbytes, st.filesize, c.offset, c.length);
  }

  return chunks.size();
}

void FilesEvents::StartScalarRead(std::unique_ptr<struct outstandingIo_s> &io, FileStatus &st, int nth, const struct timespec &now, LGMessages::wake_clientllop_s &waitinfo) {
  uint64_t n2readnext;
  LGUtil::CalcNextReadSizeAndTime(n2readnext, st.nread, st.nbissuedremain, (st.nread+st.nremain), st.filesize, st.totalXrdClientTime.GetSum(), now, st.tvnext, st.tvstart, st.tvtargetend, st.accessprofile);
  io.reset(new struct outstandingIo_s(outstandingIo_s::Type::READ, &waitinfo, st.clFile, n2readnext));
  io->ioid = st.nextIoId++;
  io->len = n2readnext;
  io->usingmockoss = st.usingmockoss;
  if (io->usingmockoss) {
    io->seed = st.out_seed;
  }
  float seekbytesperseek = 0;
  if (st.results_nseeks>0) {
    seekbytesperseek = (float)st.results_nseekbytes / st.results_nseeks;
  }
  float seekperissuedbyte = 0.0f;
  if (st.nbissued>0) {
    seekperissuedbyte = (float)st.results_nseeks/st.nbissued;
  }
  LGUtil::CalcNextOffset(io->offset, st.th[nth].issueOffset, st.filesize, io->len, seekperissuedbyte, seekbytesperseek, st.results_nseeks, st.nbissued, st.nbissued+st.nbissuedremain, st.gcd, st.accessprofile, st.seekFeedback);
  st.results_nseekbytes += (io->offset > st.th[nth].issueOffset) ? (io->offset - st.th[nth].issueOffset) : (st.th[nth].issueOffset - io->offset);
  if (io->offset != st.th[nth].issueOffset) {
    st.results_nseeks++;
  }
  st.th[nth].issueOffset = io->offset + io->len;
  st.nbissuedremain -= io->len;
  st.nbissued += io->len;
  if (st.nbissuedremain == 0) {
    st.donereadissuing=1;
  }

  XrdCl::XRootDStatus res=st.clFile->Read(io->offset, io->len, io->datap(), io->handler, XRDCL_OP_TIMEOUT);
  if (!res.IsOK()) {
    MDLog::Log(MDLog::ERROR,"Error from clFile->Read: " + res.ToString());
    io->DeleteHandler();
    throw FileError(st, res.ToString());
  }

  // count offset hits for reporting stats
  LGUtil::CountFileAccessStats(st.results_stats_offsetbytes, st.filesize, io->offset, io->len);
}

void FilesEvents::SubmitRead(const struct timespec &now, const size_t ioMaxReadOutstanding, bool &xearliest_set, struct timespec &xearliest, LGMessages::wake_clientllop_s &waitinfo) {
  bool tswaiter_set = false;
  struct timespec tswaiter;

  std::vector<uint64_t> idshuffled;
  std::unordered_map<uint64_t,int> idfull;
  for(auto &stm: stmap_) {
    const uint64_t id = stm.first;
    FileStatus &st = stm.second;
    if (st.status != READ_READY || st.donereadissuing || st.result.error) {
      continue;
    }
    idshuffled.push_back(id);
  }

  do {
    LGUtil::randidshuffle(idshuffled);
    int nsub = 0, ibreak = 0;
    for(const auto &id: idshuffled) {
      FileStatus &st = stmap_[id];
//std::cout << "checking eligable st=" << &st << " now=" << now.tv_sec << " tvnext=" << st.tvnext.tv_sec << std::endl;
      if (st.donereadissuing) {
        continue;
      }
      if (idfull.find(id) != idfull.end()) {
        continue;
      }
      if (ts_timercmp(&st.tvnext, &now, >)) {
        if (!tswaiter_set || ts_timercmp(&tswaiter, &st.tvnext, >)) {
          tswaiter_set = true;
          tswaiter = st.tvnext;
        }
        idfull[id] = 1;
        continue;
      }
//std::cerr << "st.readIODurationSample=" << st.readIODurationSample << " st.readIODurationSampleSkip=" << st.readIODurationSampleSkip << " st.targetReadOutstanding=" << st.targetReadOutstanding << " st.ioReadOutstandingPerFile=" << st.ioReadOutstandingPerFile << std::endl;
      if (st.readIODurationSample > 0 && st.readIODurationSampleSkip == 0) {
        size_t newtarget = ((float)MAX_IO_QUEUE_DURATION)/st.readIODurationSample * st.targetReadOutstanding;
        if (newtarget > st.targetReadOutstanding) {
          ++st.targetReadOutstanding;
        } else if (newtarget < st.targetReadOutstanding) {
          st.targetReadOutstanding = newtarget;
        }
        st.readIODurationSampleSkip = 1+st.ioReadOutstandingPerFile;
      }

      if (st.targetReadOutstanding <= 0) {
        st.targetReadOutstanding = 1;
      } else if (st.targetReadOutstanding > size_t(st.maxioqueue)) {
        st.targetReadOutstanding = st.maxioqueue;
      }

      if (st.ioReadOutstandingPerFile >= st.targetReadOutstanding) {
        idfull[id] = 1;
        continue;
      }

      if (outstandingIo_s::getReads() >= ioMaxReadOutstanding) {
        ibreak = 1;
        break;
      }
      // choose a simulated thread
      int n = LGUtil::randdouble() * st.nsimthreads;
      // queue up another IO

      std::unique_ptr<struct outstandingIo_s> io;
      if (st.accessprofile->fProbVecRead < LGUtil::randdouble()) {
        StartScalarRead(io, st, n, now, waitinfo);
        ++st.results_nreadcalls;
      } else {
        const size_t nchunks = StartVectorRead(io, st, n, now, waitinfo);
        st.results_nvecreadchunks += nchunks;
        ++st.results_nvecreadcalls;
      }

      const uint64_t ioid = io->ioid;
      st.th[n].outstanding[ioid] = std::move(io);
      ++st.ioReadOutstandingPerFile;
      ++nsub;
    }
    if (nsub==0 || ibreak) {
      break;
    }
  } while(1);

  // if we have space for IOs, identify when to check files again
  if (outstandingIo_s::getReads() < ioMaxReadOutstanding) {
    if (tswaiter_set) {
      if (!xearliest_set || ts_timercmp(&xearliest,&tswaiter,>)) {
        xearliest_set = true;
        xearliest = tswaiter;
      }
    }
  }

}

void FilesEvents::CompleteRead(const timespec &now) {
  for(auto &stm: stmap_) {
    FileStatus &st = stm.second;
    if (st.status != READ_READY && st.status != READ_DRAIN) {
      continue;
    }
    bool earliestTSet = false;
    timespec earliestT;
    for(int n=0;n<st.nsimthreads;++n) {
      std::vector<uint64_t> donevec;
      int nIO = 0;
      for(auto &iom: st.th[n].outstanding) {
        nIO++;
        const uint64_t ioid = iom.first;
        if (!iom.second) {
          donevec.push_back(ioid);
          continue;
        }
        {
          std::lock_guard<std::mutex> lck(iom.second->outstandingDoneMtx);
          if (!iom.second->done) {
            continue;
          }
        }
        --st.ioReadOutstandingPerFile;
        std::unique_ptr<struct outstandingIo_s> io = std::move(iom.second);
        st.totalXrdClientTime.AddRange(io->iostart,io->ioend);
        if (nIO==1) {
          if (ts_timercmp(&io->iostart, &st.th[n].noOutstandingIOBefore, >)) {
            st.th[n].noOutstandingIOBefore = io->iostart;
          }
        }
        if (!io->XrdClStatus.IsOK()) {
          std::string msg = "Error during read request: " + std::string(io->XrdClStatus.ToString());
          if (io->IOExtraErrorMessage.length()>0) {
            msg += " [LGINFO] " + io->IOExtraErrorMessage;
          }
          throw FileError(st, msg);
        }
        if (st.readIODurationSampleSkip>0) {
          st.readIODurationSampleSkip--;
        }
        if (st.readIODurationSampleSkip==0) {
          struct timespec tv;
          ts_timersub(&io->ioend, &io->iostart, &tv);
          float tf;
          ts_timer2float(&tv,&tf);
          st.readIODurationSample = tf;
        }
        st.nremain -= io->len;
        st.nread += io->len;
        if (io->type == outstandingIo_s::Type::READ) {
          st.nbscalarread += io->len;
        } else {
          st.nbvecread += io->len;
        }
        if (st.nremain == 0) {
          st.donereading=1;
        }
        donevec.push_back(ioid);
      }
      for(const auto &ioid: donevec) {
        st.th[n].outstanding.erase(ioid);
      }
      timespec tv = now;
      if (!st.th[n].outstanding.empty()) {
        tv = st.th[n].noOutstandingIOBefore;
      }
      if (!earliestTSet || ts_timercmp(&earliestT, &tv, >)) {
        earliestT = tv;
        earliestTSet = true;
      }
    }
    if (earliestTSet) {
      st.totalXrdClientTime.Summarise(earliestT);
    }
  }
}

void FilesEvents::CompleteOpen() {
  for(auto &stm: stmap_) {
    FileStatus &st = stm.second;
    if (st.status == OPEN_SUBMITTED && st.opening) {
//std::cout << "Checking opening status for st=" << &st << " io=" << io << std::endl;
      struct outstandingIo_s *io = st.opening.get();
      {
        std::lock_guard<std::mutex> lck(io->outstandingDoneMtx);
//std::cout << "Checking opening status for st=" << &st << " io=" << io << " got lock" << std::endl;
        if (!io->done) {
          continue;
        }
      }
      std::unique_ptr<struct outstandingIo_s> op = std::move(st.opening);
      st.clFile->GetProperty("DataServer", st.dataserver);
      if (!io->XrdClStatus.IsOK()) {
        throw FileError(st, "Open returned an error: " + std::string(io->XrdClStatus.ToString()));
      }
//std::cout << "opening status for st=" << &st << " io=" << io << " got lock and done" << std::endl;
      st.open = 1;
      st.status = OPEN;
    }
  }
}

void FilesEvents::SubmitStat(LGMessages::wake_clientllop_s &waitinfo) {
  for(auto &stm: stmap_) {
    FileStatus &st = stm.second;
    if (st.status == OPEN && !st.result.error) {
      std::unique_ptr<struct outstandingIo_s> io(new struct outstandingIo_s(outstandingIo_s::Type::STAT, &waitinfo, st.clFile, 0));
      st.status = STAT_SUBMIT;
      XrdCl::XRootDStatus res = st.clFile->Stat(false, io->handler, XRDCL_OP_TIMEOUT);
      if (!res.IsOK()) {
        MDLog::Log(MDLog::ERROR,"Error from clFile->Stat: " + res.ToString());
        io->DeleteHandler();
        throw FileError(st, res.ToString());
      }
      st.stating = std::move(io);
      st.status = STAT_SUBMITTED;
    }
  }
}

void FilesEvents::CompleteStat() {
  for(auto &stm: stmap_) {
    FileStatus &st = stm.second;
    if (st.status != STAT_SUBMITTED || !st.stating) {
      continue;
    }
    struct outstandingIo_s *io = st.stating.get();
//std::cerr << "Checking closing, got one done/inprogress st=" << &st << " io=" << io << std::endl;
    {
      std::lock_guard<std::mutex> lck(io->outstandingDoneMtx);
      if (!io->done) {
        continue;
      }
    }
    std::unique_ptr<struct outstandingIo_s> op = std::move(st.stating);
    if (!io->XrdClStatus.IsOK()) {
      throw FileError(st, "Stat returned an error: " + std::string(io->XrdClStatus.ToString()));
    }
    st.statdone = 1;

    st.filesize = io->statGetSize;
    st.seekFeedback.binOffset1 = -float(st.filesize);
    st.gcd = LGUtil::gcd(2*st.filesize, st.accessprofile->cdfSeekDistance.size());
//std::cerr << "gcd(" << 2*st.filesize << "," <<  st.accessprofile->cdfSeekDistance.size() << ")=" << st.gcd << std::endl;
    st.nremain = (st.coordinator_filesize ? st.coordinator_filesize : st.filesize) * st.accessprofile->fBytesRead;
    st.nbtoread = st.nremain;
    st.nbissuedremain = st.nremain;
    for(size_t idx=0;idx<size_t(st.nsimthreads);idx++) {
      st.th[idx].issueOffset = st.filesize/st.nsimthreads*idx;
    }

    float duration = st.accessprofile->fDuration;
    if (duration==0) {
      duration = st.accessprofile->fDurationPerMByte * st.nremain /1E6;
    }
    st.intendedduration = duration;
    ts_timeraddfloat(&st.tvtargetend, duration);

    // check filesize: if we throw the status_s entry will be processed and moved to terminated
    if (st.nremain==0 || st.filesize==0) {
      throw FileError(st, "File with zero length or zero bytes to read: " + st.fullfilename);
    }

    st.status = READ_READY;
  }
}

void FilesEvents::createInitialStatusFromAssignedAndSubmitOpen(GetterContext *gc, const struct timespec &now, bool &xearliest_set, struct timespec &xearliest, LGMessages::wake_clientllop_s &waitinfo) {
  // MUST be called with GetterContext assignedMtx locked

  for(const auto &rm: gc->assigned_) {
    const uint64_t id = rm.first;
    const LGMessages::req_s &req = rm.second;
    if (stmap_.find(id) != stmap_.end()) {
      continue;
    }
        
    if (ts_timercmp(&req.desiredstarttime, &now, >)) {
      if (!xearliest_set || ts_timercmp(&req.desiredstarttime, &xearliest, <)) {
        xearliest = req.desiredstarttime;
        xearliest_set = true;
      }
      continue;
    }
    // create new entry
    stmap_.erase(id);
    FileStatus &st = stmap_[id];
    st.id = id;
    st.usingmockoss = gc->usingmockoss_;

    st.fileprofile = req.fileprofile;
    st.accessprofile = req.accessprofile;
    st.coordinator_filesize = req.fsize;

    // these must be set when filesize is definitely known;
    // if we're in non MockOss mode this is after stat()
    st.filesize = 0;
    st.nremain = 0;
    st.nbissuedremain = 0;

    st.maxioqueue = st.accessprofile->maxioqueue;
    st.nsimthreads = st.accessprofile->nsimthreads;
    st.filesperlogin = st.accessprofile->filesperlogin;
    st.th.resize(st.nsimthreads);

    std::string loginName;
    LGUtil::ChooseXrdLogin(gc, LGUtil::make_xrdcl_username(id,gc->configFetchSeq_), st.filesperlogin, loginName);
    st.fullfilename = st.accessprofile->proto + "://" + loginName + "@" + gc->targethost_ + "/" + gc->pathprefix_ + "/" + req.filename;
    if (st.usingmockoss) {
      // information for checking the server generated content
      st.fullfilename += "_" + std::to_string(req.fsize) + "_" + std::to_string(st.fileprofile->latency);
      size_t out_len;
      if (!parse_filename(st.fullfilename, out_len, st.out_latency, st.out_seed, st.out_inode) || out_len != req.fsize) {
        throw FileError(st, "Error parsing filename: " + st.fullfilename);
      }
    }

    st.results_stats_offsetbytes.resize(gc->accessmapsize_);
    st.tvstart = now;
    st.tvnext = st.tvstart;

    // tvtargetend will be updated with the duration which
    // is done after stat() in case it depends on filesize
    st.tvtargetend = st.tvstart;

    // prepare the open Io
    std::unique_ptr<struct outstandingIo_s> io(new struct outstandingIo_s(outstandingIo_s::Type::OPEN, &waitinfo, st.clFile, 0));
//std::cerr << "Starting to open file=" << st.fullfilename << " st.nremain=" << st.nremain << " duraiton=" << duration << std::endl;
    XrdCl::XRootDStatus res = st.clFile->Open(st.fullfilename, XrdCl::OpenFlags::Read, XrdCl::Access::None, io->handler, XRDCL_OP_TIMEOUT);

    {
      char buf[256];
      sprintf(buf,"%p",(void*)st.clFile.get());
      MDLog::Log(MDLog::DEBUG,"Called open on clFile " + std::string(buf) + " for id=" + std::to_string(st.id) + " fn=" + st.fullfilename);
    }

    if (!res.IsOK()) {
      MDLog::Log(MDLog::ERROR,"Error from clFile->Open: " + res.ToString());
      io->DeleteHandler();
      throw FileError(st, std::string("Open returned an error: ") + res.ToString());
    }
    st.opening = std::move(io);
    st.status = OPEN_SUBMITTED;
  } // for assigned
}

void FilesEvents::Run(GetterContext *gc) {

  LGMessages::wake_clientllop_s waitinfo{};

  const size_t ioMaxReadOutstanding = 512;

  Callback::HandlerFn h1 = [&waitinfo]() {
//std::cerr << "In h1 callback in xrdClientLoop" << std::endl;
    std::lock_guard<std::mutex> lck(waitinfo.waitingMtx);
//std::cerr << "In h1 callback after lck acquire in xrdClientLoop" << std::endl;
    waitinfo.event = true;
    waitinfo.waitingCond.notify_all();
  };
  Callback cb(gc->assignedInsertedCB_, h1); 


  bool doneassigning = false;
  struct timespec activity_period_start;
  uint64_t activity_period_loops = 0;
  clock_gettime(CLOCK_MONOTONIC, &activity_period_start);
  do {
//std::cerr << "At top of do loop in xrdClientLoop" << std::endl;
    struct timespec tv_earliest;
    bool tv_earliest_set = false;

    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);

    {
      struct timespec s;
      float tf;
      ts_timersub(&now,&activity_period_start,&s);
      ts_timer2float(&s, &tf);
      gc->next_walltotal_ = tf;
      if (gc->next_walltotal_ > 30) {
        gc->prev_walltotal_ = gc->next_walltotal_;
        gc->prev_wallwait_ = gc->next_wallwait_;
        gc->next_walltotal_ = 0.0;
        gc->next_wallwait_ = 0.0;
        activity_period_start = now;
        activity_period_loops = 0;
      } else {
        std::lock_guard<std::mutex> lck(gc->dutycycleMtx_);
        if (gc->prev_walltotal_>0.0 || activity_period_loops>0) {
          gc->curr_dutycycle_ = 1.0 - ((gc->next_wallwait_ + gc->prev_wallwait_)/(gc->next_walltotal_ + gc->prev_walltotal_));
        }
      }
    }

    try {
      // check for newly assigned; create new stmap entry, add pending open
      if (!doneassigning) {
        std::lock_guard<std::mutex> lk(gc->assignedMtx_);
        createInitialStatusFromAssignedAndSubmitOpen(gc, now, tv_earliest_set, tv_earliest, waitinfo);
        gc->assignedInserted_ = false;
        if (gc->getterAndResponderDone_) {
          doneassigning = true;
        }
      }

//{
//struct timeval n2,tv;
//gettimeofday(&n2,0);
//timersub(&n2,&now,&tv);
//float d = tv.tv_sec + ((float)tv.tv_usec)/1E6;
//std::cout << "Here1 d=" << d << std::endl;
//}

      // check for completed open
      CompleteOpen();

      SubmitStat(waitinfo);

      CompleteStat();

      // check for completed Ios
      CompleteRead(now);

      // consider files which may need IOs submitted
      SubmitRead(now, ioMaxReadOutstanding, tv_earliest_set, tv_earliest, waitinfo);

      // check for files ready to start close
      SubmitClose(waitinfo);

      // check for completion of close()
      CompleteClose();

      // finalise closed or errored entries which are not open: remove
      // the status entry, remove request from assigned and add result to terminated
      moveToTerminated(gc, now);

      // check for long running
      errorLongRunning(now);

      // stop running transfers if coordinator has gone
      errorCoordinatorGone(doneassigning);

    } catch(const FileError &fe) {

      FileStatus &st = fe.s;
      // keep first instance of error
      if (!st.result.error) {
        st.result.error = true;
        st.result.error_in_state = st.status;
        st.result.errorstring = fe.reason;
      }
      // if error is on submitting close or doing close, then proceed directly to closed state
      if (st.open) {
        if (st.status == CLOSE_SUBMIT || st.status == CLOSE_SUBMITTED) {
          // just consider closed
          st.open = 0;
          st.status = CLOSED;
        } else {
          // wait for any outstanding IOs, then close
          st.status = READ_DRAIN;
        }
      }
      // go again; there are entries with errors to handle
      continue;
    }

    if (doneassigning && stmap_.size()==0) {
      break;
    }

    std::unique_lock<std::mutex> waitlck(waitinfo.waitingMtx);
    struct timespec wait_start, wait_end;

    clock_gettime(CLOCK_MONOTONIC,&wait_start);

    if (tv_earliest_set) {
      auto d = std::chrono::seconds{tv_earliest.tv_sec} + std::chrono::nanoseconds{tv_earliest.tv_nsec};
      auto abs_time = std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds>{std::chrono::duration_cast<std::chrono::steady_clock::duration>(d)};

      if (!waitinfo.event) {
        waitinfo.waitingCond.wait_until(waitlck, abs_time);
      }

    } else {
      while(!waitinfo.event) {
        waitinfo.waitingCond.wait(waitlck);
      }
    }
    waitinfo.event = false;

    clock_gettime(CLOCK_MONOTONIC,&wait_end);

    {
      struct timespec s;
      float tf;
      ts_timersub(&wait_end,&wait_start,&s);
      ts_timer2float(&s, &tf);
      gc->next_wallwait_ += tf;
      ++activity_period_loops;
    }

  } while(1);

  {
    std::string msg = "Leaving outstandingIo_s::getReads()=" + std::to_string(outstandingIo_s::getReads());
    MDLog::Log(MDLog::DEBUG, msg);
  }

}
