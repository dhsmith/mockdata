/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "LGUtil.hh"
#include "FilesEvents.hh"

#include <cmath>

void LGUtil::CalcNextOffset(uint64_t &newOffset, const uint64_t lastOffset, const uint64_t flen, const uint64_t rlen, const float seekperissuedbyte, 
                            const float seekbytesperseek, const uint64_t nbseeks, const uint64_t nbissued, const uint64_t tot2issue, const uint64_t gcd,
                            const struct Profiles::ap_s *ap, LGMessages::seekFeedback_s &fb) {
  // choose next offset
  double factor = 0.0;
  const float seekperissuedbyte_target = ap->fSeeksPerMBIssued / 1E6;
  const float seekbytesperseek_target = ap->fSeekedFracPerSeek * flen;
  {
    if (fb.effectiveCdfRecomputeCounter == 0) {
      const size_t sz = ap->cdfSeekDistance.size();
      fb.effectiveCdf.resize(sz);
      fb.seekFeedbackHist.resize(sz);
      double s=0;
      
      for(size_t idx=0;idx<fb.effectiveCdf.size();++idx) {
        fb.effectiveCdf[idx] = ap->pdfSeekDistance[idx];
        const float expected_entries = ap->pdfSeekDistance[idx] * fb.seekFeedbackHistSum;
        if (fb.seekFeedbackHist[idx]>0 || expected_entries>1.0) {
          const float diff = fb.seekFeedbackHist[idx] - expected_entries;
          const float sig2 = 4.0f*diff*diff / std::max(1.0f,expected_entries);
          const float fact = std::exp(std::min(20.0f,std::sqrt(sig2)));

          if (diff>0) {
            fb.effectiveCdf[idx] /= fact;
          } else {
            fb.effectiveCdf[idx] *= fact;
          }

        }
        s += fb.effectiveCdf[idx];
        fb.effectiveCdf[idx] = s;
      }
      for(auto &v: fb.effectiveCdf) {
        v/= s;
      }
      fb.effectiveCdfRecomputeCounter = fb.effectiveCdf.size();
//std::cerr << "s=" << s << " feedbackTotal=" << feedbackTotal << std::endl;
    } else {
      --fb.effectiveCdfRecomputeCounter;
    }

    assert(flen>0);
    assert(rlen>0);

    // find ranges in x which will keep the new seek-factor in ranges
    // possible given the current offset and the next read len
    const double xflow = -double(lastOffset)/flen;
    const double xfhigh = (1.0 + flen - rlen - lastOffset)/flen;
    const double xlow = LGUtil::ForwardCDFLookup(fb.effectiveCdf, xflow, -1.0, 1.0);
    const double xhigh = LGUtil::ForwardCDFLookup(fb.effectiveCdf, xfhigh, -1.0, 1.0);

    double x = LGUtil::randdouble();
//std::cerr << "xhigh=" << xhigh << " xlow=" << xlow << " xflow=" << xflow << " xfhigh=" << xfhigh << " x=" << x << std::endl;
    if (xhigh != xlow) {
      x = xlow + (xhigh - xlow)*x;
      factor = LGUtil::InverseCDFLookup(fb.effectiveCdf, x, -1.0, 1.0);
//std::cerr << "inverse route: factor=" << factor << " x=" << x << " xhigh=" << xhigh << " xlow=" << xlow << " xflow=" << xflow << " xfhigh=" << xfhigh << " lastOffset=" << lastOffset << " rlen=" << rlen << std::endl;
    } else {
      // i.e. pdf was zero in range
      factor = xflow + (xfhigh-xflow)*x;
//std::cerr << "pdf-zero route: factor=" << factor << " xflow=" << xflow << " xfhigh=" << xfhigh << " lastOffset=" << lastOffset << " rlen=" << rlen << std::endl;
    }

    assert(factor >=-1 && factor < 1);

    // redistrubte inside the bin
    do {
      const double bw = 2.0/fb.effectiveCdf.size();
      ssize_t deltamin = std::floor(factor*flen);
      const size_t ibin = (1.0+(double(deltamin)/flen))/bw;
//std::cerr << "floor deltamin=" << deltamin << " with ibin=" << ibin << std::endl;
      deltamin = std::floor((-1.0 + ibin*bw)*flen);
      if ( deltamin > -ssize_t(flen) &&  ((flen+deltamin-1)%(2*flen/gcd))==0 && ((flen+deltamin-1)*fb.effectiveCdf.size()/(2*flen)) == ibin) {
        --deltamin;
      }
//std::cerr << "recomputed deltamin=" << deltamin << std::endl;
      ssize_t deltamax = std::floor((-1.0 + (ibin+1)*bw)*flen);
//std::cerr << "doing " << (1.0+(double(deltamax)/flen)) << " / " << bw << " = " << ((1.0+(double(deltamax)/flen))/bw) << " size_t(..)=" << size_t((1.0+(double(deltamax)/flen))/bw) << std::endl;
//std::cerr << "flen+deltamax=" << (flen+deltamax) << "%" << (2*flen/gcd) << "=" << ((flen+deltamax)%(2*flen/gcd)) << " and ((flen+deltamax)*fb.effectiveCdf.size()/(2*flen))=" << ((flen+deltamax)*fb.effectiveCdf.size()/(2*flen)) << std::endl;
      if (((flen+deltamax)%(2*flen/gcd))==0 && ((flen+deltamax)*fb.effectiveCdf.size()/(2*flen)) == ibin+1) {
//      if ( size_t((1.0+(double(deltamax)/flen))/bw) == ibin+1) {
//std::cerr << "decreasing" << std::endl;
        --deltamax;
      } else {
//std::cerr << "not decreasing" << std::endl;
}
//std::cerr << "recomputed deltamax=" << deltamax << " calc of factor for deltamax=" << (1.0+(double(deltamax)/flen)) << std::endl;

      if (deltamax <= deltamin) {
        // only one seek value value lies within the bin
        break;
      }

      // calculate average extra (signed) seek distance needed per seek
      const double delta_bytes_per_seek = seekbytesperseek_target - seekbytesperseek;
      ssize_t minrange = deltamin;
      ssize_t maxrange = deltamax;

//std::cerr << "Comparing lastOffset=" << lastOffset << " minrange=" << minrange << " maxrange=" << maxrange << " flen-rlen=" << flen-rlen << std::endl;
      if (ssize_t(lastOffset)+minrange < 0) {
        minrange = -ssize_t(lastOffset);
      }
      if (ssize_t(lastOffset)+maxrange > ssize_t(flen-rlen)) {
        maxrange = flen-rlen-lastOffset;
      }

//std::cerr << "before deltamin=" << deltamin << " deltamax=" << deltamax << " minrange=" << minrange << " maxrange=" << maxrange << std::endl;

     assert(minrange >= deltamin);
     assert(maxrange <= deltamax);
     assert(minrange <= maxrange);

      ssize_t nbo = fb.binOffset1;
      if (delta_bytes_per_seek>0) {
        if (nbo==0) nbo=1;
        if (nbo>0) nbo *= 2;
        else nbo /= 2;
      } else {
        if (nbo==0) nbo=-1;
        if (nbo<0) nbo *=2;
        else nbo /= 2;
      }

      if (nbo > flen*bw/2) {
        nbo = flen*bw/2;
      } else if (nbo < -(bw*flen)/2) {
        nbo = -(bw*flen)/2;
      }
      fb.binOffset1 = nbo;

      const bool enhance_zero_seek = (seekperissuedbyte >= seekperissuedbyte_target) ? true : false;
      if (deltamin<0 && deltamax>0) {
        // in the bin which crosses zero but not at the edge
        if (minrange>=0 || (enhance_zero_seek && minrange<=0 && maxrange>=0) || (maxrange>=0 && LGUtil::randdouble()<(double(maxrange+1)/(maxrange-minrange+1)))) {
          // choosing split above or below zero: this is zero-or-more half of the bin which crosses zero
          if (nbo>0) {
            const ssize_t m = std::min((ssize_t)2*nbo,maxrange);
            minrange = std::max(m,minrange);
          } else {
            ssize_t m = std::min((ssize_t)0,maxrange);
            if (!enhance_zero_seek) {
              m = std::min((ssize_t)1,maxrange);
            }
            minrange = std::max(m,minrange);
            maxrange = std::max(ssize_t(maxrange+2*nbo), minrange);
          }
        } else {
          // this is less-than-zero half of the bin which crosses zero
          if (enhance_zero_seek && !(minrange<=0 && maxrange>=0)) {
            nbo = (bw*flen)/4;
          }
          if (nbo>0) {
            const ssize_t m = std::max(ssize_t(-1-(2*nbo)), minrange);
            maxrange = std::min(m,maxrange);
          } else {
            const ssize_t m = std::max((ssize_t)-1,minrange);
            maxrange = std::min(m,maxrange);
            minrange = std::min(ssize_t(minrange - 2*nbo), maxrange);
          }
        }
      } else {
        // a bin which is entirely below zero or entirely zero or above
        if (deltamax<0) {
          // bin entirely below zero
          if (enhance_zero_seek && maxrange<deltamax) {
            nbo = (bw*flen)/2;
          }
          if (nbo>0) {
            maxrange = std::max(ssize_t(maxrange - 2*nbo),minrange);
          } else {
            minrange = std::min(ssize_t(minrange - 2*nbo),maxrange);
          }
        } else { 
          // bin entriely zero or above
          if (nbo>0) {
            minrange = std::min(ssize_t(minrange + 2*nbo), maxrange);
          } else {
            if (minrange==0 && !enhance_zero_seek) {
              const ssize_t m = std::min((ssize_t)1,maxrange);
              minrange=std::max(m,minrange);
            }
            maxrange = std::max(ssize_t(maxrange + 2*nbo), minrange);
          }
        }
      }
//std::cerr << "after minrange=" << minrange << " maxrange=" << maxrange << std::endl;

      assert(deltamax > deltamin);

      // corresoonding factor
      {
        const double bf_minpp = (double(deltamin+1)/flen);
        const double bf_max = (double(deltamax)/flen);
        const double bf_start = -1.0 + bw*ibin;
        const double bf_end = -1.0 + bw*(ibin+1);
        const ssize_t n = std::floor((maxrange-minrange+1)*LGUtil::randdouble()) + (minrange-deltamin);
        if (n==0) {
          const double y = (bf_minpp < bf_end) ? bf_minpp : bf_end;
          factor = 0.5*(bf_start + y);
//std::cerr << "h1 factor=" << factor << std::endl;
        } else if (n == (deltamax-deltamin)) {
          double y = (bf_max > bf_start) ? bf_max : bf_start;
          if (y>bf_end) {
//            std::cerr << "Overrun by " << (bf_end-y) << std::endl;
            y = bf_end;
          }
          factor = 0.5*(bf_end + y);
if (factor>bf_end) {
//  std::cerr << "factor>bf_end" << std::endl;
}
//std::cerr << std::setprecision(14) << "h2 factor=" << factor << " bf_start=" << bf_start << " y=" << y << " bf_end=" << bf_end << std::endl;
        } else {
          factor = bf_minpp + ((n-0.5)/flen);
//std::cerr << "h3 factor=" << factor << " bf_minpp=" << bf_minpp << " n=" << n << " flen=" << flen << std::endl;
        }
      }

      // factor = (-1.0+bw*ibin) + std::floor((maxrange-minrange)*LGUtil::randdouble() + (minrange-deltamin))/flen;

//std::cerr << "ibin=" << ibin << " deltamin=" << deltamin << " deltamax=" << deltamax << " minrange=" << minrange << " maxrange=" << maxrange << " nbo=" << nbo << " binOffset1=" << fb.binOffset1 << " seekbytesperseek_target=" << seekbytesperseek_target << " seekbytesperseek=" << seekbytesperseek << " factor=" << factor << " dela_bytes=" << ssize_t(std::floor(factor*flen)) << " nbseeks=" << nbseeks << " xflow=" << xflow << " xfhigh=" << xfhigh << std::endl;

{
const size_t ibin2 = (1.0+factor)/bw;
(void)(ibin2); // suppress unused variable warning
//if (ibin2 != ibin) { std::cerr << "ibin2=" << ibin2 << " ibin=" << ibin << std::endl; }
assert(ibin2 == ibin);
}


{
  ssize_t d = std::floor(factor*flen);
(void)(d); // suppress unused variable warning
  assert(d >= minrange);
  assert(d<= maxrange);
}

//      if (factor>xfhigh) factor=-1.0 + bw*ibin;
//      if (factor<xflow) factor=xflow;
    } while(0);

    fb.IncFeedbackHist(factor);
  }

  // if out of range truncate
  int64_t bdiff = std::floor(factor*flen);
  if (factor<0.0) {
    if (std::abs(bdiff) > ssize_t(lastOffset)) {
//std::cerr << "A! factor=" << factor << " lastOffset=" << lastOffset << " multi=" << std::abs(factor)*(flen+1) << " flen=" << flen << " xflow=" << xflow << " xfhigh=" << xfhigh << " x=" << x << " xlow=" << xlow << " xhigh=" << xhigh << std::endl;
      newOffset = 0;
    } else {
      newOffset = lastOffset - std::abs(bdiff);
    }
  } else {
    newOffset = lastOffset + bdiff;
  }
  if (newOffset > flen-rlen) {
//std::cerr << "B! factor=" << factor << " lastOffset=" << lastOffset << " multi=" << std::abs(factor)*(flen+1) << " flen=" << flen << " xflow=" << xflow << " xfhigh=" << xfhigh << " x=" << x << " xlow=" << xlow << " xhigh=" << xhigh << std::endl;
      newOffset = flen-rlen;
  }

//std::cerr << "Choosing newOffset=" << newOffset << " rlen=" << rlen << " flen=" << flen << " bdiff=" << bdiff << " factor=" << factor << std::endl;
  assert(newOffset <= flen-rlen);
}

// nRead is the amount received
// nRemain is the amount remaining and not yet issued (not only has the data not been received but the request has not been issued)
// nTot is the total amount that has been and will be fetched from the file
// nFsize is the actual file length
void LGUtil::CalcNextReadSizeAndTime(uint64_t &nextRSize, const uint64_t nRead, const uint64_t nRemain, const uint64_t nTot, const uint64_t nFsize,
                                     const float totalXrdClientTime, const struct timespec &now, struct timespec &tvnext, const struct timespec &tvstart,
                                     const struct timespec &tvtargetend, const struct Profiles::ap_s *ap) {
  // choose next read block size
  do {
    double x = LGUtil::randdouble();
    nextRSize = LGUtil::InverseCDFLookup(ap->cdfReadSize, x, ap->fReadSizeLow, ap->fReadSizeHigh);
    if (nextRSize == 0) {
      continue;
    }
    if (nextRSize <= std::min(nFsize,nRemain)) {
      break;
    }
    // truncate if too large
    nextRSize = std::min(nFsize,nRemain);
    break;
  } while(1);
//std::cerr << "Chose nextRSize=" << nextRSize << " where nRead=" << nRead << " nRemain=" << nRemain << " nTot=" << nTot << " totalXrdClientTime=" << totalXrdClientTime << std::endl;
  struct timespec tv;

  tvnext = now;

  if (ts_timercmp(&tvtargetend, &now, <=)) {
    // we are overdue:
    // add no further delay
    return;
  }

  // time remaining for transfer
  ts_timersub(&tvtargetend, &now, &tv);
  float remain;
  ts_timer2float(&tv,&remain);
//std::cerr << "remain = "<< remain << std::endl;

  if (nRead==0) {
    // random delay based on total time remaing for transfer
    float delay = remain * LGUtil::randdouble();
    if (nRemain>0) {
      delay *= float(nextRSize)/nRemain;
    }
    ts_timeraddfloat(&tvnext,delay);
//std::cerr << "CalcNextReadSizeAndTime returing now+delat for nread==0 case, delay=" << delay << std::endl;

    return;
  }

  // time taken so far
  ts_timersub(&now, &tvstart, &tv);
  float taken;
  ts_timer2float(&tv,&taken);

  // find average read rate needed to finish on time and average so far
  float desiredAvg = nTot/(taken+remain);
  float currentAvg = nRead/taken;

  // to make the currentAvg equal desiredAvg should read
  // nextRSize bytes in next delta seconds:
  float delta = (currentAvg/desiredAvg - 1.0)*taken + nextRSize/desiredAvg;

  // assume the time for the request once queued with XrdCl will be based on the average so far
  float avgQueueTimePerByte = totalXrdClientTime/nRead;

  // time to wait before queuing
  delta -= avgQueueTimePerByte*nextRSize;

  if (delta <= 0) {
    return;
  }
//std::cerr << "CalcNextReadSizeAndTime using delta=" << delta << " remain = "<< remain << " ca=" << currentAvg << " da=" << desiredAvg << " taken=" << taken << " nextRSize=" << nextRSize << " avgQueueTimePerByte=" << avgQueueTimePerByte << std::endl;

  ts_timeraddfloat(&tvnext,delta);
}
