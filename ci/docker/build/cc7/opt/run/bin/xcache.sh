#!/bin/bash 

. /opt/run/bin/init_pod.sh

if [ ! -e /etc/buildtreeRunner ]; then
  # Install missing RPMs
  yum -y install xrootd-server
fi

yes | cp -r /opt/ci/datasource/etc /
mkdir -p /data/xrd-cache
chown xrootd:xrootd /data/xrd-cache

if [ "-${CI_CONTEXT}-" == '-nosystemd-' ]; then
  # systemd is not available
  runuser --shell='/bin/bash' --session-command='cd ~xrootd; xrootd -s /var/run/xrootd/xrootd-pfc.pid -l /var/log/xrootd/xrootd.log -k fifo -n pfc -c /etc/xrootd/xrootd-pfc.cfg -I v4' xrootd
  echo "pfc xrootd server died"
  sleep infinity
else
  # Add a DNS cache on the client as kubernetes DNS complains about `Nameserver limits were exceeded`
  yum install -y systemd-resolved
  systemctl start systemd-resolved

  # systemd is available
  echo "Launching pfc server with systemd:"
  systemctl start xrootd@pfc

  echo "Status is now:"
  systemctl status xrootd@pfc
fi
