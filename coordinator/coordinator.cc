/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "CoordConfig.hh"
#include "CoordinatingGrpcServer.hh"
#include "ListSource.hh"
#include "ListSourceConfig.hh"
#include "FileBasedReader.hh"
#include "YAMLReader.hh"
#include "CmdlineOptions.hh"
#include "common/MDLog.hh"

#include <memory>
#include <iostream>
#include <string>
#include <thread>
#include <unistd.h>

#include <iostream>

void runTheList(CmdlineOptions &cline) {
  ListSourceConfig lsConfig(cline);

  CoordConfig coordConfig;
  coordConfig.SetConfigFn(cline.GetConfigFn());

  try {
    ReadYaml(coordConfig);
  } catch(const std::string &s) {
    std::cerr << "Error while reading configuration: " + s << std::endl;
    exit(1);
  }

  FileBasedReader fbr;
  ListSource src(fbr, lsConfig);

  CoordinatingGrpcServer server(src,coordConfig);

  server.Start();

  bool finished;
  bool requestedfinish = false;

  do {
    std::chrono::system_clock::time_point deadline = std::chrono::system_clock::now() + std::chrono::seconds(1);
    server.Tick(finished, deadline);
    if (finished) break;
    if (!requestedfinish && src.finished()) {
      server.RequestFinish();
      requestedfinish = true;
    }
  } while(1);
}

int main(int argc, char** argv) {

  CmdlineOptions cline(argc, argv);
  if (cline.error()) {
    std::cerr << "Error during command line parsing" << std::endl;
    return 1;
  }

  if (cline.help()) {
    return 0;
  }

  cline.PrintConfig();

  do {
    runTheList(cline);
  } while(cline.loop());

  MDLog::Log(MDLog::INFO, "Finished the filelist, coordinator exiting");

  return 0;
}
