/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <random>
#include <cstdint>
#include <mutex>
#include <thread>
#include <assert.h>
#include <chrono>
#include <time.h>
#include <iomanip>
#include <cmath>
#include <algorithm>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <sys/syscall.h>

#include "common/HashUtils.hh"
#include "common/PseudoDataGenerator.hh"

#include "XrdPosix/XrdPosixXrootd.hh"
#include "XrdCl/XrdClDefaultEnv.hh"
#include "XrdOuc/XrdOucIOVec.hh"

static float parse_fact(const char *const in) {
   float fact= 1;
   const char *s = in;
   while(*s && (*s == '.' || (*s>='0' && *s<='9'))) s++;
   if (!strcmp(s,"KB")) {
     fact = 1E3;
   } else if (!strcmp(s,"MB")) {
     fact = 1E6;
   } else if (!strcmp(s,"GB")) {
     fact = 1E9;
   } else if (!strcmp(s,"TB")) {
     fact = 1E12;
   } else if (!strcmp(s,"PB")) {
     fact = 1E15;
   }
   return fact;
}

static void find_b_fact(size_t v, std::string &pfx, float &fact) {
  float l = log10((float)v);
  if (l < 3) {
    pfx = "B";
    fact = 1;
  } else if (l<6) {
    pfx = "KB";
    fact = 1E3;
  } else if (l<9) {
    pfx = "MB";
    fact = 1E6;
  } else if (l<12) {
    pfx = "GB";
    fact = 1E9;
  } else if (l<15) {
    pfx = "TB";
    fact = 1E12;
  } else if (l<18) {
    pfx = "PB";
    fact = 1E15;
  } else {
    pfx = "B";
    fact = 1;
  }
}

class CmdlineOptions {
public:
  CmdlineOptions(int argc, char **argv) : m_nthreads(1), m_maxfilesize(50000000ULL), m_maxblocksize(250000ULL),
                     m_maxrveclen(50), m_lastfiles(100), m_maxreadfactor(0.25), m_probnewfile(0.25),
                     m_probseek(0.10), m_probnosinglevread(0.5), m_nfiles(100), m_verbose(0), m_nooverlap(false) {
    optind = 1;
    m_errflg = m_help = 0;
    int c;
    static struct option longopts[] = {
       { NULL, 0, NULL, '\0' }
    };
    while ((c = getopt_long(argc, argv, "hn:l:c:v:x:m:f:s:r:e:zy",longopts, NULL))!= -1) {
       switch (c) {
       case 'h':
         usage();
         m_help = 1;
         break;
       case 'n':
         {
           try {
             m_nthreads = std::stoi(optarg);
           } catch(std::exception &) {
             m_errflg++;
           }
           break;
         }
       case 'l':
         {
           try {
             m_maxfilesize = std::stof(optarg) * parse_fact(optarg);
           } catch(std::exception &) {
             m_errflg++;
           }
           break;
         }
       case 'c':
         {
           try {
             m_maxblocksize = std::stof(optarg) * parse_fact(optarg);
           } catch(std::exception &) {
             m_errflg++;
           }
           break;
         }
       case 'v':
         {
           try {
             m_maxrveclen = std::stoi(optarg);
           } catch(std::exception &) {
             m_errflg++;
           }
           break;
         }
       case 'x':
         {
           try {
             m_lastfiles = std::stoi(optarg);
           } catch(std::exception &) {
             m_errflg++;
           }
           break;
         }
       case 'm':
         {
           try {
             m_maxreadfactor = std::stof(optarg);
           } catch(std::exception &) {
             m_errflg++;
           }
           break;
         }
       case 'f':
         {
           try {
             m_probnewfile = std::stof(optarg);
           } catch(std::exception &) {
             m_errflg++;
           }
           break;
         }
       case 's':
         {
           try {
             m_probseek = std::stof(optarg);
           } catch(std::exception &) {
             m_errflg++;
           }
           break;
         }
       case 'r':
         {
           try {
             m_probnosinglevread = std::stof(optarg);
           } catch(std::exception &) {
             m_errflg++;
           }
           break;
         }
       case 'e':
         {
           try {
             m_nfiles = std::stoi(optarg);
           } catch(std::exception &) {
             m_errflg++;
           }
           break;
         }
       case 'z':
         m_verbose++;
         break;
       case 'y':
         m_nooverlap = true;
         break;
       case '?': /* Unrecognized option */
       default:
         {
           usage();
           m_errflg++;
           break;
         }
       } //switch
       if (m_errflg || m_help) return;
    } //while getopts

    // get address
    if (optind>=argc) {
      // no address
      usage();
      m_errflg++;
     } else {
       m_address = argv[optind++];
     }

  } //constructor

  void usage() {
    std::cout << "Usage: ";
    std::cout << "-h | -z -y -e <files_per_thread> -n <NTHREADS> -l <maxfilesize> -c <maxchunk> -v <maxrvec> -x <lastfiles> -m <maxreadfact> -f <prob_newfile> -s <prob_seek> -r <prob_no_single_vread> <endpoint>" << std::endl;
  }

  void PrintConfig() {
    std::string pfx;
    float fact;

    std::cout << "+++++++++++++++++++++++++" << std::endl;
    std::cout << "Address prefix: " << m_address << std::endl;
    std::cout << "Number of threads: " << m_nthreads << std::endl;
    find_b_fact(m_maxfilesize, pfx, fact);
    std::cout << "Max files ize: " << m_maxfilesize/fact << pfx << std::endl;
    find_b_fact(m_maxblocksize, pfx, fact);
    std::cout << "Max chunk size: " << m_maxblocksize/fact << pfx << std::endl;
    std::cout << "Max Rvec Length: " << m_maxrveclen << std::endl;
    std::cout << "Active last-files: " << m_lastfiles << std::endl;
    std::cout << "Max read factor: " << m_maxreadfactor << std::endl;
    std::cout << "Prob new file: " << m_probnewfile << std::endl;
    std::cout << "Prob seek: " << m_probseek << std::endl;
    std::cout << "Prob use Read() for single chunks: " << m_probnosinglevread << std::endl;
    std::cout << "+++++++++++++++++++++++++" << std::endl;
  }

  int Nthreads() const { return m_nthreads; }
  off_t Maxfilesize() const { return m_maxfilesize; }
  size_t Maxblocksize() const { return m_maxblocksize; }
  int Maxrveclen() const { return m_maxrveclen; }
  int Lastfiles() const { return m_lastfiles; }
  float Maxreadfactor() const { return m_maxreadfactor; }
  float Probnewfile() const { return m_probnewfile; }
  float Probseek() const { return m_probseek; }
  float Probnosinglevread() const { return m_probnosinglevread; }
  std::string Address() const { return m_address; }
  int Nfiles() const { return m_nfiles; }
  int Verbose() const { return m_verbose; }
  bool NoOverlap() const { return m_nooverlap; }

  bool error() const { return m_errflg; }
  bool help() const { return m_help; }
  
private:
  int m_errflg;
  int m_help;

  int m_nthreads;
  off_t m_maxfilesize;
  size_t m_maxblocksize;
  int m_maxrveclen;
  int m_lastfiles;
  float m_maxreadfactor;
  float m_probnewfile;
  float m_probseek;
  float m_probnosinglevread;
  std::string m_address;
  int m_nfiles;
  int m_verbose;
  bool m_nooverlap;
};

class RunState {
public:
  RunState(CmdlineOptions &cline) : cline_(cline), seq_(0), any_seq_opened_(false) {
    std::random_device r;
    std::mt19937 gen(r());;
    std::uniform_int_distribution<std::uintmax_t> dis;
    runnumber_ = dis(gen) & 0xffffffffffULL;
  }
  ~RunState() { }

  uint64_t getRunNumber() const { return runnumber_; }

  uint64_t getSeq() const { 
    std::lock_guard<std::mutex> guard(seq_mutex_);
    return seq_;
  }

  uint64_t getSeqPostIncrement() {
    std::lock_guard<std::mutex> guard(seq_mutex_);
    uint64_t s = seq_++;
    seq_map_[s] = true;
    return s;
  }

  void updateHighestSeq(uint64_t seq) {
    std::lock_guard<std::mutex> guard(seq_mutex_);
    if (!any_seq_opened_ || seq>highest_opened_seq_) {
      any_seq_opened_ = true;
      highest_opened_seq_ = seq;
    }
    seq_map_.erase(seq);
  }

  std::pair<bool, uint64_t> getHighestSeq() const {
    std::lock_guard<std::mutex> guard(seq_mutex_);
    if (!any_seq_opened_) return std::make_pair(false,(uint64_t)0);
    return std::make_pair(true, highest_opened_seq_);
  }

  bool seqStillOpening(uint64_t seq) const {
    std::lock_guard<std::mutex> guard(seq_mutex_);
    if (seq_map_.find(seq) != seq_map_.end()) return true;
    return false;
  }

  CmdlineOptions &cline() { return cline_; }
  mutable std::mutex cout_mutex_;

private:
  CmdlineOptions &cline_;
  uint64_t runnumber_;
  mutable std::mutex seq_mutex_;
  uint64_t seq_;
  bool any_seq_opened_;
  uint64_t highest_opened_seq_;
  std::map<uint64_t, bool> seq_map_;
};

class ReadFiles {
public:
  ReadFiles(RunState &run) : run_(run), gen_(rd_()), exp_d_(1/(float)run.cline().Lastfiles()), unif_d_(0.0,1.0), vec_size_d_(1, run.cline().Maxrveclen()) { }
  ~ReadFiles() { }

  std::string makefilename(uint64_t tag, uint64_t seq, size_t &fsize);
  uint64_t choose_oldfile(uint64_t maxseq);
  int Run();
  float randfloat();
  size_t rnd_lentoread(size_t smax);
  size_t choose_randblocksize(size_t smax, off_t pos);
  off_t choose_randpos(size_t fsize, size_t bs, off_t curpos);
  int choose_rveclen(size_t fsize);
  void setup_vecs(unsigned char *const buf, XrdOucIOVec *vec, int &n, size_t &totlen, off_t &final_pos, const off_t start_pos, const size_t max_len, const size_t fsize);
  int Open(const std::string &fn);
  void do_reads(const int fd, const XrdOucIOVec *const vecoffs, const int nvec_elements);

private:
   RunState &run_;

   std::random_device rd_;
   std::mt19937 gen_;

   std::exponential_distribution<> exp_d_;
   std::uniform_real_distribution<> unif_d_;
   std::uniform_int_distribution<> vec_size_d_;
};

std::string ReadFiles::makefilename(uint64_t tag, uint64_t seq, size_t &fsize) {
  std::string prefix = run_.cline().Address();
  std::string fn = "file_tag" + std::to_string(tag) + "_fnum" + std::to_string(seq) + ".dat";

  uint8_t digest[16];
  HashUtils::MurmurHash3_x64_128(fn.c_str(), fn.size(), 0, digest);
  fsize = U8TO64_LE(digest);
  fsize %= run_.cline().Maxfilesize();
  fsize++;

  fn += "_" + std::to_string(fsize) + "_0";
  if (prefix.length() && prefix.back() != '/') prefix += "/";
  return prefix + fn;
}

uint64_t ReadFiles::choose_oldfile(uint64_t maxseq) {
  // Choose between 0 and maxseq-1: favor more recent entries
  if (maxseq==1) return 0;
  do {
    uint64_t v = exp_d_(gen_);
//std::cerr << "v=" << v << std::endl;
    if (v < maxseq && !run_.seqStillOpening(maxseq -1 - v)) {
      return maxseq -1 - v;
    }
  } while(1);
}

float ReadFiles::randfloat() {
  // uniform between 0 and 1
  return unif_d_(gen_);
}

size_t ReadFiles::rnd_lentoread(size_t smax) {
  if (smax * run_.cline().Maxreadfactor() < 1) {
    return 1;
  }
  size_t r;
  do {
    r = 0.5 + randfloat() * smax * run_.cline().Maxreadfactor();
    if (r>0) return r;
  } while(1);
}

size_t ReadFiles::choose_randblocksize(size_t smax, off_t pos) {
  size_t bs = 1 + (randfloat() * run_.cline().Maxblocksize());
  if (bs+pos > smax) bs = smax - pos;
  return bs;
}

off_t ReadFiles::choose_randpos(size_t fsize, size_t bs, off_t curpos) {
  off_t newoff;
  assert(fsize>=bs);
  do {
    newoff = randfloat() * fsize;
    if (newoff <= (off_t)(fsize-bs)) {
      return newoff;
    }
  } while(1);
}

int ReadFiles::choose_rveclen(size_t fsize) {
  if (fsize<20) return 1;
  size_t s;
  do {
    s = vec_size_d_(gen_);
    if (s<= fsize/10) return s;
  } while(1);
}

int ReadFiles::Open(const std::string &fn) {
  int fd = -1;
//  int retr = 10;
  int retr = 0;
  do {
    fd = XrdPosixXrootd::Open(fn.c_str(), O_RDONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    if (fd<0) {
      std::cerr << "XrdPosixXrootd::Open returned <0";
      if (retr>0) {
        --retr;
        std::cerr << ": Retrying" << std::endl;
        continue;
      }
      std::cerr << std::endl;
    }
    break;
  } while(1);
  return fd;
}

void ReadFiles::setup_vecs(unsigned char *const buf, XrdOucIOVec *vec, int &n, size_t &totlen, off_t &final_pos, const off_t start_pos, const size_t max_len, const size_t fsize) {
  const int nmax = choose_rveclen(fsize);
  assert(max_len > 0);
  std::map<std::pair<off_t, size_t>, int > m;
  off_t curpos = start_pos;
  size_t avail = max_len;
  totlen = 0;
  n = 0;

  if (run_.cline().NoOverlap()) {
    if (avail > fsize) avail=fsize;
  }

  do {
    if (randfloat() < run_.cline().Probseek() || curpos >= (off_t)fsize) {
      curpos = choose_randpos(fsize, 1, curpos);
    }

    size_t bs = choose_randblocksize(fsize,curpos);
    size_t toread = avail;
    if (toread > bs) toread = bs;

    if (m.find(std::make_pair(curpos,toread)) != m.end()) continue;

    vec[n].offset = curpos;
    vec[n].size = toread;
if (vec[n].size<0) { std::cerr << "Blocksize neg: " << toread << std::endl; exit(1); }
if (vec[n].offset<0) { std::cerr << "offset neg: " << toread << " fsize=" << fsize << std::endl; exit(1); }
if (vec[n].offset>=(off_t)fsize) { std::cerr << "offset at fsize: " << toread << " fsize=" << fsize << std::endl; exit(1); }
    vec[n].data = (char*)(&buf[totlen]);
    vec[n].info = 0;

    m[std::make_pair(curpos, toread)] = 1;

    totlen += toread;
    curpos += toread;
    avail -= toread;
    ++n;
  } while(n < nmax && avail>0);
  final_pos = curpos;

  if (run_.cline().NoOverlap()) {
    // find order of chunks sorted by offset
    std::vector<int> idx(n);
    std::iota(idx.begin(), idx.end(), 0);
    std::sort(idx.begin(),idx.end(), [vec](int l, int r) { return vec[l].offset < vec[r].offset; });

    // compress starts to [0, fsize-totlen]
    for(int i=0;i<n;++i) {
      vec[i].offset = ((double)vec[i].offset)/(fsize-1)*(fsize-totlen);
    }
    // adjust starts to account for length of previous chunks
    size_t sum = 0;
    for(int i=0;i<n;++i) {
      int j = idx[i];
      vec[j].offset += sum;
if (vec[j].offset>=(off_t)fsize) { std::cerr << "Here2 vec[j].offset=" << vec[j].offset << " fsize=" << fsize << std::endl; exit(1); }
      sum += vec[j].size;
    }
  }
}

void ReadFiles::do_reads(const int fd, const XrdOucIOVec *const vecoffs, const int nvec_elements) {
  size_t vec_bytelen = 0;
  for(int i=0;i<nvec_elements;++i) vec_bytelen += vecoffs[i].size;

//  int retr = 10;
  int retr = 0;
  do {
    ssize_t rret = 0;
    if (nvec_elements == 1 && randfloat() < run_.cline().Probnosinglevread()) {
      size_t toread = vec_bytelen;
      size_t beenread = 0;
      off_t newpos = XrdPosixXrootd::Lseek(fd, vecoffs[0].offset, SEEK_SET);
      if (newpos<0) {
        std::cerr << "FATAL: Could not LSeek" << std::endl;
        exit(1);
      }
      while(toread>0) {
        rret = XrdPosixXrootd::Read(fd, vecoffs[0].data + beenread, toread);
        if (rret<=0) {
          break;
        }
        beenread += rret;
        toread -= rret;
        rret = beenread;
      }
    } else {
      rret = XrdPosixXrootd::VRead(fd, vecoffs, nvec_elements);
    }
    if (rret<0 || vec_bytelen != (size_t)rret) {
      std::cerr << "VRead returned error or an unexpected number of bytes: " << rret << " ";
      if (rret<0 && retr>0) {
        retr--;
        std::cerr << "Retrying" << std::endl;
        continue;
      }
      std::cerr << "FATAL" << std::endl;
std::cerr << "nvec_elements=" << nvec_elements << " vec_bytelen=" << vec_bytelen << std::endl;
for(int idhs=0;idhs<nvec_elements;++idhs) {
  std::cerr << "n[" << idhs << "].offset=" << vecoffs[idhs].offset << std::endl;
  std::cerr << "n[" << idhs << "].size=" << vecoffs[idhs].size << std::endl;
  std::cerr << "n[" << idhs << "].data=" << (void*)(vecoffs[idhs].data) << std::endl;
  std::cerr << std::endl;
}
      exit(1);
    }
    break;
  } while(1);
}

int ReadFiles::Run() {
  unsigned char *buf=(unsigned char*)malloc(run_.cline().Maxblocksize()*run_.cline().Maxrveclen());
  unsigned char *buf2=(unsigned char*)malloc(run_.cline().Maxblocksize());
  XrdOucIOVec *vecoffs = new XrdOucIOVec[run_.cline().Maxrveclen()];

  for(int i=0;i<run_.cline().Nfiles();i++) {

    std::string fn;
    size_t fsize;
    bool new_file = false;
    uint64_t fseq;
    int nreq = 0, nchunks = 0;

    if (randfloat() < run_.cline().Probnewfile()) {
      fseq = run_.getSeqPostIncrement();
      new_file = true;
    } else {
      std::pair<bool, uint64_t> h = run_.getHighestSeq();
      if (!h.first) {
        fseq = run_.getSeqPostIncrement();
        new_file = true;
      } else {
        fseq = choose_oldfile(h.second+1);
      }
    }
    fn = makefilename(run_.getRunNumber(), fseq, fsize);

    size_t out_len;
    unsigned int out_latency;
    std::vector<uint8_t> out_seed;
    ino_t out_inode;

    if (!parse_filename(fn, out_len, out_latency, out_seed, out_inode)) {
      std::cerr << "FATAL: Could not parse filename" << std::endl;
      exit(1);
      return -1;
    }

//    std::cerr << "Opening file " << fn << std::endl;
    auto now_open = std::chrono::system_clock::now();
    int fd = Open(fn);
    if (fd<0) {
      std::cerr << "FATA: Could not open file " << fn << std::endl;
      exit(1);
    }

    if (new_file) {
      run_.updateHighestSeq(fseq);
    }
      
    size_t lentoread = rnd_lentoread(fsize);
    size_t nread = 0;
    off_t curpos = 0;
    while(nread < lentoread) {
      size_t vec_bytelen;
      off_t final_pos;
      int nvec_elements;
      setup_vecs(buf, vecoffs, nvec_elements, vec_bytelen, final_pos, curpos, lentoread-nread, fsize);

      do_reads(fd, vecoffs, nvec_elements);
      nreq++;
      nchunks += nvec_elements;

      off_t memoff = 0;
      for(int nv=0; nv< nvec_elements; ++nv) {
        genData(buf2, vecoffs[nv].size, vecoffs[nv].offset, &out_seed[0]);
        if (memcmp(&buf[memoff], buf2, vecoffs[nv].size)) {
          std::cerr << "FATAL: +++ Bytes mismatch! on " << fn << " nv=" << nv << " +++" << std::endl;

std::cerr << "nvec_elements=" << nvec_elements << " vec_bytelen=" << vec_bytelen << " final_pos=" << final_pos << " fsize=" << fsize << std::endl;
for(int idhs=0;idhs<nvec_elements;++idhs) {
  std::cerr << "n[" << idhs << "].offset=" << vecoffs[idhs].offset << std::endl;
  std::cerr << "n[" << idhs << "].size=" << vecoffs[idhs].size << std::endl;
  std::cerr << "n[" << idhs << "].data=" << (void*)(vecoffs[idhs].data) << std::endl;
  std::cerr << std::endl;
}

          exit(1);
        }
        memoff += vecoffs[nv].size;
      }

      nread += vec_bytelen;
      curpos = final_pos;
    }
    XrdPosixXrootd::Close(fd);
    auto now_close = std::chrono::system_clock::now();

    if (run_.cline().Verbose()) {
      pid_t tid = syscall(SYS_gettid);
      std::time_t now_timet = std::chrono::system_clock::to_time_t(now_close);
      std::tm timeinfo;
      gmtime_r(&now_timet, &timeinfo);
      char tbuffer[strlen("2019-07-23T12:23:03.000Z")+1];
      std::strftime(tbuffer, sizeof(tbuffer), "%FT%T.000Z", &timeinfo);
      std::chrono::milliseconds milli = std::chrono::duration_cast<std::chrono::milliseconds>(now_close.time_since_epoch());
      {
        char pbuf[64];
        snprintf(pbuf,sizeof(pbuf),"%03ldZ",milli.count()%1000);
        strncpy(&tbuffer[20],pbuf,5);
        tbuffer[24] = '\0';
      }

      std::chrono::duration<float> dur = now_close - now_open;

      std::lock_guard<std::mutex> guard(run_.cout_mutex_);
      std::cout << tbuffer << " [" << tid << "] ";
      if (new_file) {
        std::cout << "Create ";
       } else {
        std::cout << "Reread ";
       }
       std::cout << "#" << fseq << " read=";
       std::string pfx;
       float fact;
       find_b_fact(lentoread, pfx, fact);
       std::cout << lentoread/fact << pfx << "(" << (float(lentoread)/fsize*100) << "%) ";
       std::cout << "at " << (float(lentoread)/dur.count()/1E6) << "MB/s ";
       std::cout << "blocks=" << nreq << ":" << nchunks << std::endl;
    }
  } // for() over nfiles

  delete[] vecoffs;
  free(buf2);
  free(buf);

  return 0;
}
  
int main(int argc, char **argv) {
   //XrdCl::DefaultEnv::SetLogLevel("Debug");

   CmdlineOptions cline(argc, argv);
   if (cline.error()) {
     std::cerr << "Error during command line parsing" << std::endl;
     return 1;
   }

   if (cline.help()) {
     return 0;
   }

   cline.PrintConfig();

   XrdPosixXrootd myPFS(-10*cline.Nthreads(), 10*cline.Nthreads(), 10*cline.Nthreads());

   RunState rstate(cline);

   if (cline.Verbose()) {
      std::cout << std::setprecision(3);
      std::cout << "Run number #" << rstate.getRunNumber() << std::endl;
   }

   std::vector<std::thread> thr;
   for(int n=0;n<cline.Nthreads();++n) {
     thr.emplace_back([&rstate] { ReadFiles r(rstate); r.Run(); });
   }

   for(int n=0;n<cline.Nthreads();++n) {
     thr[n].join();
   }

   return 0;
}
