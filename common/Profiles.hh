/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <cstdint>
#include <string>
#include <vector>
#include <utility>

class Profiles {
public:

  Profiles() : version(0) { }

  struct fp_matches_s {
    float prob;
    std::string ap;
  };
    
  struct fp_s {
    std::string name;
    std::string reg;
    unsigned int latency;
    std::vector<struct fp_matches_s> matches;
  };

  struct ap_s {
    std::string name;
    std::string proto;
    double fBytesRead;
    float fDuration;
    float fDurationPerMByte;
    float fProbVecRead;
    float fOverrunFactor;
    float fOverrunAbs;
    int nsimthreads;
    int ivecreadsize;
    int maxioqueue;
    int filesperlogin;
    float fSeeksPerMBIssued;
    float fSeekedFracPerSeek;
    float fReadSizeLow, fReadSizeHigh;
    std::vector<float> pdfReadSize;
    std::vector<float> cdfReadSize;
    std::vector<float> pdfSeekDistance;
    std::vector<float> cdfSeekDistance;
  };

  uint64_t getVersion() const { return version; }

  void setVersion(const uint64_t ver) { version = ver; }

  std::string getTargetHost() const { return targethost; }

  void setTargetHost(const std::string &th) { targethost = th; }

  std::string getPathPrefix() const { return pathprefix; }

  int getReqSubstreams() const { return reqsubstreams; }

  bool getUsingMockOss() const { return usingmockoss; }

  int getAccessMapSize() const { return accessmapsize; }

  void setPathPrefix(const std::string &pp) { pathprefix = pp; }

  void setReqSubstreams(const int n) { reqsubstreams = n; }

  void setUsingMockOss(const bool v) { usingmockoss = v; }

  void setAccessMapSize(const int v) { accessmapsize = v; }

  void AddToFileProfile(const std::string &name, const std::string &reg, const unsigned int latency, const std::vector<std::pair<float, std::string> > &matches);

  void AddToAccessProfile(const std::string &name, const std::string &proto, const double fBytesRead, const float fDuration,
                          const float fDurationPerMByte, const float fOverrunFactor, const float fOverrunAbs, const float fSeeksPerMBIssued, const float fSeekedFracPerSeek, const float fProbVecRead,
                          const int ivecreadsize, const int maxioqueue, const int filesperlogin, const int nsimthreads, const float readSizeLow, const float readSizeHigh,
                          const std::vector<float> &readSize, const std::vector<float> &seekDistance);

  void clear() {
    fileProfiles.clear();
    accessProfiles.clear();
  }

  const std::vector<struct fp_s> &getFileProfiles() const {
    return fileProfiles;
  }

  const std::vector<struct ap_s> &getAccessProfiles() const {
    return accessProfiles;
  }

private:
  std::vector<struct fp_s> fileProfiles;
  std::vector<struct ap_s> accessProfiles;
  std::string targethost;
  std::string pathprefix;
  bool usingmockoss;
  int accessmapsize;
  uint64_t version;
  int reqsubstreams;
};
