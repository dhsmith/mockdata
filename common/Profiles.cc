/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Profiles.hh"

void Profiles::AddToFileProfile(const std::string &name, const std::string &reg, const unsigned int latency, const std::vector<std::pair<float, std::string> > &matches) {
  struct Profiles::fp_s newFp;
  newFp.name = name;
  newFp.reg = reg;
  newFp.latency = latency;
  for(const auto &m: matches) {
    struct Profiles::fp_matches_s match;
    match.prob = m.first;
    match.ap = m.second;
    newFp.matches.push_back(match);
  }
  fileProfiles.push_back(newFp);
}


void Profiles::AddToAccessProfile(const std::string &name, const std::string &proto, const double fBytesRead, const float fDuration,
                                     const float fDurationPerMByte, const float fOverrunFactor, const float fOverrunAbs, const float fSeeksPerMBIssued, const float fSeekedFracPerSeek,
                                     const float fProbVecRead, const int ivecreadsize, const int maxioqueue, const int filesperlogin, const int nsimthreads, const float readSizeLow,
                                     const float readSizeHigh, const std::vector<float> &readSize, const std::vector<float> &seekDistance) {
  struct Profiles::ap_s newAp;
  newAp.name = name;
  newAp.proto = proto;
  newAp.fBytesRead = fBytesRead;
  newAp.fDuration = fDuration;
  newAp.fDurationPerMByte = fDurationPerMByte;
  newAp.nsimthreads = nsimthreads;
  newAp.ivecreadsize = ivecreadsize;
  newAp.maxioqueue = maxioqueue;
  newAp.filesperlogin = filesperlogin;
  newAp.fProbVecRead = fProbVecRead;
  newAp.fOverrunFactor = fOverrunFactor;
  newAp.fOverrunAbs = fOverrunAbs;
  newAp.fSeeksPerMBIssued = fSeeksPerMBIssued;
  newAp.fSeekedFracPerSeek = fSeekedFracPerSeek;
  newAp.fReadSizeLow = readSizeLow;
  newAp.fReadSizeHigh = readSizeHigh;
  double sum = 0.0;
  for(const auto &rs: readSize) {
    sum += rs;
    newAp.pdfReadSize.push_back(rs);
    newAp.cdfReadSize.push_back(sum);
  }
  if (sum == 0.0) {
    throw std::string("Invalid read-size probabily distribution");
  }
  for(auto &rsPdf: newAp.pdfReadSize) {
    rsPdf /= sum;
  }
  for(auto &rsCdf: newAp.cdfReadSize) {
    rsCdf /= sum;
  }
  sum = 0.0;
  for(const auto &sd: seekDistance) {
    sum += sd;
    newAp.pdfSeekDistance.push_back(sd);
    newAp.cdfSeekDistance.push_back(sum);
  }
  if (sum == 0.0) {
    throw std::string("Invalid Seek-distance probability distribution");
  }
  for(auto &sdPdf: newAp.pdfSeekDistance) {
    sdPdf /= sum;
  }
  for(auto &sdCdf: newAp.cdfSeekDistance) {
    sdCdf /= sum;
  }
  accessProfiles.push_back(newAp);
}
