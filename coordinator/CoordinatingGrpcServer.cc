/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "version.h"
#include "CoordinatingGrpcServer.hh"
#include "GlobalRateEstimate.hh"

static void fillFileResult(FileResult &result, const FileResultMsg &msg) {
  result.id_ = msg.id();
  result.nbissued_ = msg.nbissued();
  result.nbscalarread_ = msg.nbscalarread();
  result.nbvecread_ = msg.nbvecread();
  result.duration_ = msg.duration();
  result.residue_ = msg.residue();
  result.nseekbytes_ = msg.nseekbytes();
  result.nseeks_ = msg.nseeks();
  result.nvecreadcalls_ = msg.nvecreadcalls();
  result.nreadcalls_ = msg.nreadcalls();
  result.nvecreadchunks_ = msg.nvecreadchunks();
  result.fsize_ = msg.fsize();
  result.lastserver_ = msg.lastserver();
  result.xrdreqcallbacktime_ = msg.xrdreqcallbacktime();
  result.xrdreqcallbacktsum_ = msg.xrdreqcallbacktsum();
  result.accessprofilename_ = msg.accessprofilename();
  result.fileprofilename_ = msg.fileprofilename();
  result.overrunfactor_ = msg.overrunfactor();
  result.overrunabs_ = msg.overrunabs();
  result.nsubstreams_ = msg.nsubstreams();
  result.url_ = msg.url();
  result.intendedduration_ = msg.intendedduration();
  result.nsimthreads_ = msg.nsimthreads();
  result.lengthfactor_ = msg.lengthfactor();
  result.nbtoread_ = msg.nbtoread();

  result.accessmap_.clear();
  for(const auto v: msg.accessmap()) {
    result.accessmap_.push_back(v);
  }
}

static void fillLGInfo(LGInfo &li, const LGInfoMsg &msg) {
  li.lgidstr_ = msg.lgidstr();
  li.dutycycle_ = msg.dutycycle();
  li.nassigned_ = msg.nassigned();
  li.lgbread_ = msg.lgbread();
  li.lgbreadms_ = msg.lgbreadms();
}

CoordinatingGrpcServer::~CoordinatingGrpcServer() {
  Stop();
}

void CoordinatingGrpcServer::receivedClaim(uint64_t id, TransferRpcHandler *h, TransferReport &r) {
  const time_t t = time(0);
  LGInfo li;
  fillLGInfo(li, r.lginfo());
  GRENotifyLGRBytes(li);
  if (li.nassigned_ > 0 && li.dutycycle_ > 0.80) {
    MDLog::Log(MDLog::DEBUG, "Ignoring claim id=" + std::to_string(id) + " from " + li.lgidstr_ + " due to high dutycycle");
    return;
  }
  struct grpc_claim_info_s ci;
  const FileResultMsg &msg = r.fileresult();
  fillFileResult(ci.fileresult, msg);
  ci.lginfo = li;
  ci.startDelay = r.startdelay();
  ci.h = h;
  ci.claimtime = t;
  time_t tidx = t;
  bool tidxreset = false;
  for(auto &cm: claims_) {
    for(auto &idm: cm.second) {
      if (idm.first == id) {
        tidx = cm.first;
        tidxreset = true;
        break;
      }
    }
    if (tidxreset) break;
  }
  claims_[tidx][id].push_back(ci);
  MDLog::Log(MDLog::DEBUG, "Received claim for offer id=" + std::to_string(id) + " from lgidstr=" + li.lgidstr_);
}

void CoordinatingGrpcServer::receivedResult(uint64_t id, TransferRpcHandler *h, TransferReport &r) {
  const int rc = r.rc();
  const FileResultMsg &msg = r.fileresult();
  FileResult result;
  LGInfo li;

  fillFileResult(result, msg);
  fillLGInfo(li, r.lginfo());

  src_.notifyFileResult(result, rc, r.message(), li);
}

void CoordinatingGrpcServer::RemoveRpcTick(RpcHandler *h) {
  // make sure all unassigned claims (claims_) associated with the handler are removed
  for(auto &cm: claims_) {
    for(auto &idm: cm.second) {
      std::vector<grpc_claim_info_s> &v = idm.second;
      v.erase(std::remove_if(v.begin(), v.end(), [=](const grpc_claim_info_s &o) { return o.h == h; }), v.end());
    }
  }
  activehandlers_.erase(h);
}

void CoordinatingGrpcServer::distributeOffers(const std::list<FileEntryForClient> &l, TransferRpcHandler *handler) {
  std::set<uint64_t> ids;
  for(auto &le: l) {
    ids.insert(le.id);
  }
  // we can forget about distributions of ids not in the list.
  for(auto itr = offdist_id2handler_.begin(); itr != offdist_id2handler_.end(); ) {
    if (ids.find(itr->first) == ids.end()) {
      itr = offdist_id2handler_.erase(itr);
    } else {
      ++itr;
    }
  }
  // remove any transfer handlers which are no longer active
  for(auto itr = offdist_thandlers_.begin(); itr != offdist_thandlers_.end(); ) {
    if (activehandlers_.find(*itr) == activehandlers_.end()) {
      offdist_thandlers_.erase(itr++);
    } else {
      ++itr;
    }
  }

  // know the supplied handler is an active transfer handler
  offdist_thandlers_.insert(handler);

  // distribute the ids amongst handlers
  for(auto id: ids) {
    if (offdist_id2handler_.find(id) != offdist_id2handler_.end()) continue;
    auto itr = offdist_thandlers_.begin();
    std::advance(itr, rand() % offdist_thandlers_.size());
    offdist_id2handler_[id] = *itr;
    MDLog::Log(MDLog::DEBUG, "Distributing offer id=" + std::to_string(id) + " to lgidstr=" + (*itr)->lgidstr_);
  }
}

bool CoordinatingGrpcServer::isOfferSendable(uint64_t id, TransferRpcHandler *handler) {
  if (offdist_id2handler_.find(id) != offdist_id2handler_.end()) {
    return offdist_id2handler_[id] == handler;
  }
  return false;
}

void CoordinatingGrpcServer::assignClaims() {
  time_t t = time(0);
  std::vector<time_t> toerase;
  for (auto &p : claims_) {
    if (p.first+9 >= t) continue;
    std::map<uint64_t, std::vector<grpc_claim_info_s> > &q = p.second;
    for (auto &p2 : q) {
      const uint64_t id = p2.first;
      std::vector<grpc_claim_info_s> &v = p2.second;
      grpc_claim_info_s *ci = 0;
      if (v.size()>0) {
        const size_t xx = rand() % (v.size());
        ci = &v[xx];
      }
      if (ci && src_.notifyFileAssigned(id, ci->claimtime, ci->fileresult, ci->lginfo, ci->startDelay)) {
        ci->h->claimResult(id);
      }
    }
    toerase.push_back(p.first);
  }
  for (auto &p: toerase) {
    claims_.erase(p);
  }
}

void CoordinatingGrpcServer::Start() {
  std::string server_address("0.0.0.0:50051");
  ServerBuilder builder;

  // Listen on the given address without any authentication mechanism.
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

  // Register "service_" as the instance through which we'll communicate with
  // clients. In this case it corresponds to an *asynchronous* service.
  builder.RegisterService(&service_);

  // Get hold of the completion queue used for the asynchronous communication
  // with the gRPC runtime.
  cq_ = builder.AddCompletionQueue();

  // assemble the server.
  server_ = builder.BuildAndStart();

  // Finally spawn two RpcHandler instances to server the GetLatestConfig and GetTransfers rpc alls.
  new ConfigRpcHandler(&service_, cq_.get(), this);
  new TransferRpcHandler(&service_, cq_.get(), this);

  started_ = true;
}

void CoordinatingGrpcServer::Tick(bool &finished, const std::chrono::system_clock::time_point &deadline) {
  void* tag;  // uniquely identifies a request.
  bool ok;

  finished = false;

  // pass1: allow writes: process all pending, and wait for new until deadline
  // pass2: no writes: process all pending events (no wait for new events)

  bool intr = true;
  writequiet_ = false;

  do {

    assignClaims();

    if ( shutdownRequested() && activehandlers_.size() == 0 ) {
      Stop();
    }

    filelistsource()->Tick();

    do {

      // driving the state machine with respect to sending
      for(auto h: activehandlers_) {
        h.first->Tick();
      }

      const std::chrono::system_clock::time_point d = (intr) ? deadline : std::chrono::system_clock::now();
      ServerCompletionQueue::NextStatus ns = cq_->AsyncNext(&tag, &ok, d);
      if (ns == ServerCompletionQueue::SHUTDOWN) {
        finished = true;
        return;
      }

      if (ns == ServerCompletionQueue::TIMEOUT) {
        break;
      }

      // will drive the statemachine with respect to receiving
      int val;
      RpcHandler *p;
      static_cast<RpcHandler::tagdata*>(tag)->recycle(val, p);
      p->Proceed(val, ok);

      if (intr && std::chrono::system_clock::now() >= deadline) {
        break;
      }

    } while(1);

    if (intr) {
      // end of pass 1, do pass 2
      intr = false;
      writequiet_ = true;
      continue;
    }
    // pass2, finished
    break;

  } while(1);

}

RpcHandler::RpcHandler(LoadGenerator::AsyncService* service, ServerCompletionQueue* cq, CoordinatingGrpcServer *coord)
        : outstandingtags_(0), service_(service), cq_(cq), coordinatingserver_(coord), firstprocessincomplete_(true), processingfinished_(false) {
    // Take in the "service" instance (in this case representing an asynchronous
    // server) and the completion queue "cq" used for asynchronous communication
    // with the gRPC runtime.
}

void ConfigRpcHandler::clone() {
  new ConfigRpcHandler(service_, cq_, coordinatingserver_);
}

void TransferRpcHandler::clone() {
  new TransferRpcHandler(service_, cq_, coordinatingserver_);
}

ConfigRpcHandler::ConfigRpcHandler(LoadGenerator::AsyncService* service, ServerCompletionQueue* cq, CoordinatingGrpcServer *coord) :
            RpcHandler(service,cq,coord), responder_(&ctx_), finishsent_(false) { 
  service_->RequestGetLatestConfig(&ctx_, &request_, &responder_, cq_, cq_, maketag(0));
}

TransferRpcHandler::TransferRpcHandler(LoadGenerator::AsyncService* service, ServerCompletionQueue* cq, CoordinatingGrpcServer *coord) :
            RpcHandler(service,cq,coord), rwstream_(&ctx_),
            readoutstanding_(false), writeoutstanding_(false), finishoutstanding_(false), clientwritesdone_(false), writesdead_(false), firstreaddone_(false) { 
  service_->RequestGetTransfers(&ctx_, &rwstream_, cq_, cq_, maketag(0));
}

void ConfigRpcHandler::Process(int val, bool ok) {
  if (!firstprocessincomplete_) {
    // result of our write. We won't be called again
    processingfinished_ = true;
    return;
  }
  ConfigReply reply;
  reply.set_version(coordinatingserver_->config()->getLatestVersion());
  reply.set_targethost(coordinatingserver_->config()->profiles.getTargetHost());
  reply.set_pathprefix(coordinatingserver_->config()->profiles.getPathPrefix());
  reply.set_usingmockoss(coordinatingserver_->config()->profiles.getUsingMockOss());
  reply.set_accessmapsize(coordinatingserver_->config()->profiles.getAccessMapSize());
  reply.set_nsubstreams(coordinatingserver_->config()->profiles.getReqSubstreams());
  reply.set_mdversionstr(MOCKDATA_VERSION);

  for(const auto &fp: coordinatingserver_->config()->profiles.getFileProfiles()) {
    ConfigReply::FileProfile *fpm = reply.add_fileprofiles();
    fpm->set_name(fp.name);
    fpm->set_fileregexp(fp.reg);
    fpm->set_latency(fp.latency);
    for(const auto &match: fp.matches) {
      ConfigReply::FileProfile::AssociatedAccessProfileWeight *weight = fpm->add_associatedprofiles();
      weight->set_name(match.ap);
      weight->set_weight(match.prob);
    }
  }

  for(const auto &ap: coordinatingserver_->config()->profiles.getAccessProfiles()) {
    ConfigReply::AccessProfile *apm = reply.add_accessprofiles();
    apm->set_name(ap.name);
    apm->set_lentoread(ap.fBytesRead);
    apm->set_durationpermbyte(ap.fDurationPerMByte);
    apm->set_duration(ap.fDuration);
    apm->set_overrunfactor(ap.fOverrunFactor);
    apm->set_overrunabs(ap.fOverrunAbs);
    apm->set_nsimthreads(ap.nsimthreads);
    apm->set_vecreadsize(ap.ivecreadsize);
    apm->set_maxioqueue(ap.maxioqueue);
    apm->set_filesperlogin(ap.filesperlogin);
    apm->set_probvecread(ap.fProbVecRead);
    apm->set_seekspermbissued(ap.fSeeksPerMBIssued);
    apm->set_seekedfracperseek(ap.fSeekedFracPerSeek);
    apm->set_proto(ap.proto);
    ConfigReply::AccessProfile::PDF *pdfRs = new ConfigReply::AccessProfile::PDF();
    pdfRs->set_min(ap.fReadSizeLow);
    pdfRs->set_max(ap.fReadSizeHigh);
    for(const auto &bin: ap.pdfReadSize) {
      pdfRs->add_bin(bin);
    }
    apm->set_allocated_pdfreadsize(pdfRs);
    ConfigReply::AccessProfile::PDF *pdfSd = new ConfigReply::AccessProfile::PDF();
    for(const auto &bin: ap.pdfSeekDistance) {
      pdfSd->add_bin(bin);
    }
    apm->set_allocated_pdfseekdistance(pdfSd);
  }
  responder_.Finish(reply, Status::OK, maketag(0));
  MDLog::Log(MDLog::DEBUG, "Sent configuration to load-generator " + request_.lgidstr() + " v" + request_.mdversionstr());
}

void TransferRpcHandler::queueOfferSend() {
  std::list<FileEntryForClient> n = coordinatingserver_->filelistsource()->getListOfNextFiles();
  if (!n.empty()) {
    // remove any seen that have lower id than first of the next set
    uint64_t targetid = n.front().id;
    std::set<uint64_t>::iterator itr;
    for(itr = seen_.begin(); itr != seen_.end(); ++itr) {
      if (*itr >= targetid) break;
    }
    seen_.erase(seen_.begin(), itr);
    coordinatingserver_->distributeOffers(n, this);
    for (auto &l: n) {
      if (seen_.find(l.id) != seen_.end()) continue;
      seen_.insert(l.id);
      if (!coordinatingserver_->isOfferSendable(l.id, this)) continue;
      TransferReply reply;
      reply.set_id(l.id);
      reply.set_status(TransferReply::OFFER);
      reply.set_filename(l.filename);
      reply.set_startdelay(l.startDelay);
      reply.set_fsize(l.fsize);
      reply.set_latestconfigversion(coordinatingserver_->config()->getLatestVersion());
      rwstream_.Write(reply, maketag(TagMeta::WRITE));
      writeoutstanding_ = true;
      MDLog::Log(MDLog::DEBUG, "Sending offer for id=" + std::to_string(l.id) + " to lgidstr=" + lgidstr_ + " for filename=" + l.filename);
      break;
    }
  }
}

void TransferRpcHandler::Tick() {

  if (finishoutstanding_) {
    // just waiting for finish
    return;
  }

  if (coordinatingserver_->shutdownRequested() || (!writeoutstanding_ && !readoutstanding_)) {
    rwstream_.Finish(Status::OK, maketag(TagMeta::FINISH));
    finishoutstanding_ = true;
    return;
  }

  if (!firstreaddone_) {
    // waiting for first request
    return;
  }

  if (writesdead_) {
    // can not send anything now
    return;
  }

  if (coordinatingserver_->writeQuiet()) {
    // this leaves a poll will no outstanding write,
    // to avoid suspected read starvation
    return;
  }

  // send any outstanding claim results (assigned/claim_rejected)
  if (!writeoutstanding_ && !claim_results_.empty()) {
    uint64_t id = claim_results_.front();
    claim_results_.pop_front();
    TransferReply r;
    r.set_id(id);
    r.set_status(TransferReply::ASSIGNED);
    rwstream_.Write(r, maketag(TagMeta::WRITE));
    writeoutstanding_ = true;
    MDLog::Log(MDLog::DEBUG, "Sending claim assign id=" + std::to_string(id) + " to lgidstr=" + lgidstr_);
  }

  // send an OFFER if possible
  if (!writeoutstanding_) {
    queueOfferSend();
  }

}

void TransferRpcHandler::claimResult(uint64_t id) {
  claim_results_.push_back(id);
}

void TransferRpcHandler::Process(int val, bool ok) {

  if (firstprocessincomplete_) {
    if (!finishoutstanding_) {
      // need to get the first request.
      readoutstanding_ = true;
      rwstream_.Read(&report_, maketag(TagMeta::READ));
    }
    return;
  }

  if (val == READ) {
    readoutstanding_ = false;
    firstreaddone_ = true;
    if (!ok) {
      clientwritesdone_ = true;
    } else {
      if (report_.status() == TransferReport::CLAIM) {
        coordinatingserver_->receivedClaim(report_.id(), this, report_);
      }
      if (report_.status() == TransferReport::DONE || report_.status() == TransferReport::FAILED) {
        coordinatingserver_->receivedResult(report_.id(), this, report_);
      }
      if (report_.status() == TransferReport::JOINING) {
        lgidstr_ = report_.lginfo().lgidstr();
      }
    }
  }
  if (val == WRITE) {
    writeoutstanding_ = false;
    if (!ok) {
      writesdead_ = true;
    }
  }

  if (val == FINISH) {
    finishoutstanding_ = false;
    processingfinished_ = true;
    return;
  }

  if (finishoutstanding_) {
    return;
  }

  if (!readoutstanding_ && !clientwritesdone_) {
    readoutstanding_ = true;
    rwstream_.Read(&report_, maketag(TagMeta::READ));
  }

}

void RpcHandler::Proceed(int val, bool ok) {
  GPR_ASSERT(!processingfinished_);

  if (firstprocessincomplete_) {
    // wr're a listener
    if (ok) {
      // Make a new instance that will go back to list for more requests
      clone();
      // We've become a handler for a connection: register for ticks
      coordinatingserver_->RegisterRpcTick(this);
    } else {
      // we're a listening handler being shutdown
      processingfinished_ = true;
    }
  }

  if (!processingfinished_) {
    // process this request
    Process(val, ok);
    firstprocessincomplete_ = false;
  }

  if (processingfinished_) {
    // expect to never have another call of our tag,
    // so deallocate ourselves (RpcHandler).
    GPR_ASSERT(outstandingtags_ == 0);
    delete this;
  }
}
