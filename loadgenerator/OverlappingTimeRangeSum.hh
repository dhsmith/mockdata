/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "TimeMacros.hh"

#include <algorithm>
#include <vector>
#include <iterator>
#include <iostream>

class OverlappingTimeRangeSum {
public:

  class MyTimeRep {
  public:
    MyTimeRep() : tv_{0,0} { }
    MyTimeRep(const timespec &tv) : tv_(tv) { }
    ~MyTimeRep() { }
    bool operator<(const MyTimeRep &x) const {
      return ts_timercmp(&tv_, &x.tv_, <);
    }
    bool operator>(const MyTimeRep &x) const {
      return ts_timercmp(&tv_, &x.tv_, >);
    }
    bool zero() const {
      return (tv_.tv_sec == 0 && tv_.tv_nsec == 0);
    }

    timespec tv_;
  };

  OverlappingTimeRangeSum() : sum_(0.0), sumoverlap_(0.0) { }
  ~OverlappingTimeRangeSum() { }

  void AddRange(const MyTimeRep &start, const MyTimeRep &stop) {
    const auto ub = std::upper_bound(stops_.begin(), stops_.end(), stop);
    const auto idx = std::distance(stops_.begin(), ub);
    stops_.insert(ub, stop);
    starts_.insert(starts_.begin()+idx, start);
  }

  void Summarise() {
    Summarise(MyTimeRep());
  }

  void Summarise(const MyTimeRep &until) {
    auto ub = stops_.end();
    if (!until.zero()) {
      ub = std::upper_bound(stops_.begin(), stops_.end(), until);
    }
    const auto idx = std::distance(stops_.begin(), ub);
    if (idx == 0) {
      return;
    }
    size_t i2 = idx;
    MyTimeRep prev_start;
    while(i2>0) {
      i2--;

      float tf;
      timespec tv;
      ts_timersub(&stops_[i2].tv_, &starts_[i2].tv_, &tv);
      ts_timer2float(&tv, &tf);
      sumoverlap_ += tf;

      if (!prev_start.zero() && starts_[i2] > prev_start) {
        continue;
      }
      if (HighestInSum_ > stops_[i2]) {
        continue;
      }
      timespec sta = starts_[i2].tv_;
      if (HighestInSum_ > starts_[i2]) {
        sta = HighestInSum_.tv_;
      }
      timespec sto = stops_[i2].tv_;
      if (!prev_start.zero() && stops_[i2] > prev_start) {
        sto = prev_start.tv_;
      }
      if (ts_timercmp(&sta,&sto,>)) {
        continue;
      }
      ts_timersub(&sto, &sta, &tv);
      ts_timer2float(&tv, &tf);
      sum_ += tf;
      prev_start = starts_[i2];
    }
    HighestInSum_ = stops_[idx-1];
    starts_.erase(starts_.begin(), starts_.begin()+idx);
    stops_.erase(stops_.begin(), ub);
  }

  double GetSum() const {
    return sum_;
  }

  double GetSumWithOverlap() const {
    return sumoverlap_;
  }

  std::vector<MyTimeRep> starts_;
  std::vector<MyTimeRep> stops_;
  MyTimeRep HighestInSum_;
  double sum_;
  double sumoverlap_;
};
