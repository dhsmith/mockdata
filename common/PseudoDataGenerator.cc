/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h>

#include "PseudoDataGenerator.hh"
#include "HashUtils.hh"

bool parse_filename(const std::string fn, size_t &out_len, unsigned int &out_latency, std::vector<uint8_t> &out_seed, ino_t &out_inode) {
  std::vector<uint8_t> seed(16);
  std::string fn_name = fn, fn_leadingpath;
  size_t ridx;

  // remove double slashes
  do {
    ridx = fn_name.find("//");
    if (ridx != std::string::npos) {
      fn_name.erase(ridx,1);
    }
  } while(ridx != std::string::npos);

  // split leading path from filename
  ridx = fn_name.rfind('/', std::string::npos);
  if (ridx != std::string::npos) {
    fn_leadingpath = fn_name.substr(0,ridx);
    fn_name.erase(0,ridx+1);
  }

  // find latency start
  ridx = fn_name.rfind('_', std::string::npos);
  if (ridx == std::string::npos || ridx<2) {
    return false;
  }
  std::string latstr = fn_name.substr(ridx+1, std::string::npos);

  // find size start
  size_t ridx2 = fn_name.rfind('_', ridx-1);
  if (ridx2 == std::string::npos) {
    return false;
  }

  // parse length and latency number
  std::string lenstr = fn_name.substr(ridx2+1, ridx-ridx2-1);
  size_t pos;
  try {
    out_len = std::stoull(lenstr, &pos);
  } catch(std::exception &) {
    return false;
  }
  if ((int64_t)out_len < 0 || pos != lenstr.length()) {
    return false;
  }
  try {
    out_latency = std::stoul(latstr, &pos);
  } catch (std::exception &) {
    return false;
  }
  if (pos != latstr.length()) {
    return false;
  }

  // for the content generation seed, hash the filename without any leading path
  // but with the length

  std::string fnAndSize = fn_name;
  fn_name.erase(ridx2, std::string::npos); // filename only
  fnAndSize.erase(ridx, std::string::npos); // filename_size

  HashUtils::MurmurHash3_x64_128(fnAndSize.c_str(), fnAndSize.length(), 0x0, &seed[0]);
  out_seed = seed;

  // for the file inode, hash the filename including leading path but excluding
  // the length and latency specifiers

  std::string fn_nospecifiers = fn_leadingpath;
  if (fn_nospecifiers.length()) fn_nospecifiers += "/";
  fn_nospecifiers += fn_name;
  HashUtils::MurmurHash3_x64_128(fn_nospecifiers.c_str(),
                     fn_nospecifiers.length(),
                     0x0,
                     &seed[0]);

  out_inode = U8TO64_LE(seed);
  return true;
}

void genData16(uint8_t *buf, const size_t len, const uint64_t off, const uint8_t *seed16) {
  const uint32_t bs = 16;
  const uint64_t s1 = off % bs;
  const uint64_t s2 = off / bs;
  const uint64_t e1 = (off+len) % bs;
  const uint64_t e2 = (off+len) / bs;
  uint64_t i;
  uint8_t tmphash16[16];
  uint8_t inblock[24];
  memcpy(inblock+8,seed16,8);
  memcpy(inblock+16,seed16+8,8);
  if (e2>s2) {
    for(i=s2+1;i<e2;i++) {
         uint64_t secnum = i/(512/bs);
         uint32_t seed = i%(512/bs);
         U64TO8_LE(inblock,secnum);
         HashUtils::MurmurHash3_x64_128(inblock, 24, seed, &buf[(i-s2)*bs-s1]);
    }
    if (e1>0) {
      uint64_t secnum = e2/(512/bs);
      uint32_t seed = e2%(512/bs);
      U64TO8_LE(inblock,secnum);
      HashUtils::MurmurHash3_x64_128(inblock, 24, seed, tmphash16);
      memcpy(&buf[(e2-s2)*bs-s1],tmphash16,e1);
    }
  }
  uint64_t secnum = s2/(512/bs);
  uint32_t seed = s2%(512/bs);
  U64TO8_LE(inblock,secnum);
  HashUtils::MurmurHash3_x64_128(inblock, 24, seed, tmphash16);
  {
    size_t l = bs - s1;
    if (l>len) l = len;
    memcpy(buf,&tmphash16[s1],l);
  }
}

void genData512(uint8_t *buf, const size_t len, const uint64_t off, const uint8_t *seed16) {
  const uint32_t bs = 512;
  const uint64_t s1 = off % bs;
  const uint64_t s2 = off / bs;
  const uint64_t e1 = (off+len) % bs;
  const uint64_t e2 = (off+len) / bs;
  uint64_t i;
  uint8_t tmphash512[512];
  uint64_t seed8 = U8TO64_LE(seed16);
  uint64_t seed8_2 = U8TO64_LE(seed16+8);
  if (e2>s2) {
    for(i=s2+1;i<e2;i++) {
         HashUtils::MurmurHash3_x64_512B(i, seed8, seed8_2, &buf[(i-s2)*bs-s1]);
    }
    if (e1>0) {
      HashUtils::MurmurHash3_x64_512B(e2, seed8, seed8_2, tmphash512);
      memcpy(&buf[(e2-s2)*bs-s1],tmphash512,e1);
    }
  }
  HashUtils::MurmurHash3_x64_512B(s2, seed8, seed8_2, tmphash512);
  {
    size_t l = bs - s1;
    if (l>len) l = len;
    memcpy(buf,&tmphash512[s1],l);
  }
}

void genData(uint8_t *buf, const size_t len, const uint64_t off, const uint8_t *seed16) {
  if (len<512) {
    genData16(buf, len, off, seed16);
    return;
  }
  genData512(buf, len, off, seed16);
}
