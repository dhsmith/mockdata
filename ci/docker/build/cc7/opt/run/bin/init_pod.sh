# This file must be sourced from another shell script
# . /opt/run/bin/init_pod.sh

dnsip=10.254.0.10
cat <<EOF > /etc/resolv.conf
search ${MY_NAMESPACE}.svc.cluster.local svc.cluster.local cluster.local
nameserver $dnsip
options ndots:5
EOF

LOGMOUNT=/mnt/logs

PV_PATH=""

if [ "-${MY_CONTAINER}-" != "--" ]; then
  PV_PATH="${LOGMOUNT}/${MY_NAME}/${MY_CONTAINER}"
else
  PV_PATH="${LOGMOUNT}/${MY_NAME}"
fi
mkdir -p ${PV_PATH}

echo "Copying initial /var/log content to ${PV_PATH}"
cd /var/log
tar -c . | tar -C ${PV_PATH} -xv

echo "Mounting logs volume ${PV_PATH} in /var/log"
mount --bind ${PV_PATH} /var/log

# all core dumps will go there as all the pods AND kubelet are sharing the same kernel.core_pattern
mkdir /var/log/tmp
chmod 1777 /var/log/tmp
echo '/var/log/tmp/%h-%t-%e-%p-%s.core' > /proc/sys/kernel/core_pattern

echo -n "Fixing reverse DNS for $(hostname) for xrootd: "
sed -i -c "s/^\($(hostname -i)\)\s\+.*$/\1 $(hostname -s).$(grep search /etc/resolv.conf | cut -d\  -f2) $(hostname -s)/" /etc/hosts
echo "DONE"

# Not needed anymore, keep it in case it comes back
echo -n "Yum should resolve names using IPv4 DNS: "
echo "ip_resolve=IPv4" >> /etc/yum.conf
echo "DONE"

# defining CI_CONTEXT
# possible values are "systemd" and "nosystemd"
# this is just to understand if the container is managed through systemd or not
CI_CONTEXT="nosystemd"
if [ "-$(cat /proc/1/cmdline 2>&1 | sed -e 's/\x0//g;s/init.*/init/')-" == '-/usr/sbin/init-' ]; then
  CI_CONTEXT="systemd"
fi
