/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "LGMessages.hh"
#include "IOHandling.hh"
#include "GetterContext.hh"

void LGMessages::FillFileResult_Joining(TransferReport &report, const int nsubstreams) {
  FileResultMsg &msg = *report.mutable_fileresult();
  msg.set_nsubstreams(nsubstreams);
}

void LGMessages::FillLGInfo(TransferReport &report, GetterContext *gc) {
  LGInfoMsg &li = *report.mutable_lginfo();

  uint64_t nassigned;
  {
    std::lock_guard<std::mutex> lcg(gc->assignedMtx_);
    nassigned = gc->assigned_.size();
  }

  float dutycycle = 0.0f;
  if (nassigned>0) {
    std::lock_guard<std::mutex> lck(gc->dutycycleMtx_);
    dutycycle = gc->curr_dutycycle_;
  }

  li.set_lgidstr(gc->LGIdStr_);
  li.set_dutycycle(dutycycle);
  li.set_nassigned(nassigned);
  uint64_t ms;
  li.set_lgbread(outstandingIo_s::getReadBytes(&ms));
  li.set_lgbreadms(ms);
}

void LGMessages::FillFileResult_Claim(TransferReport &report, const uint64_t id, const float duration, const uint64_t toread, const struct Profiles::fp_s *fileprofile,  const struct Profiles::ap_s *accessprofile) {
  FileResultMsg &msg = *report.mutable_fileresult();

  msg.set_id(id);
  msg.set_intendedduration(duration);
  msg.set_accessprofilename(accessprofile->name);
  msg.set_fileprofilename(fileprofile->name);
  msg.set_overrunfactor(accessprofile->fOverrunFactor);
  msg.set_overrunabs(accessprofile->fOverrunAbs);
  msg.set_nsubstreams(accessprofile->maxioqueue);
  msg.set_nsimthreads(accessprofile->nsimthreads);
  msg.set_lengthfactor(accessprofile->fBytesRead);
  msg.set_nbtoread(toread);
}

void LGMessages::FillFileResult_Finished(TransferReport &report, const FileResult &result) {
  FileResultMsg &msg = *report.mutable_fileresult();

  msg.set_id(result.id_);
  msg.set_nbissued(result.nbissued_);
  msg.set_nbscalarread(result.nbscalarread_);
  msg.set_nbvecread(result.nbvecread_);
  msg.set_duration(result.duration_);
  msg.set_residue(result.residue_);
  msg.set_nseekbytes(result.nseekbytes_);
  msg.set_nseeks(result.nseeks_);
  msg.set_nvecreadcalls(result.nvecreadcalls_);
  msg.set_nreadcalls(result.nreadcalls_);
  msg.set_fsize(result.fsize_);
  msg.set_lastserver(result.lastserver_);
  msg.set_xrdreqcallbacktime(result.xrdreqcallbacktime_);
  msg.set_xrdreqcallbacktsum(result.xrdreqcallbacktsum_);
  msg.set_accessprofilename(result.accessprofilename_);
  msg.set_fileprofilename(result.fileprofilename_);
  msg.set_overrunfactor(result.overrunfactor_);
  msg.set_overrunabs(result.overrunabs_);
  msg.set_nsubstreams(result.nsubstreams_);
  msg.set_url(result.url_);
  msg.set_intendedduration(result.intendedduration_);
  msg.set_nsimthreads(result.nsimthreads_);
  msg.set_nvecreadchunks(result.nvecreadchunks_);
  msg.set_lengthfactor(result.lengthfactor_);
  msg.set_nbtoread(result.nbtoread_);

  for(const auto v: result.accessmap_) {
    msg.add_accessmap(v);
  }
}
