/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "LGMessages.hh"
#include "common/Profiles.hh"

// forward dec
struct GetterContext;

#include <cstdint>
#include <string>
#include <vector>
#include <time.h>

namespace LGUtil {

std::string ByteMismatchInfo(const uint64_t foffset, const void *badp, const uint8_t *goodp, const uint32_t len);

uint64_t gcd(uint64_t a, uint64_t b);

void ChooseXrdLogin(GetterContext *gc, const std::string &newLogin, int filesPerLogin, std::string &result);

std::string make_xrdcl_username(const uint64_t fid, const uint16_t cseq);

double InverseCDFLookup(const std::vector<float> &cdf, const double yval, const double xmin, const double xmax);

double ForwardCDFLookup(const std::vector<float> &cdf, const double xval, const double xmin, const double xmax);

void CountFileAccessStats(std::vector<uint64_t> &stats, const uint64_t flen, const uint64_t off, const uint64_t len);

int shorten_to_avoid_overlap(const uint64_t offset, uint64_t &length, std::vector<uint64_t> &lower, std::vector<uint64_t> &upper);

double randdouble();

void randidshuffle(std::vector<uint64_t> &v);

void CalcNextOffset(uint64_t &newOffset, const uint64_t lastOffset, const uint64_t flen, const uint64_t rlen,
                    const float seekperissuedbyte, const float seekbytesperseek, const uint64_t nbseeks, const uint64_t nbissued,
                    const uint64_t tot2issue, const uint64_t gcd, const struct Profiles::ap_s *ap, LGMessages::seekFeedback_s &fb);

void CalcNextReadSizeAndTime(uint64_t &nextRSize, const uint64_t nRead, const uint64_t nRemain, const uint64_t nTot, const uint64_t nFsize,
                    const float totalXrdClientTime, const struct timespec &now, struct timespec &tvnext, const struct timespec &tvstart,
                    const struct timespec &tvtargetend, const struct Profiles::ap_s *ap);

void ChooseProfiles(GetterContext *gc, const LGMessages::req_s &req, const struct Profiles::fp_s **fpp, const struct Profiles::ap_s **app);

} // namespace
