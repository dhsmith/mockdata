/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <getopt.h>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

class CmdlineOptions {
public:
  CmdlineOptions(int argc, char **argv) : m_loop(false), m_skipuntil(0), m_filelist("filelist.txt"), m_configfn("config.yaml"),
                                          m_timescale(1.0), m_smooth(false), m_smooth_rstart(0.0f), m_smooth_rend(0.0f),
                                          m_smooth_duration(0) {
    optind = 1;
    m_errflg = m_help = 0;
    int c;
    static struct option longopts[] = {
       { NULL, 0, NULL, '\0' }
    };
    while ((c = getopt_long(argc, argv, "hf:rs:l:c:",longopts, NULL))!= -1) {
       switch (c) {
       case 'h':
         usage();
         m_help = 1;
         break;
       case 'f':
         {
           try {
             std::stringstream ss(optarg);
             std::string token;
             std::vector<std::string> toks;
             while(std::getline(ss, token, ',')) {
               toks.push_back(token);
             }
             if (toks.size() == 1) {
               m_timescale = std::stod(toks[0]);
             } else if (toks.size() == 3) {
               m_smooth = true;
               m_smooth_rstart = std::stof(toks[0])*1E6;
               m_smooth_rend = std::stof(toks[1])*1E6;
               m_smooth_duration = std::stoi(toks[2]);
             } else {
               throw "Invalid option f format";
             }
           } catch(std::exception &) {
             m_errflg++;
           }
           break;
         }
       case 'r':
         m_loop = true;
         break;
       case 'l':
         m_filelist = optarg;
         break;
       case 's':
         m_skipuntil = std::stoi(optarg);
         break;
       case 'c':
         m_configfn = optarg;
         break;
       case '?': /* Unrecognized option */
       default:
         {
           usage();
           m_errflg++;
           break;
         }
       } //switch
       if (m_errflg || m_help) return;
    } //while getopts

  } //constructor

  void usage() {
    std::cout << "Usage: ";
    std::cout << "-h | [-r] [-f <scaling factor>|<smoothed rate low, high, duration>] [-s <skip until time>] [-l <filelist name>] [-c <config name>]" << std::endl;
  }

  void PrintConfig() {
    std::cout << "+++++++++++++++++++++++++" << std::endl;
    if (m_smooth) {
      std::cout << "Smoothing mode: rateMBs=" + std::to_string(m_smooth_rstart/1E6) + "/" + std::to_string(m_smooth_rend/1E6) + " duration=" + std::to_string(m_smooth_duration) << std::endl;
    } else {
      std::cout << "Time scaling factor: " << m_timescale << std::endl;
    }
    std::cout << "Continuous repated replay of file list: " << (m_loop ? "yes" : "no") << std::endl;
    std::cout << "Skip until time: " << m_skipuntil << std::endl;
    std::cout << "Filelist filename: " << m_filelist << std::endl;
    std::cout << "Configuration filename: " << m_configfn << std::endl;
    std::cout << "+++++++++++++++++++++++++" << std::endl;
  }

  double GetTimeScaling() const { return m_timescale; }
  time_t GetSkipUntil() const { return m_skipuntil; }
  std::string GetFilelistFn() const { return m_filelist; }
  std::string GetConfigFn() const { return m_configfn; }
  bool smoothingModeParams(float &rstart, float &rend, time_t &duration) const {
    if (!m_smooth) {
      return false;
    }
    rstart = m_smooth_rstart;
    rend = m_smooth_rend;
    duration = m_smooth_duration;
    return true;
  }

  bool error() const { return m_errflg; }
  bool help() const { return m_help; }
  bool loop() const { return m_loop; }
  
private:
  int m_errflg;
  int m_help;
  bool m_loop;
  time_t m_skipuntil;
  std::string m_filelist;
  std::string m_configfn;
  double m_timescale;
  bool m_smooth;
  float m_smooth_rstart;
  float m_smooth_rend;
  time_t m_smooth_duration;
};
