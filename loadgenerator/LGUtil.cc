/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "LGUtil.hh"
#include "GetterContext.hh"

#include <mutex>
#include <random>

// internal
static std::random_device rd_;
static std::mutex prngmtx_;
static std::mt19937 gen_(rd_());
static std::uniform_real_distribution<> unif_d_(0.0, 1.0);

std::string LGUtil::ByteMismatchInfo(const uint64_t foffset, const void *badp, const uint8_t *goodp, const uint32_t len) {
  if (len == 0) {
    return "No bytes to compare";
  }
  char pb[3];
  const uint8_t *p = (uint8_t*)badp;
  uint32_t nmis = 0, bidx = len;
  for(uint32_t i=0;i<len;++i) {
    if (p[i] != goodp[i]) {
      nmis++;
      if (bidx==len) {
        bidx = i;
      }
    }
  }
  if (bidx == len) {
    return "No mismatch";
  }
  std::string msg;
  msg = "First chunk with mismatch has " + std::to_string(nmis) + " of " + std::to_string(len);
  msg += " bytes mismatched. First is at fileoffset " + std::to_string(foffset+bidx);
  msg += " buffer offset " + std::to_string(bidx);
  msg += " sequence starting at first mismatch (received) ";
  const uint32_t plen = std::min(128U,len-bidx);
  for(uint32_t i=0;i<plen;++i) {
    snprintf(pb, sizeof(pb), "%02x", p[bidx+i]);
    msg += std::string(pb);
  }
  msg += " (calculated) ";
  for(uint32_t i=0;i<plen;++i) {
    snprintf(pb, sizeof(pb), "%02x", goodp[bidx+i]);
    msg += std::string(pb);
  }
  return msg;
}

// Recursive function to return gcd of a and b using the Euclidean algorithm
uint64_t LGUtil::gcd(uint64_t a, uint64_t b) { 
  if (b == 0) {
    return a;
  }
  return gcd(b, a % b);
}

void LGUtil::ChooseXrdLogin(GetterContext *gc, const std::string &newLogin, int filesPerLogin, std::string &result) {

  std::lock_guard<std::mutex> lck(gc->loginMapMtx_);

  GetterContext::login_id_s &lid = gc->loginMap_[filesPerLogin];
  lid.useCount++;
  if (lid.useCount>filesPerLogin || !lid.nameset) {
    lid.loginName = newLogin;
    lid.nameset = true;
    lid.useCount = 1;
  }

  result = lid.loginName;
}

std::string LGUtil::make_xrdcl_username(const uint64_t fid, const uint16_t cseq) {
  std::string s(8,'0');
  char c;
  uint64_t x;

  x = cseq;
  c = x%52;
  x /= 52;
  if (c<26) s[0] = 'a' + c;
  else s[0] = 'A' + (c-26);

  c = x%62;
  if (c<26) s[1] = 'a' + c;
  else if (c<52) s[1] = 'A' + (c-26);
  else s[1] = '0' + (c-52);

  x = fid;
  c = x%10;
  x /= 10;
  s[7] = '0' + c;

  c = x%10;
  x /= 10;
  s[6] = '0' + c;

  c = x%10;
  x /= 10;
  s[5] = '0' + c;

  c = x%52;
  x /= 52;
  if (c<26) s[4] = 'a' + c;
  else s[4] = 'A' + (c-26);

  c = x%62;
  x /= 62;
  if (c<26) s[3] = 'a' + c;
  else if (c<52) s[3] = 'A' + (c-26);
  else s[3] = '0' + (c-52);

  c = x%62;
  if (c<26) s[2] = 'a' + c;
  else if (c<52) s[2] = 'A' + (c-26);
  else s[2] = '0' + (c-52);

  return s;
}

// Given a CDF with X-axis range  xmin and xmax, return the x value correspoding to yval
//
// 0 <= yval <= 1
// xmin <= returnedVal <= xmax
//
double LGUtil::InverseCDFLookup(const std::vector<float> &cdf, const double yval, const double xmin, const double xmax) {
  double factor = 0.0;

  for(size_t i=0;i<cdf.size();i++) {
    double binlow = 0.0, binhigh = cdf[i];
    if (i>0) {
      binlow = cdf[i-1];
    }
    if (yval<=binhigh) {
      double iF = i;
      if (binhigh>binlow) {
        iF += (yval-binlow)/(binhigh-binlow);
      }
      factor = xmin + (xmax - xmin)/(cdf.size()) * iF;
      break;
    }
  }
  return factor;
}

// Returns the y-axis value corresonding to xval
//
// 0 <= returnedVal <= 1
// xmin <= xval <= xmax
//
double LGUtil::ForwardCDFLookup(const std::vector<float> &cdf, const double xval, const double xmin, const double xmax) {
  double ret;

//std::cerr << "ForwardCDFLookup for xval=" << xval << " xmin=" << xmin << " xmax=" << xmax << std::endl;

  double iF = (xval - xmin)/((xmax-xmin)/cdf.size());
  size_t i = iF;
  iF -= i;
  ret = 0.0;
  if (i>0) {
    ret = cdf[i-1];
  }
  ret += (cdf[i]-ret) * iF;
  return ret;
}

void LGUtil::CountFileAccessStats(std::vector<uint64_t> &stats, const uint64_t flen, const uint64_t off, const uint64_t len) {
  size_t binwidth = std::ceil((float)flen / stats.size());
//std::cerr << "flen=" << flen << " off=" << off << " len=" << len << " binwidth=" << binwidth << std::endl;
  size_t sbin = off/binwidth;
  size_t ebin = (off+len-1)/binwidth;

  stats[sbin] += ((sbin+1)*binwidth - off);
  if (sbin == ebin) {
    stats[ebin] -= ((ebin+1)*binwidth - (len+off));
  } else {
    stats[ebin] += ((len+off) - ebin*binwidth);
  }
  for(size_t i=sbin+1; i<ebin; ++i) {
    stats[i] += binwidth;
  }
}

// Maintains two vectors of sorted range start in           lower
//                                 range end (inclusive) in upper
//
// Add new range [offset,offset+length) to the two vectors
// adjusting length, if possible, so that there is no overlap with existing ranges
//
// Returns 0: New range added, no need to adjust length
//         1: New range added, length was adjusted
//        -1: New range not added, start is already contained in a range
//
int LGUtil::shorten_to_avoid_overlap(const uint64_t offset, uint64_t &length, std::vector<uint64_t> &lower, std::vector<uint64_t> &upper) {

  assert(length>0);

  // find end of first range which ends at or after our start
  auto lb1 = std::lower_bound(upper.begin(), upper.end(), offset);

  if (lb1 == upper.end()) {
    // no range ending beyond us
    lower.push_back(offset);
    upper.push_back(offset+length-1);
    return 0;
  }

  // find start 'x' the range beyond us
  const auto diff = std::distance(upper.begin(), lb1);
  const uint64_t x = lower[diff];

  if (offset>=x) {
    // our start is within the range: we can not adjust our length so we don't overlap
    return -1;
  }

  // possibly shorten our length so we don't overlap with start of next range
  const uint64_t new_length = std::min(length, x - offset);

  // record our range
  lower.insert(lower.begin()+diff, offset);
  upper.insert(upper.begin()+diff, offset+new_length-1);

  if (new_length < length) {
    // we had to shorten
    length = new_length;
    return 1;
  }
  
  return 0;
}

double LGUtil::randdouble() {
  std::lock_guard<std::mutex> lck(prngmtx_);
  return unif_d_(gen_);
}

void LGUtil::randidshuffle(std::vector<uint64_t> &v) {
  std::lock_guard<std::mutex> lck(prngmtx_);
  std::shuffle(v.begin(), v.end(), gen_);
}
