/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "version.h"
#include "XrdMockData.hh"
#include "XrdMockDataTrace.hh"

#include "common/HashUtils.hh"
#include "common/PseudoDataGenerator.hh"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>
#include <unistd.h>

#include <thread>

#include <XrdOuc/XrdOucEnv.hh>
#include <XrdOuc/XrdOucTrace.hh>
#include <XrdSec/XrdSecEntity.hh>
#include <XrdSys/XrdSysPthread.hh>
#include <XrdSfs/XrdSfsAio.hh>
#include <XrdVersion.hh>

namespace mockdata {
  template<typename T, typename... Ts> std::unique_ptr<T> make_unique(Ts&&... params) {
    return std::unique_ptr<T>(new T(std::forward<Ts>(params)...));
  }
}

XrdVERSIONINFO(XrdOssGetStorageSystem,XrdMockDataOss)

/******************************************************************************/
/*                  E r r o r   R o u t i n g   O b j e c t                   */
/******************************************************************************/

namespace MockDataOss {
   XrdSysError Say(0, "mockdataoss_");
   XrdOucTrace Trace(&Say);
}

using namespace MockDataOss;

const char *XrdMockDataOssErrorText[] =
      {XRDMOCKDATAOSS_T8001,
       XRDMOCKDATAOSS_T8002,
       XRDMOCKDATAOSS_T8003,
       XRDMOCKDATAOSS_T8004};

XrdMockDataOssDir::~XrdMockDataOssDir() {
}

int XrdMockDataOssDir::Close(long long *retsz) {
   EPNAME("Close");

   DEBUG_TID("closed");
   return 0;
}

int XrdMockDataOssDir::Opendir(const char *path, XrdOucEnv &Info) {
   EPNAME("Opendir");

   return -ENOTDIR;
}

int XrdMockDataOssDir::Readdir(char *buff, int blen) {
   EPNAME("Readdir");
   (void)buff; (void)blen;

   return -ENOTDIR;
}

XrdMockDataOssFile::~XrdMockDataOssFile() {
}

int XrdMockDataOssFile::Fstat(struct stat *st) {
   EPNAME("Fstat");

   if (m_fd) {
     memset(st,0,sizeof(struct stat));
     st->st_mode = S_IFREG | 0444;
     st->st_ino = m_fd->inode();
     st->st_dev = st->st_rdev = 0;
     st->st_nlink = 1;
     st->st_uid = 100;
     st->st_gid = 100;
     st->st_size = m_fd->filesize();
     st->st_atim = st->st_mtim = st->st_ctim = {0,0};
     st->st_blksize = 4096;
     st->st_blocks = st->st_size ? ((st->st_size-1)/4096 + 1)*8 : 0;
     return 0;
   }

   return -ENOENT;
}

int XrdMockDataOssFile::Fsync() {
   EPNAME("Fsync");

   return 0;
}

int XrdMockDataOssFile::Fsync(XrdSfsAio *aiop) {
   aiop->Result = this->Fsync();
   aiop->doneWrite();
   return 0;
}

int XrdMockDataOssFile::Ftruncate(unsigned long long len) {
   EPNAME("Ftruncate");
   return -ENOTSUP;
}

int XrdMockDataOssFile::getFD() {
   EPNAME("getFD");

   return -1;
}

off_t XrdMockDataOssFile::getMmap(void **addr) {
   if (addr) *addr = 0;
   return 0;
}

int XrdMockDataOssFile::isCompressed(char *cxidp) {
   (void)cxidp;
   return 0;
}

ssize_t XrdMockDataOssFile::Read(off_t off, size_t sz) {
   EPNAME("Read");

   DEBUG_TID("return 0 (preread)");
   return 0;
}

ssize_t XrdMockDataOssFile::Read(void *p, off_t off, size_t sz) {
   EPNAME("Read");

   if (m_fd->latency() != 0) {
     usleep((useconds_t)(m_fd->latency() * 1E3));
   }

   return ReadNoDelay(p, off, sz);
}

ssize_t XrdMockDataOssFile::ReadNoDelay(void *p, off_t off, size_t sz) {
   EPNAME("ReadNoDelay");

   if (off<0 || (size_t)off >= m_fd->filesize()) {
     return 0;
   }

   size_t avail = sz;
   if (off + sz > m_fd->filesize()) {
     avail = m_fd->filesize() - off;
   }
   genData((uint8_t*)p, avail, off, m_fd->seed());

   return avail;
}

void XrdMockDataOssFile::delayAioRead(XrdSfsAio *aiop) {
   usleep((useconds_t)(m_fd->latency() * 1E3));
   aiop->Result = this->ReadNoDelay((void*)aiop->sfsAio.aio_buf,
      (off_t)aiop->sfsAio.aio_offset,
      (size_t)aiop->sfsAio.aio_nbytes);

   aiop->doneRead();
}

int XrdMockDataOssFile::Read(XrdSfsAio *aiop) {
   if (m_fd->latency() != 0) {
     std::thread thr(&XrdMockDataOssFile::delayAioRead, this, aiop);
     thr.detach();
   } else {
     // execute in a synchronous fashion
     aiop->Result = this->Read((void*)aiop->sfsAio.aio_buf,
        (off_t)aiop->sfsAio.aio_offset,
        (size_t)aiop->sfsAio.aio_nbytes);
     aiop->doneRead();
   }
   return 0;
}

ssize_t XrdMockDataOssFile::ReadRaw(void *p, off_t off, size_t sz) {
   return this->Read(p,off,sz);
}

ssize_t XrdMockDataOssFile::ReadV(XrdOucIOVec *readV, int n) {
   ssize_t nbytes = 0, curCount = 0;

   if (m_fd->latency() != 0) {
     usleep((useconds_t)(m_fd->latency() * 1E3));
   }

   for (int i=0; i<n; i++) {
      curCount = ReadNoDelay((void *)readV[i].data,
                             (off_t)readV[i].offset,
                             (size_t)readV[i].size);
      if (curCount != readV[i].size) {
         if (curCount < 0) {
           return curCount;
         }
         return -ESPIPE;
      }
      nbytes += curCount;
   }
   return nbytes;
}

ssize_t XrdMockDataOssFile::Write(const void *p, off_t off, size_t sz) {
   EPNAME("Write");

   return -ENOTSUP;
}

int XrdMockDataOssFile::Write(XrdSfsAio *aiop) {
   return -ENOTSUP;
}

ssize_t XrdMockDataOssFile::WriteV(XrdOucIOVec *writeV, int n) {
   return XrdOssDF::WriteV(writeV, n);
}

int XrdMockDataOssFile::Fchmod(mode_t mode) {
   EPNAME("Fchmod");

   return -ENOTSUP;
}

int XrdMockDataOssFile::Open(const char *fn, int Oflag, mode_t mode, XrdOucEnv &env) {
   EPNAME("Open");

   if (m_fd) {
     DEBUG_TID("Already open");
     return -XRDMOCKDATAOSS_E8003;
   }

   if (Oflag & (O_WRONLY | O_RDWR)) {
     return -EROFS;
   }

   size_t len;
   unsigned int latency;
   std::vector<uint8_t> seed;
   ino_t inode;

   bool res = parse_filename(fn, len, latency, seed, inode);
   if (!res) {
     return -ENOENT;
   }

   m_fd = mockdata::make_unique<FileDescriptor>(len, latency, seed, inode);

   return 0;
}

int XrdMockDataOssFile::Close(long long *retsz) {
   EPNAME("Close");

   if (!m_fd) {
     return -EBADF;
   }
   m_fd.reset();
   return 0;
}

int XrdMockDataOss::Chmod(const char *path, mode_t mode, XrdOucEnv *eP) {
   EPNAME("Chmod");
   DEBUG("EINVAL");
   return -EINVAL;
}

int XrdMockDataOss::Create(const char *tident, const char *path, mode_t access_mode,
      XrdOucEnv &env, int Opts) {
   EPNAME("Create");

   return -ENOTSUP;
}

int XrdMockDataOss::ConfigProc(XrdSysError &Eroute, const char *configfn) {
   char *var;
   int cfgFD, retc, NoGo = 0;
   XrdOucEnv myEnv;
   XrdOucStream Config(&Eroute, getenv("XRDINSTANCE"), &myEnv, "=====> ");

   if (!configfn || !*configfn) {
      Eroute.Say("Config warning: config file not specified; "
            "defaults assumed.");
      return 0;
   }

   if ((cfgFD = open(configfn, O_RDONLY, 0)) < 0) {
      Eroute.Emsg("Config", errno, "open config file", configfn);
      return 1;
   }
   Config.Attach(cfgFD);

   while((var = Config.GetMyFirstWord())) {
   }

   if ((retc = Config.LastError())) {
      NoGo = Eroute.Emsg("Config", retc, "read config file",
         configfn);
   }
   Config.Close();

   return NoGo;
}

int XrdMockDataOss::Init(XrdSysLogger *lp, const char *configfn) {

   Say.logger(lp);

   XrdSysError_Table *ETab = new XrdSysError_Table(XRDMOCKDATAOSS_EBASE,
               XRDMOCKDATAOSS_ELAST, XrdMockDataOssErrorText);
   Say.addTable(ETab);

   Say.Say("This is XrdMockDataOss "
           MOCKDATA_VERSION
           " compiled with xroot "
           XrdCompileVersion );

   Trace.What = 0;
   //Trace.What = CommonConfig.OssTraceLevel;

   int NoGo = ConfigProc(Say, configfn);
   if (NoGo) return NoGo;

   return 0;
}

int XrdMockDataOss::Mkdir(const char *path, mode_t mode, int mkpath,
      XrdOucEnv *envP) {
   EPNAME("Mkdir");
   DEBUG("EINVAL");
   return -EINVAL;
}

int XrdMockDataOss::Remdir(const char *, int Opts, XrdOucEnv *eP) {
   EPNAME("Remdir");
   DEBUG("EINVAL");
   return -EINVAL;
}

int XrdMockDataOss::Rename(const char *, const char *, XrdOucEnv *eP1,
      XrdOucEnv *eP2) {
   EPNAME("Rename");
   DEBUG("EINVAL");
   return -EINVAL;
}

int XrdMockDataOss::Stat(const char *fn, struct stat *st, int opts, XrdOucEnv *eP) {
   EPNAME("Stat");

   size_t len;
   unsigned int latency;
   std::vector<uint8_t> seed;
   ino_t inode;

   bool res = parse_filename(fn, len, latency, seed, inode);
   if (!res) {
     return -ENOENT;
   }

   memset(st,0,sizeof(struct stat));
   st->st_mode = S_IFREG | 0444;
   st->st_ino = inode;
   st->st_dev = st->st_rdev = 0;
   st->st_nlink = 1;
   st->st_uid = 100;
   st->st_gid = 100;
   st->st_size = len;
   st->st_atim = st->st_mtim = st->st_ctim = {0,0};
   st->st_blksize = 4096;
   st->st_blocks = st->st_size ? ((st->st_size-1)/4096 + 1)*8 : 0;

   return 0;
}

int XrdMockDataOss::StatFS(const char *path, char *buff, int &blen, XrdOucEnv *eP) {
   EPNAME("StatFS");

   return -ENOTSUP;
}

int XrdMockDataOss::StatXA(const char *path, char *buff, int &blen, XrdOucEnv *eP) {
   EPNAME("StatXA");

   return -ENOTSUP;
}

int XrdMockDataOss::StatLS(XrdOucEnv &env, const char *cgrp, char *buff, int &blen) {
   EPNAME("StatLS");

   return -ENOTSUP;
}

int XrdMockDataOss::Unlink(const char *fn, int Opts, XrdOucEnv *eP) {
   EPNAME("Unlink");

   return -ENOTSUP;
}

int XrdMockDataOss::Truncate(const char *path, unsigned long long size,
      XrdOucEnv *eP) {
   EPNAME("Truncate");
   return -ENOTSUP;
}

int XrdMockDataOss::StatXP(const char *path, unsigned long long &attr,
      XrdOucEnv *eP) {
   EPNAME("StatXP");
   DEBUG("StatXP");
   return 0;
}

int XrdMockDataOss::Stats(char *buff, int blen) {
   //EPNAME("Stats");
   static const char statfmt1[] =
      "<stats id=\"mockdataoss\" v=\"" MOCKDATA_VERSION 
      "/" XrdCompileVersion "/" __TIME__ "/" __DATE__ "\">";
   static const char statfmt2[] = "</stats>";
   static const int  statflen = sizeof(statfmt1) + sizeof(statfmt2) - 1;
   char *bp = buff;

// If only size wanted, return what size we need
//
   if (!buff) {
      int ex = 0;
      return statflen + ex;
   }

   // Make sure we have enough space
   //
   if (blen < statflen) return 0;
   strcpy(bp, statfmt1);
   bp += sizeof(statfmt1)-1; blen -= sizeof(statfmt1)-1;

   // Add trailer
   //
   if (blen >= (int)sizeof(statfmt2)) {
      strcpy(bp, statfmt2);
      bp += sizeof(statfmt2)-1;
      blen -= sizeof(statfmt2)-1;
   }

   return bp - buff;
}

int XrdMockDataOss::Lfn2Pfn(const char *Path, char *buff, int blen) {
   return -ENOENT;
}

const char *XrdMockDataOss::Lfn2Pfn(const char *Path, char *buff, int blen,
      int &rc) {
   rc = Lfn2Pfn(Path, buff, blen);
   if (rc) { return 0; }
   return buff;
}


// -------------------------

XrdOss *XrdOssGetStorageSystem(XrdOss *native_oss, XrdSysLogger *Logger,
   const char *config_fn, const char *parms) {
   //EPNAME("XrdOssGetStorageSystem");

   XrdMockDataOss *myoss = new XrdMockDataOss(native_oss, (char *) parms);
   if (myoss) {
      if (myoss->Init(Logger, config_fn)) {
         delete myoss;
         myoss = 0;
      }
   }

   return myoss;
}
