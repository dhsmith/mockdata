/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

//-----------------------------------------------------------------------------
// MurmurHash3 was written by Austin Appleby, and is placed in the public
// domain. The author hereby disclaims copyright to this source code.

// Note - The x86 and x64 versions do _not_ produce the same results, as the
// algorithms are optimized for their respective platforms. You can still
// compile and run any of them on any platform, but your performance with the
// non-native version will be less than optimal.

#include "HashUtils.hh"

//-----------------------------------------------------------------------------
// Platform-specific functions and macros

#ifdef __GNUC__
#define FORCE_INLINE __attribute__((always_inline)) inline
#else
#define FORCE_INLINE inline
#endif

static FORCE_INLINE uint64_t rotl64 ( uint64_t x, int8_t r )
{
  return (x << r) | (x >> (64 - r));
}

#define ROTL64(x,y)	rotl64(x,y)

#define BIG_CONSTANT(x) (x##LLU)

//-----------------------------------------------------------------------------
// Block read - if your platform needs to do endian-swapping or can only
// handle aligned reads, do the conversion here

#define getblock(p, i) (p[i])

//-----------------------------------------------------------------------------
// Finalization mix - force all bits of a hash block to avalanche

static FORCE_INLINE uint64_t fmix64 ( uint64_t k )
{
  k ^= k >> 33;
  k *= BIG_CONSTANT(0xff51afd7ed558ccd); // (inv 0x4f74430c22a54005)
  k ^= k >> 33;
  k *= BIG_CONSTANT(0xc4ceb9fe1a85ec53); //  (inv 0x9cb4b2f8129337db)
  k ^= k >> 33;

  return k;
}

void HashUtils::MurmurHash3_x64_128 ( const void * key, const int len,
                           const uint32_t seed, void * out )
{
  const uint8_t * data = (const uint8_t*)key;
  const int nblocks = len / 16;
  int i;

  uint64_t h1 = seed;
  uint64_t h2 = seed;

  uint64_t c1 = BIG_CONSTANT(0x87c37b91114253d5);
  uint64_t c2 = BIG_CONSTANT(0x4cf5ad432745937f);

  //----------
  // body

  const uint64_t * blocks = (const uint64_t *)(data);

  for(i = 0; i < nblocks; i++)
  {
    uint64_t k1 = getblock(blocks,i*2+0);
    uint64_t k2 = getblock(blocks,i*2+1);

    k1 *= c1; k1  = ROTL64(k1,31); k1 *= c2; h1 ^= k1;

    h1 = ROTL64(h1,27); h1 += h2; h1 = h1*5+0x52dce729;

    k2 *= c2; k2  = ROTL64(k2,33); k2 *= c1; h2 ^= k2;

    h2 = ROTL64(h2,31); h2 += h1; h2 = h2*5+0x38495ab5;
  }

  //----------
  // tail

  const uint8_t * tail = (const uint8_t*)(data + nblocks*16);

  uint64_t k1 = 0;
  uint64_t k2 = 0;

  switch(len & 15)
  {
  case 15: k2 ^= (uint64_t)(tail[14]) << 48;
           // falls through
  case 14: k2 ^= (uint64_t)(tail[13]) << 40;
           // falls through
  case 13: k2 ^= (uint64_t)(tail[12]) << 32;
           // falls through
  case 12: k2 ^= (uint64_t)(tail[11]) << 24;
           // falls through
  case 11: k2 ^= (uint64_t)(tail[10]) << 16;
           // falls through
  case 10: k2 ^= (uint64_t)(tail[ 9]) << 8;
           // falls through
  case  9: k2 ^= (uint64_t)(tail[ 8]) << 0;
           k2 *= c2; k2  = ROTL64(k2,33); k2 *= c1; h2 ^= k2;
           // falls through

  case  8: k1 ^= (uint64_t)(tail[ 7]) << 56;
           // falls through
  case  7: k1 ^= (uint64_t)(tail[ 6]) << 48;
           // falls through
  case  6: k1 ^= (uint64_t)(tail[ 5]) << 40;
           // falls through
  case  5: k1 ^= (uint64_t)(tail[ 4]) << 32;
           // falls through
  case  4: k1 ^= (uint64_t)(tail[ 3]) << 24;
           // falls through
  case  3: k1 ^= (uint64_t)(tail[ 2]) << 16;
           // falls through
  case  2: k1 ^= (uint64_t)(tail[ 1]) << 8;
           // falls through
  case  1: k1 ^= (uint64_t)(tail[ 0]) << 0;
           k1 *= c1; k1  = ROTL64(k1,31); k1 *= c2; h1 ^= k1;
  };

  //----------
  // finalization

  h1 ^= len; h2 ^= len;

  h1 += h2;
  h2 += h1;

  h1 = fmix64(h1);
  h2 = fmix64(h2);

  h1 += h2;
  h2 += h1;

  ((uint64_t*)out)[0] = h1;
  ((uint64_t*)out)[1] = h2;
}


void HashUtils::MurmurHash3_x64_512B(const uint64_t sector, const uint64_t iv1, const uint64_t iv2, uint8_t *out) {

  const uint64_t c1 = BIG_CONSTANT(0x87c37b91114253d5);
  const uint64_t c2 = BIG_CONSTANT(0x4cf5ad432745937f);
  const uint32_t ns = 32;
  uint32_t i;

  uint64_t h1[ns], h2[ns];
  uint64_t k1[ns], k2[ns];

  // init
  for(i=0;i<ns;++i) h1[i] = h2[i] = i;

  // first round; 2 8 byte blocks: sector , iv1

  for(i=0;i<ns;++i) {
    k1[i] = ROTL64(sector*c1,31) * c2;
    h1[i] = (ROTL64(h1[i]^k1[i],27) + h2[i])*5 + 0x52dce729;
    k2[i] = ROTL64(iv1*c2,33) * c1;
    h2[i] = (ROTL64(h2[i]^k2[i],31) + h1[i])*5 + 0x38495ab5;
  }

  // partial round; 8 bytes: iv2

  for(i=0;i<ns;++i) h1[i] ^= ROTL64(iv2*c1,31)*c2;

  //----------
  // finalization

  for(i=0;i<ns;++i) {
    h1[i] ^= 24;
    h2[i] ^= 24;
    h1[i] += h2[i];
    h2[i] += h1[i];
    h1[i] = fmix64(h1[i]);
    h2[i] = fmix64(h2[i]);
    h1[i] += h2[i];
    h2[i] += h1[i];
    U64TO8_LE(out+16*i, h1[i]); 
    U64TO8_LE(out+8+16*i, h2[i]); 
  }

}
