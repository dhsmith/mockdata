/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ListSourceSmoothing.hh"
#include "common/MDLog.hh"
#include <assert.h>

ListSourceSmoothing::ListSourceSmoothing(const ListSourceConfig &cfg) : smoothing_mode_(false) {
  if (cfg.cline_.smoothingModeParams(smoothing_rstart_, smoothing_rend_, smoothing_duration_)) {
    smoothing_mode_ = true;
    smoothing_bytecount_ = 0;
  }
}


bool ListSourceSmoothing::needNextFile(const time_t referenceWallStart) {
  assert(smoothing_mode_);
  const time_t now = time(0);
  if (now <= referenceWallStart) return false;
  const double avrate = double(smoothing_bytecount_) / (now - referenceWallStart);

  //
  // desired instantaneous rate at elapsed time t is
  // r(t) = low + t/duration * (high - low)
  //
  // average desired rate is
  // avgr(t) = 1/t * \int[0,t](r(t) dt) = low + 0.5*t/duration * (high-low)
  //
  const double targetavrate = smoothing_rstart_ + 0.5*(double(now - referenceWallStart)/smoothing_duration_)*(smoothing_rend_ - smoothing_rstart_);

//  MDLog::Log(MDLog::DEBUG, "smoothing rate=" + std::to_string(avrate) + " target=" + std::to_string(targetavrate));
  if (avrate > targetavrate) return false;
  return true;
}

void ListSourceSmoothing::accountLostFid(const uint64_t id) {
  if (!smoothing_mode_) return;
  if (smoothing_fmap_.find(id) == smoothing_fmap_.end()) return;
  smoothing_bytecount_ -= smoothing_fmap_[id];
  smoothing_fmap_.erase(id);
}

void ListSourceSmoothing::accountOfferedFid(const uint64_t id, const uint64_t aproxlen) {
  if (!smoothing_mode_) return;
  if (smoothing_fmap_.find(id) != smoothing_fmap_.end()) return;
  smoothing_bytecount_ += aproxlen;
  smoothing_fmap_[id] = aproxlen;
}

void ListSourceSmoothing::accountClaimedFid(const uint64_t id, const uint64_t len) {
  if (!smoothing_mode_) return;
  if (smoothing_fmap_.find(id) == smoothing_fmap_.end()) return;
  if (len > smoothing_fmap_[id]) {
    smoothing_bytecount_ += len - smoothing_fmap_[id];
  } else {
    smoothing_bytecount_ -= smoothing_fmap_[id] - len;
  }
  smoothing_fmap_.erase(id);
}
