/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "TransferGetterAndResponder.hh"
#include "FilesEvents.hh"
#include "GetterContext.hh"

#include "common/Profiles.hh"
#include "common/MDLog.hh"

#include "lg.grpc.pb.h"

#include <grpcpp/grpcpp.h>
#include <grpc/support/log.h>

#include <XrdCl/XrdClEnv.hh>
#include <XrdCl/XrdClDefaultEnv.hh>
#include <XrdCl/XrdClConstants.hh>

#include <thread>
#include <unistd.h>

using grpc::Channel;

int main(int argc, char **argv) {

  if (argc != 2) {
    std::cerr << "Usage: mock-loadgenerator <coordinator hostname>" << std::endl;
    return 1;
  }

  std::string chost(argv[1]);

  bool first_connect = true;

  {
    // The hash-data validaiton happens in the callback threads:
    // make sure there can be enough fully load the machine
    XrdCl::Env *env  = XrdCl::DefaultEnv::GetEnv();
    env->PutInt("WorkerThreads", sysconf(_SC_NPROCESSORS_CONF));
    // number of seconds to keep idle physical connections alive
    env->PutInt("DataServerTTL", 30);
    env->PutInt("LoadBalancerTTL", 30);
  }

  do {

    if (!first_connect) {
      MDLog::Log(MDLog::INFO, "Delay 15 seconds. Then will try to reconnect to coordinator");
      sleep(15);
    }
    first_connect = false;

    std::shared_ptr<grpc::Channel> channel = CreateChannel(chost.c_str(), grpc::InsecureChannelCredentials());

    GetterContext gc{};
    gc.stub_ = loadgenerator::LoadGenerator::NewStub(channel);

    TransferGetterAndResponder netevents;
    FilesEvents fevents;
 
    try {
      gc.getConfig();
      XrdCl::Env *env = XrdCl::DefaultEnv::GetEnv();
      int streams = XrdCl::DefaultSubStreamsPerChannel;
      env->GetInt( "SubStreamsPerChannel", streams );
      if( streams < 1 ) streams = 1;
      if (gc.coordReqSubstreams_ > 0 && streams != gc.coordReqSubstreams_) {
        env->PutInt("SubStreamsPerChannel", gc.coordReqSubstreams_);
      }
    } catch(const std::string &reason) {
      std::string msg = "Error during getConfig: " + reason;
      MDLog::Log(MDLog::ERROR, msg);
      continue;
    }

    {
      std::string msg = "Loaded configuration from " + chost + " v" + gc.coordMdVersionStr_ +
                        " File profiles: " + std::to_string(gc.profiles_.getFileProfiles().size()) +
                        " Access profiles: " + std::to_string(gc.profiles_.getAccessProfiles().size());
      MDLog::Log(MDLog::INFO, msg);
    }

    try {
      std::thread transferGetterThread(&TransferGetterAndResponder::Run, &netevents, &gc);

      std::thread xrdClientLoopThread(&FilesEvents::Run, &fevents, &gc);

      transferGetterThread.join();

      MDLog::Log(MDLog::DEBUG, "transferGetterThread joined");

      {
        std::lock_guard<std::mutex> lck(gc.assignedMtx_);
        gc.getterAndResponderDone_ = true;
      }
      gc.assignedInsertedCB_.CallbackInterested();

      MDLog::Log(MDLog::DEBUG, "trying to join xrdClientLoopThread");

      xrdClientLoopThread.join();

      if (gc.finishRequested_) {
        break;
      }

    } catch(const std::string &reason) {
      std::string msg = "Error while checking for files or running transfers: " + reason;
      MDLog::Log(MDLog::ERROR, msg);
    }

  } while(1);

}
