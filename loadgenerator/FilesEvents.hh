/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "IOHandling.hh"
#include "LGMessages.hh"
#include "OverlappingTimeRangeSum.hh"
#include "GetterContext.hh"

#include <cstdint>

#include <XrdCl/XrdClFileSystem.hh>
#include <XrdCl/XrdClFile.hh>
#include <XrdCl/XrdClURL.hh>
#include <XrdCl/XrdClEnv.hh>
#include <XrdCl/XrdClDefaultEnv.hh>
#include <XrdCl/XrdClConstants.hh>
#include <XrdCl/XrdClXRootDResponses.hh>

class FileStatus {
public:
  FileStatus() : id(0), usingmockoss(true), status(LGMessages::status_e::OTHER), fileprofile(0), accessprofile(0), filesize(0), coordinator_filesize(0), nbtoread(0), intendedduration(0.0f), nread(0), nbscalarread(0),
               nbvecread(0), nremain(0), nbissued(0), nbissuedremain(0), clFile(new XrdCl::File), open(0), donereading(0), donereadissuing(0), nsimthreads(1), maxioqueue(1), filesperlogin(1),
               nextIoId(0), gcd(0), statdone(false), out_latency(0), out_inode(0), th(0), ioReadOutstandingPerFile(0), targetReadOutstanding(0), readIODurationSampleSkip(0), readIODurationSample(0.0f),
               results_nseekbytes(0), results_nseeks(0), results_nvecreadcalls(0), results_nvecreadchunks(0), results_nreadcalls(0) { }
  ~FileStatus() { }

  uint64_t id;
  bool usingmockoss;
  LGMessages::status_e status;
  LGMessages::req_result_s result;
  const struct Profiles::fp_s *fileprofile;
  const struct Profiles::ap_s *accessprofile;
  uint64_t filesize;
  uint64_t coordinator_filesize;
  uint64_t nbtoread;
  float intendedduration;
  uint64_t nread;
  uint64_t nbscalarread;
  uint64_t nbvecread;
  uint64_t nremain;
  uint64_t nbissued;
  uint64_t nbissuedremain;
  std::shared_ptr<XrdCl::File> clFile;
  int open;
  int donereading;
  int donereadissuing;
  int nsimthreads;
  int maxioqueue;
  int filesperlogin;
  uint64_t nextIoId;
  uint64_t gcd;
  std::string dataserver;

  bool statdone;
  std::string fullfilename;

  // information for checking the server generated content
  // only filled in in MockOss mode
  unsigned int out_latency;
  std::vector<uint8_t> out_seed;
  ino_t out_inode;

  std::unique_ptr<struct outstandingIo_s> opening;
  std::unique_ptr<struct outstandingIo_s> stating;
  std::unique_ptr<struct outstandingIo_s> closing;
  struct simthreadspecific_s {
    simthreadspecific_s() : issueOffset(0) { clock_gettime(CLOCK_MONOTONIC, &noOutstandingIOBefore); }
    simthreadspecific_s(simthreadspecific_s&&) = default;
    ~simthreadspecific_s() { }
    std::map<uint64_t, std::unique_ptr<struct outstandingIo_s > > outstanding;
    uint64_t issueOffset;
    timespec noOutstandingIOBefore;
  };
  std::vector<struct simthreadspecific_s> th;
  struct timespec tvstart,tvtargetend,tvnext;
  OverlappingTimeRangeSum totalXrdClientTime;

  //
  size_t ioReadOutstandingPerFile;
  size_t targetReadOutstanding;

  //
  int readIODurationSampleSkip;
  float readIODurationSample;

  std::vector<uint64_t> results_stats_offsetbytes;
  uint64_t results_nseekbytes;
  uint64_t results_nseeks;
  uint64_t results_nvecreadcalls;
  uint64_t results_nvecreadchunks;
  uint64_t results_nreadcalls;

  LGMessages::seekFeedback_s seekFeedback;
};

class FileError {
public:
  FileError(FileStatus &s, const std::string &reason) : s(s), reason(reason) { }
  ~FileError() { }

  FileStatus &s;
  std::string reason;
};

class FilesEvents {
public:
  void fillFileResult(FileResult &fileresult, const struct timespec &now, const FileStatus &st);
  void errorLongRunning(const struct timespec &now);
  void errorCoordinatorGone(const bool doneassigning);
  void moveToTerminated(GetterContext *gc, const struct timespec &now);
  void CompleteClose();
  void SubmitClose(LGMessages::wake_clientllop_s &waitinfo);
  size_t StartVectorRead(std::unique_ptr<struct outstandingIo_s> &io, FileStatus &st, int nth, const struct timespec &now, LGMessages::wake_clientllop_s &waitinfo);
  void StartScalarRead(std::unique_ptr<struct outstandingIo_s> &io, FileStatus &st, int nth, const struct timespec &now, LGMessages::wake_clientllop_s &waitinfo);
  void SubmitRead(const struct timespec &now, const size_t ioMaxReadOutstanding, bool &xearliest_set, struct timespec &xearliest, LGMessages::wake_clientllop_s &waitinfo);
  void CompleteRead(const timespec &now);
  void CompleteOpen();
  void SubmitStat(LGMessages::wake_clientllop_s &waitinfo);
  void CompleteStat();
  void createInitialStatusFromAssignedAndSubmitOpen(GetterContext *gc, const struct timespec &now, bool &xearliest_set, struct timespec &xearliest, LGMessages::wake_clientllop_s &waitinfo);

  void Run(GetterContext *gc);

  std::map<uint64_t, FileStatus> stmap_;
};
