/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "GetterContext.hh"
#include "version.h"

#include <utility>

void GetterContext::getConfig() {

  ClientContext context;
  ConfigRequest request;
  ConfigReply reply;
  request.set_mdversionstr(MOCKDATA_VERSION);
  request.set_lgidstr(LGIdStr_);
  Status status = stub_->GetLatestConfig(&context, request, &reply);

  if (!status.ok()) {
    std::string msg = std::string("Error making GetLatestConfig grpc call: ") +
                  std::to_string(status.error_code()) + ": " + status.error_message();
    throw msg;
  }

  // remove any previous
  profiles_.clear();
  profile_pcre2_regexs_.clear();

  targethost_ = reply.targethost();
  pathprefix_ = reply.pathprefix();
  usingmockoss_ = reply.usingmockoss();
  accessmapsize_ = reply.accessmapsize();
  version_ = reply.version();
  profiles_.setVersion(reply.version());
  coordReqSubstreams_ = reply.nsubstreams();
  coordMdVersionStr_ = reply.mdversionstr();

  {
    static uint64_t xxlid=0;
    static std::mutex loginidMtx;
    std::lock_guard<std::mutex> lck(loginidMtx);
    configFetchSeq_ = xxlid++;
  }

  for(const auto &fp: reply.fileprofiles()) {
    std::vector<std::pair<float, std::string> > matches;
    for(const auto &fpm: fp.associatedprofiles()) {
      matches.push_back(std::make_pair(fpm.weight(), fpm.name()));
    }
    profiles_.AddToFileProfile(fp.name(), fp.fileregexp(), fp.latency(), matches);
    // compile the pcre2 regexp
    const std::string restr = fp.fileregexp();
    pcre2_code *re;
    PCRE2_SIZE erroroffset;
    int errornumber;
    re = pcre2_compile((PCRE2_SPTR8)restr.c_str(), PCRE2_ZERO_TERMINATED, 0, &errornumber, &erroroffset, 0);
    if (!re) {
      PCRE2_UCHAR buffer[256];
      pcre2_get_error_message(errornumber, buffer, sizeof(buffer));
      throw std::string("For profile " + fp.name() + " PCRE2 regexp compilation failed at offset " + std::to_string(erroroffset) + ": " + std::string((char*)buffer));
    }
    profile_pcre2_regexs_.emplace_back(re);
  }
  for(const auto &ap: reply.accessprofiles()) {
    std::vector<float> readSize;
    std::vector<float> seekDistance;
    for(const auto &rs: ap.pdfreadsize().bin()) {
      readSize.push_back(rs);
    }
    for(const auto &rs: ap.pdfseekdistance().bin()) {
      seekDistance.push_back(rs);
    }
    profiles_.AddToAccessProfile(ap.name(), ap.proto(), ap.lentoread(), ap.duration(), ap.durationpermbyte(),
                                    ap.overrunfactor(), ap.overrunabs(), ap.seekspermbissued(), ap.seekedfracperseek(), ap.probvecread(),
                                    ap.vecreadsize(), ap.maxioqueue(), ap.filesperlogin(), ap.nsimthreads(), ap.pdfreadsize().min(),
                                    ap.pdfreadsize().max(), readSize, seekDistance);
  }
}
