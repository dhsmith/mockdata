/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <sys/time.h>

#define ts_timercmp(a, b, CMP)                                                \
  (((a)->tv_sec == (b)->tv_sec) ?                                             \
   ((a)->tv_nsec CMP (b)->tv_nsec) :                                          \
   ((a)->tv_sec CMP (b)->tv_sec))

#define ts_timersub(a, b, result)                                             \
  do {                                                                        \
    (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;                             \
    (result)->tv_nsec = (a)->tv_nsec - (b)->tv_nsec;                          \
    if ((result)->tv_nsec < 0) {                                              \
      --(result)->tv_sec;                                                     \
      (result)->tv_nsec += 1000000000;                                        \
    }                                                                         \
  } while (0)

#define ts_timer2float(a, result)                                             \
  do {                                                                        \
    *(result) = (a)->tv_sec + ((float)((a)->tv_nsec))/1E9;                    \
  } while (0)

#define ts_timeraddfloat(a, f)                                                \
  do {                                                                        \
    (a)->tv_sec += (time_t)(f);                                               \
    (a)->tv_nsec += (long)( ((f) - ((int)(f)))*1E9 );                         \
    if ((a)->tv_nsec >= 1000000000) {                                         \
      ++(a)->tv_sec;                                                          \
      (a)->tv_nsec -= 1000000000;                                             \
    } else if ((a)->tv_nsec < 0) {                                            \
      --(a)->tv_sec;                                                          \
      (a)->tv_nsec += 1000000000;                                             \
    }                                                                         \
  } while (0)
