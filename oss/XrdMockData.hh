/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef XRDMOCKDATAOSS_HH
#define XRDMOCKDATAOSS_HH

#include <vector>
#include <memory>
#include <string.h>
#include <stdint.h>

#include <XrdOss/XrdOss.hh>
#include <XrdOuc/XrdOucStream.hh>
#include <XrdSys/XrdSysPthread.hh>

#define XRDMOCKDATAOSS_EBASE 8001
  
#define XRDMOCKDATAOSS_E8001 8001
#define XRDMOCKDATAOSS_E8002 8002
#define XRDMOCKDATAOSS_E8003 8003
#define XRDMOCKDATAOSS_E8004 8004

#define XRDMOCKDATAOSS_ELAST 8004
  
#define XRDMOCKDATAOSS_T8001 "directory object in use (internal error)"
#define XRDMOCKDATAOSS_T8002 "directory object not open (internal error)"
#define XRDMOCKDATAOSS_T8003 "file object in use (internal error)"
#define XRDMOCKDATAOSS_T8004 "file object not open (internal error)"

class FileDescriptor {
public:
  FileDescriptor(const size_t len, const unsigned int latency, const std::vector<uint8_t> s, const ino_t inode) :
        m_len(len), m_lat(latency), m_inode(inode) {
    size_t vs = s.size();
    if (vs>sizeof(m_seed)) vs=sizeof(m_seed);
    memcpy(m_seed, &s[0], vs);
    memset(&m_seed[vs], 0, sizeof(m_seed) - vs);
  }

  size_t filesize() const { return m_len; }

  unsigned int latency() const { return m_lat; }

  ino_t inode() const { return m_inode; }

  const uint8_t *seed() const { return m_seed; }

private:
  size_t m_len;
  unsigned int m_lat;
  uint8_t m_seed[16];
  ino_t m_inode;
};

class XrdMockDataOssDir : public XrdOssDF {
public:
   int Close(long long *retsz = 0);
   int Opendir(const char *, XrdOucEnv &);
   int Readdir(char *buff, int blen);

   // Constructor and destructor
   XrdMockDataOssDir(const char *tid) : tident(tid) {}
   ~XrdMockDataOssDir();

private:
   XrdMockDataOssDir(const XrdMockDataOssDir&);
   XrdMockDataOssDir& operator=(const XrdMockDataOssDir&);

   const char                *tident;
};

class XrdMockDataOssFile : public XrdOssDF {
public:
   int Close(long long *retsz = 0);
   int Open(const char *, int, mode_t, XrdOucEnv &);
   int Fchmod(mode_t mode);

   int Fstat(struct stat *st);
   int Fsync();
   int Fsync(XrdSfsAio *aiop);
   int Ftruncate(unsigned long long len);
   int getFD();
   off_t getMmap(void **addr);
   int isCompressed(char *cxidp=0);
   ssize_t Read(off_t off, size_t sz);
   ssize_t Read(void *p, off_t off, size_t sz);
   int Read(XrdSfsAio *aiop);
   ssize_t ReadRaw(void *p, off_t off, size_t sz);
   ssize_t Write(const void *p, off_t off, size_t sz);
   int Write(XrdSfsAio *aiop);
   ssize_t ReadV(XrdOucIOVec *readV, int n);
   ssize_t WriteV(XrdOucIOVec *writeV, int n);

   // Constructor and destructor
   XrdMockDataOssFile(XrdOssDF *dfroute, const char *tid) : tident(tid), dfroute(dfroute) {}
   virtual ~XrdMockDataOssFile();

private:
   XrdMockDataOssFile(const XrdMockDataOssFile&);
   XrdMockDataOssFile& operator=(const XrdMockDataOssFile&);
   ssize_t ReadNoDelay(void *p, off_t off, size_t sz);
   void delayAioRead(XrdSfsAio *aiop);

   const char                *tident;
   XrdOssDF                  *dfroute;
   std::unique_ptr<FileDescriptor> m_fd;
};

class XrdMockDataOss : public XrdOss {
public:
   virtual XrdOssDF *newDir(const char *tident) {
      return (XrdOssDF *)new XrdMockDataOssDir(tident);
   }
   virtual XrdOssDF *newFile(const char *tident) {
      XrdOssDF *dfroute = 0;
      return (XrdOssDF *)new XrdMockDataOssFile(dfroute, tident);
   }

   virtual int Chmod(const char *, mode_t mode, XrdOucEnv *eP = 0);
   virtual int Create(const char *, const char *, mode_t, XrdOucEnv &, int opts = 0);
   virtual int Init(XrdSysLogger *, const char *);
   virtual int Mkdir(const char *, mode_t mode, int mkpath = 0, XrdOucEnv *eP = 0);

   virtual int Reloc(const char *, const char *, const char *, const char *x = 0) {
      return -ENOTSUP;
   }
   virtual int Remdir(const char *, int Opts = 0, XrdOucEnv *eP = 0);
   virtual int Rename(const char *, const char *,
      XrdOucEnv *eP1 = 0, XrdOucEnv *eP2 = 0);
   virtual int Stat(const char *, struct stat *, int opts = 0, XrdOucEnv *eP = 0);

   virtual int StatFS(const char *path, char *buff, int &blen, XrdOucEnv *eP = 0);

   virtual int StatLS(XrdOucEnv &env, const char *cgrp, char *buff, int &blen);

   virtual int StatXA(const char *path, char *buff, int &blen, XrdOucEnv *eP = 0);

   virtual int StatXP(const char *path, unsigned long long &attr,
      XrdOucEnv *eP = 0);

   virtual int Truncate(const char *, unsigned long long, XrdOucEnv *eP = 0);

   virtual int Unlink(const char *fn, int Opts = 0, XrdOucEnv *eP = 0);

   virtual int Stats(char *bp, int bl);

   virtual int StatVS(XrdOssVSInfo *sP, const char *sname = 0, int updt = 0) {
      return -ENOTSUP;
   }

   virtual int Lfn2Pfn(const char *Path, char *buff, int blen);

   virtual
   const char *Lfn2Pfn(const char *Path, char *buff, int blen, int &rc);

   XrdMockDataOss(XrdOss *OssRoute, char *params): XrdOss(), OssRoute(OssRoute) { }
   
   virtual ~XrdMockDataOss() { }

private:
   int ConfigProc(XrdSysError &Eroute, const char *configfn);
   XrdOss                *OssRoute;
};
#endif   /* XRDMOCKDATAOSS_HH */


/******************************************************************************/
/*           S t o r a g e   S y s t e m   I n s t a n t i a t o r            */
/******************************************************************************/

// This function is called to obtain an instance of a configured XrdOss object.
// It is passed the object that would have been used as the storage system.
// The object is not initialized (i.e., Init() has not yet been called).
// This allows one to easily wrap the native implementation or to completely 
// replace it, as needed. The name of the config file and any parameters
// specified after the path on the ofs.osslib directive are also passed (note
// that if no parameters exist, parms may be null).

extern "C" {
   XrdOss *XrdOssGetStorageSystem(XrdOss *native_oss,
      XrdSysLogger *Logger,
      const char *config_fn,
      const char *parms);
}
