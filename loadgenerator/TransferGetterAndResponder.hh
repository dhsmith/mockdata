/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "GetterContext.hh"

#include "lg.grpc.pb.h"

#include <cstdint>
#include <grpcpp/grpcpp.h>
#include <grpc/support/log.h>

using loadgenerator::TransferReply;
using loadgenerator::TransferReport;

class TransferGetterAndResponder {
public:

  struct readinfo_s {
    readinfo_s(grpc::ClientReaderWriter<TransferReport, TransferReply> *rw, TransferReply &reply) : rw(rw), reply(reply), rret(false), newres(false), finish(false) { }
    ~readinfo_s() { }

    grpc::ClientReaderWriter<TransferReport, TransferReply> *rw;
    TransferReply &reply;
    bool rret, newres;
    bool finish;
    std::mutex mtx;
    std::condition_variable cv;
  };

  static void getterReadWatcher(GetterContext *gc, struct readinfo_s *r, LGMessages::wake_clientllop_s *winf);

  void transferGetterProcessRead(GetterContext *gc, TransferReply &reply, std::map<uint64_t, LGMessages::req_s> &offered, grpc::ClientReaderWriter<TransferReport, TransferReply> *rw, bool &finish);

  void Run(GetterContext *gc);
};
