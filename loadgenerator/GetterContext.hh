/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "Callbacks.hh"
#include "LGMessages.hh"
#include "common/Profiles.hh"

#include "lg.grpc.pb.h"

#include <grpcpp/grpcpp.h>
#include <grpc/support/log.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#include <cstdint>
#include <string>
#include <vector>
#include <utility>
#include <map>
#include <memory>
#include <condition_variable>
#include <stdlib.h>
#include <unistd.h>

using grpc::ClientContext;
using grpc::Status;
using loadgenerator::ConfigRequest;
using loadgenerator::ConfigReply;

struct GetterContext {
  GetterContext() : version_(0), usingmockoss_(false), accessmapsize_(0), coordReqSubstreams_(0), configFetchSeq_(0), prev_walltotal_(0.0f), prev_wallwait_(0.0f), next_walltotal_(0.0f), 
                    next_wallwait_(0.0f), curr_dutycycle_(0.0f), assignedInserted_(false), terminatedInserted_(false),
                    getterAndResponderDone_(false), finishRequested_(false) {
    LGIdStr_ = std::string(getenv("HOSTNAME") ? getenv("HOSTNAME") : "[no hostname]") + "#" + std::to_string(getpid());
  }
  ~GetterContext() { }

  void getConfig();

  struct pcre2_regex_holder_s {
    pcre2_regex_holder_s() : re(0) { }
    pcre2_regex_holder_s(pcre2_code *re) : re(re) { }
    pcre2_regex_holder_s(pcre2_regex_holder_s&&other) { re = other.re; other.re = 0; }
    ~pcre2_regex_holder_s() {
      if (re) { pcre2_code_free(re); }
    }
    pcre2_code *re;
  };

  struct login_id_s {
    login_id_s() : nameset(false), useCount(0) { }
    ~login_id_s() { }

    bool nameset;
    std::string loginName;
    int useCount;
  };
    
  std::unique_ptr<loadgenerator::LoadGenerator::Stub> stub_;

  uint64_t version_;
  std::string targethost_;
  std::string pathprefix_;
  bool usingmockoss_;
  int accessmapsize_;
  std::string LGIdStr_;

  std::string coordMdVersionStr_;
  int coordReqSubstreams_;
  uint16_t configFetchSeq_;

  float prev_walltotal_;
  float prev_wallwait_;
  float next_walltotal_;
  float next_wallwait_;
  std::mutex dutycycleMtx_;
  float curr_dutycycle_;

  std::mutex loginMapMtx_;
  std::map<int, login_id_s> loginMap_;

  Profiles profiles_;
  std::vector<pcre2_regex_holder_s> profile_pcre2_regexs_;

  std::mutex assignedMtx_;
  std::mutex terminatedMtx_;
  bool assignedInserted_;
  bool terminatedInserted_;
  CallbackServer assignedInsertedCB_;
  CallbackServer terminatedInsertedCB_;
  std::map<uint64_t, LGMessages::req_s> assigned_;
  std::map<uint64_t, std::pair<LGMessages::req_s, LGMessages::req_result_s> > terminated_;

  bool getterAndResponderDone_;
  bool finishRequested_;
};
