/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "common/Messages.hh"
#include "common/Profiles.hh"

#include "lg.grpc.pb.h"

#include <mutex>
#include <condition_variable>

using loadgenerator::TransferReport;
using loadgenerator::FileResultMsg;
using loadgenerator::LGInfoMsg;

// forward decs
struct GetterContext;

namespace LGMessages {

struct wake_clientllop_s {
  wake_clientllop_s() : event(false) { }
  ~wake_clientllop_s() { }
  std::mutex waitingMtx;
  bool event;
  std::condition_variable waitingCond;
};

namespace enums {
  enum status_e {
     OTHER,
     OPEN_SUBMITTED,
     OPEN,
     STAT_SUBMIT,
     STAT_SUBMITTED,
     READ_READY,
     READ_DRAIN,
     CLOSE_SUBMIT,
     CLOSE_SUBMITTED,
     CLOSED
  };
}
using namespace enums;

struct seekFeedback_s {
  seekFeedback_s() : effectiveCdfRecomputeCounter(0), seekFeedbackHistSum(0), binOffset1(0.0) { }
  ~seekFeedback_s() { }

  void IncFeedbackHist(const double x) {
    size_t ibin = (x+1.0)/(2.0/seekFeedbackHist.size());
    seekFeedbackHist[ibin]++;
    seekFeedbackHistSum++;
  }

  uint64_t effectiveCdfRecomputeCounter;
  std::vector<float> effectiveCdf;

  uint64_t seekFeedbackHistSum;
  std::vector<uint64_t> seekFeedbackHist;

  float binOffset1;
};

struct req_s {
  req_s() : id(0), fsize(0), timeoffered{0,0}, desiredstarttime{0,0}, fileprofile(nullptr), accessprofile(nullptr) { }
  ~req_s() { }
  uint64_t id;
  std::string filename;
  uint64_t fsize;
  struct timespec timeoffered;
  struct timespec desiredstarttime;
  const struct Profiles::fp_s *fileprofile;
  const struct Profiles::ap_s *accessprofile;
};

struct req_result_s {
  req_result_s() : error(false), error_in_state(OTHER) { }
  ~req_result_s() { }

  bool error;
  enum status_e error_in_state;
  std::string errorstring;

  FileResult fileresult;
};

void FillFileResult_Joining(TransferReport &report, const int nsubstreams);

void FillLGInfo(TransferReport &report, GetterContext *gc);

void FillFileResult_Claim(TransferReport &report, const uint64_t id, const float duration, const uint64_t toread, const struct Profiles::fp_s *fileprofile,  const struct Profiles::ap_s *accessprofile);

void FillFileResult_Finished(TransferReport &report, const FileResult &result);

} // namespace
