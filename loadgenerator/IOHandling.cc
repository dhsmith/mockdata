/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "IOHandling.hh"

// storage for the two class members of outstandingIo_s
//
std::mutex outstandingIo_s::countLck;
size_t outstandingIo_s::globalIoOutstandingReads;
size_t outstandingIo_s::globalIoReadBytes;

outstandingIo_s::outstandingIo_s(Type t, LGMessages::wake_clientllop_s *w, std::shared_ptr<XrdCl::File> &fp, const size_t ndata) :
   ioid(0), usingmockoss(true), handler(new MyResponseHandler(this,fp,ndata)), done(0), statGetSize(0), XrdClChunkBytesRead(0),
   type(t), offset(0), len(0), w(w) {
  if (type == Type::READ || type == Type::VREAD) {
    incReads();
  }
}

void outstandingIo_s::DeleteHandler() {
  delete handler;
  handler = 0;
  if (type == Type::READ || type == Type::VREAD) {
    decReads();
  }
}

outstandingIo_s::~outstandingIo_s() {
   if (handler) {
     handler->DetachIO();
   }
}

uint8_t *outstandingIo_s::datap() const {
  if (!handler) return 0;
  return handler->datap();
}
