/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "MDLog.hh"

#include <time.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <mutex>

void MDLog::Log(int level, const std::string &msg) {
  static std::mutex stdoutmtx;
  struct tm tmstruct;
  char timestr[64];
  static char strftime_format[] = "%b %e %H:%M:%S";
  static char hostname[256];

  {
    std::lock_guard<std::mutex> lck(stdoutmtx);
    if (!*hostname) {
      gethostname(hostname, sizeof(hostname));
      char *p;
      if ((p=strchr(hostname,'.'))) *p = '\0';
    }
  }

  auto now = std::chrono::system_clock::now();
  std::time_t now_timet = std::chrono::system_clock::to_time_t(now);
  localtime_r(&now_timet,&tmstruct);
  std::chrono::microseconds micro = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());

  strftime(timestr,sizeof(timestr),strftime_format,&tmstruct);
  snprintf(&timestr[strlen(timestr)],sizeof(timestr)-strlen(timestr),".%06ld", micro.count()%1000000);

  std::string levelstr = "UNKNOWN";
  if (level == INFO) {
    levelstr = "INFO";
  } else if (level == DEBUG) {
    levelstr = "DEBUG";
  } else if (level == WARN) {
    levelstr = "WARN";
  } else if (level == ERROR) {
    levelstr = "ERROR";
  }

  std::lock_guard<std::mutex> lck(stdoutmtx);
  std::cout << timestr << " " << hostname << " [" << levelstr << "] " << msg << std::endl;
}
