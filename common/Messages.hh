/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <string>
#include <vector>
#include <cstdint>
#include <sstream>
#include <numeric>

class LGInfo {
public:

  LGInfo() : dutycycle_(0.0f), nassigned_(0), lgbread_(0), lgbreadms_(0) { }
  ~LGInfo() { }

  std::string lgidstr_;
  float dutycycle_;
  uint64_t nassigned_;
  uint64_t lgbread_;
  uint64_t lgbreadms_;
};

class FileResult {
public:

  FileResult() : id_(0), nbissued_(0), nbscalarread_(0), nbvecread_(0), duration_(0.0f), residue_(0.0f),
                 nseekbytes_(0), nseeks_(0), nvecreadcalls_(0), nreadcalls_(0), nvecreadchunks_(0), fsize_(0),
                 xrdreqcallbacktime_(0.0f), xrdreqcallbacktsum_(0.0f), overrunfactor_(0.0f), overrunabs_(0.0f),
                 nsubstreams_(0), intendedduration_(0.0f), nsimthreads_(0), lengthfactor_(0.0), nbtoread_(0) { }
  ~FileResult() { }

  std::string msgString() const {
    std::stringstream osum;
    osum << residue_;

    std::string oh = "|";
    {
      // offset hits visualisation
      uint64_t ec = 0;
      if (accessmap_.size() >0) {
        ec = std::accumulate(accessmap_.begin(), accessmap_.end(), 0ULL) / accessmap_.size() / 2;
      }
      for(const auto cnt: accessmap_) {
        if (cnt>ec) oh += "*";
        else if (cnt>0) oh += "o";
        else oh += "-";
      }
      oh += "|";
    }

    std::string msg;
    msg  = "Final stats for fid=" + std::to_string(id_) + " fileUrl=" + url_;
    msg += " intended_duration=" + std::to_string(intendedduration_);
    msg += " nbissued=" + std::to_string(nbissued_);
    msg += " nread/vecread_bytes=" + std::to_string(nbscalarread_) + "/" + std::to_string(nbvecread_);
    msg += " nread/vecread_calls=" + std::to_string(nreadcalls_) + "/" + std::to_string(nvecreadcalls_);
    msg += " duration=" + std::to_string(duration_);
    msg += " accessMap=" + oh;
    msg += " seekPdfFit=" + osum.str();
    msg += " nseekbytes=" + std::to_string(nseekbytes_);
    msg += " nseeks=" + std::to_string(nseeks_);
    msg += " seekedFracPerSeek=" + (nseeks_ ? std::to_string( (double)nseekbytes_/nseeks_/fsize_) : "0");
    msg += " seeksPerMB=" + (nbissued_ ? std::to_string((double)nseeks_/nbissued_*1E6) : "0");
    msg += " Vnchunks/Vnread=" + (nvecreadcalls_ ? std::to_string((double)nvecreadchunks_ / nvecreadcalls_) : "0");
    msg += " fsize=" + std::to_string(fsize_);
    msg += " lastServer=" + lastserver_;
    msg += " xrdReqCallbackTime=" + std::to_string(xrdreqcallbacktime_);
    msg += " xrdReqOverlapFactor=" + (xrdreqcallbacktime_ ? std::to_string(xrdreqcallbacktsum_ / xrdreqcallbacktime_) : "0");
    return msg;
  }

  uint64_t id_;
  uint64_t nbissued_;
  uint64_t nbscalarread_;
  uint64_t nbvecread_;
  float duration_;
  std::vector<uint64_t> accessmap_;
  float residue_;
  uint64_t nseekbytes_;
  uint64_t nseeks_;
  uint64_t nvecreadcalls_;
  uint64_t nreadcalls_;
  uint64_t nvecreadchunks_;
  uint64_t fsize_;
  std::string lastserver_;
  float xrdreqcallbacktime_;
  float xrdreqcallbacktsum_;
  std::string accessprofilename_;
  std::string fileprofilename_;
  float overrunfactor_;
  float overrunabs_;
  int nsubstreams_;
  std::string url_;
  float intendedduration_;
  int nsimthreads_;
  double lengthfactor_;
  uint64_t nbtoread_;
};
