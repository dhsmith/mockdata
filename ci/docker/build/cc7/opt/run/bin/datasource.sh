#!/bin/bash 

. /opt/run/bin/init_pod.sh

if [ ! -e /etc/buildtreeRunner ]; then
  # Install missing RPMs
  yum -y install mockdata-plugin xrootd-server
fi

yes | cp -r /opt/ci/datasource/etc /

if [ "-${CI_CONTEXT}-" == '-nosystemd-' ]; then
  # systemd is not available
  runuser --shell='/bin/bash' --session-command='cd ~xrootd; xrootd -s /var/run/xrootd/xrootd-mockdata.pid -l /var/log/xrootd/xrootd.log -k fifo -n mockdata -c /etc/xrootd/xrootd-mockdata.cfg -I v4' xrootd
  echo "mockdata xrootd server died"
  sleep infinity
else
  # Add a DNS cache on the client as kubernetes DNS complains about `Nameserver limits were exceeded`
  yum install -y systemd-resolved
  systemctl start systemd-resolved

  # systemd is available
  echo "Launching mockdata server with systemd:"
  systemctl start xrootd@mockdata

  echo "Status is now:"
  systemctl status xrootd@mockdata
fi
