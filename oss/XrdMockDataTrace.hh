/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _XRDOSS_TRACE_H
#define _XRDOSS_TRACE_H

#include <XrdSys/XrdSysError.hh>
#include <XrdOuc/XrdOucTrace.hh>

// Trace flags
//
#define TRACE_MOST     0x3fcd
#define TRACE_ALL      0xffff
#define TRACE_opendir  0x0001
#define TRACE_readdir  0x0002
#define TRACE_closedir TRACE_opendir
#define TRACE_delay    0x0400
#define TRACE_dir      TRACE_opendir | TRACE_readdir | TRACE_closedir
#define TRACE_open     0x0004
#define TRACE_qscan    0x0008
#define TRACE_close    TRACE_open
#define TRACE_read     0x0010
#define TRACE_redirect 0x0800
#define TRACE_write    0x0020
#define TRACE_IO       TRACE_read | TRACE_write | TRACE_aio
#define TRACE_exists   0x0040
#define TRACE_chmod    TRACE_exists
#define TRACE_remove   0x0080
#define TRACE_rename   TRACE_remove
#define TRACE_sync     0x0100
#define TRACE_truncate 0x0200
#define TRACE_fsctl    0x0400
#define TRACE_getstats 0x0800
#define TRACE_mkdir    0x1000
#define TRACE_aio      0x4000
#define TRACE_debug    0x8000

#ifndef NODEBUG

#include <XrdSys/XrdSysHeaders.hh>

#define DEBUG(y) if (Trace.What & TRACE_debug) TRACEX(y)
#define TRACE(x,y) if (Trace.What & TRACE_ ## x) TRACEX(y)
#define TRACEX(y) {Trace.Beg(epname); cerr <<y; Trace.End();}

#define DEBUG_TID(y) if (Trace.What & TRACE_debug) TRACEX_TID(y)
#define TRACE_TID(x,y) if (Trace.What & TRACE_ ## x) TRACEX_TID(y)
#define TRACEX_TID(y) {Trace.Beg(epname,tident); cerr <<y; Trace.End();}

#define EPNAME(x) static const char *epname __attribute__((unused)) = x;

#else

#define DEBUG(y)
#define TRACE(x, y)
#define EPNAME(x)
#define DEBUG_TID(y)
#define TRACE_TID(y)

#endif
#endif
