/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "Reader.hh"
#include "ListSourceConfig.hh"
#include "ListSourceSmoothing.hh"
#include "common/MDLog.hh"
#include "common/Messages.hh"

#include <list>
#include <unordered_map>
#include <string>
#include <utility>
#include <time.h>
#include <unistd.h>

struct FileEntryForClient {
  uint64_t id;
  std::string filename;
  float startDelay;
  uint64_t fsize;
};

struct claimInfo {
   bufferFileEntry be;
   float intendedduration;
   float startDelay;
   LGInfo lginfo;
   float overrunfactor;
   float overrunabs;
   time_t claimTime;
};

class ListSource {
public:
  ListSource(Reader &reader, const ListSourceConfig &cfg);
  ~ListSource();
  void Load();
  bool LoadOne();
  std::list<FileEntryForClient> getListOfNextFiles();
  bool notifyFileAssigned(const uint64_t id, const time_t claimTime, const FileResult &fileresult, const LGInfo &li, const float startDelay);
  void notifyFileResult(const FileResult &fileresult, const int32_t rc, const std::string &msg, const LGInfo &li);
  bool finished();
  void Tick();

private:
  time_t original2wall(const time_t);
  void removeOldEntriesInBuffer(time_t targetWallTime);
  void removeClaimedEntriesInBuffer();
  void removeOverdueClaimed();

  ListSourceConfig cfg_;

  std::list<bufferFileEntry> bufferFileList_;
  std::unordered_map<uint64_t, claimInfo> claimed_;
  Reader &fd_;
  time_t referenceOriginalStart_;
  bool firstload_;

  bool haveBufferedEntry_;
  bufferFileEntry bufferedEntry_;

  static uint64_t nextid_;

  ListSourceSmoothing smoothing_;

  std::list<std::pair<time_t, uint64_t> > assignRateQ_;
  uint64_t assignRateQsum_;
};
