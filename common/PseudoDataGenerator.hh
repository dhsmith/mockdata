/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <vector>
#include <string>
#include <stdint.h>
#include <stddef.h>

bool parse_filename(const std::string fn, size_t &out_len, unsigned int &out_latency, std::vector<uint8_t> &out_seed, ino_t &out_inode);
void genData16(uint8_t *buf, const size_t len, const uint64_t off, const uint8_t *seed16);
void genData512(uint8_t *buf, const size_t len, const uint64_t off, const uint8_t *seed16);
void genData(uint8_t *buf, const size_t len, const uint64_t off, const uint8_t *seed16);
