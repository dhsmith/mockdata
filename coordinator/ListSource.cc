/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ListSource.hh"
#include "GlobalRateEstimate.hh"
#include "common/Constants.hh"
#include "CmdlineOptions.hh"

#include <algorithm>
#include <iostream>
#include <list>
#include <cmath>

// storage for class variable
uint64_t ListSource::nextid_;

ListSource::ListSource(Reader &reader, const ListSourceConfig &cfg) : cfg_(cfg), fd_(reader), referenceOriginalStart_(0),
                                      firstload_(true), haveBufferedEntry_(false), smoothing_(cfg), assignRateQsum_(0) {
  fd_.SetFilename(cfg_.cline_.GetFilelistFn());
  fd_.SetSkipUntil(cfg_.cline_.GetSkipUntil());
}

ListSource::~ListSource() {
}

bool ListSource::LoadOne() {
  if (haveBufferedEntry_) {
    return true;
  }
  if (fd_.LoadOne(bufferedEntry_)) {
    if (smoothing_.triggerFinish(time(0) - cfg_.referenceWallStart_)) {
      fd_.finishEarly();
      return false;
    }
    haveBufferedEntry_ = true;
    return true;
  }
  return false;
}
  

void ListSource::Load() {
  if (firstload_) {
    if (!LoadOne()) {
      return;
    }
    referenceOriginalStart_ = bufferedEntry_.originalStartTime;
    MDLog::Log(MDLog::INFO, "Setting the startpoint timestamp from the filelist to " + std::to_string(referenceOriginalStart_));
    bufferedEntry_.id = nextid_++;
    bufferedEntry_.wallTimeStart = cfg_.referenceWallStart_;
    smoothing_.accountOfferedFid(bufferedEntry_.id, bufferedEntry_.fsize);
    bufferFileList_.push_back(bufferedEntry_);
    haveBufferedEntry_ = false;
    firstload_ = false;
  }

  do {
    if (!LoadOne()) {
      return;
    }
    if (smoothing_.active()) {
      if (!smoothing_.needNextFile(cfg_.referenceWallStart_)) break;
      bufferedEntry_.id = nextid_++;
      bufferedEntry_.wallTimeStart = time(0)+cfg_.wallTimeWindow_;
      smoothing_.accountOfferedFid(bufferedEntry_.id, bufferedEntry_.fsize);
    } else {
      time_t st = original2wall(bufferedEntry_.originalStartTime);
      if (st > time(0)+cfg_.wallTimeWindow_) break;
      bufferedEntry_.id = nextid_++;
      bufferedEntry_.wallTimeStart = st;
    }
//    MDLog::Log(MDLog::DEBUG, "Adding " + bufferedEntry_.filename + " to bufferedfilelist startTime=" + std::to_string(bufferedEntry_.wallTimeStart) + " id=" + std::to_string(bufferedEntry_.id));
    bufferFileList_.push_back(bufferedEntry_);
    haveBufferedEntry_ = false;
  } while(1);
}

void ListSource::Tick() {
  if (!cfg_.referenceWallStart_) {
    // didn't yet start reading the file list
    return;
  }

  const time_t currWallTime = time(0);

  removeClaimedEntriesInBuffer();
  removeOldEntriesInBuffer(currWallTime);
  removeOverdueClaimed();

  // keep reading in new entries
  Load();
}

std::list<FileEntryForClient> ListSource::getListOfNextFiles() {
  std::list<FileEntryForClient> next;
  time_t currWallTime = time(0);
  if (!cfg_.referenceWallStart_) {
    cfg_.referenceWallStart_ = currWallTime + cfg_.wallTimeWindow_;
    MDLog::Log(MDLog::INFO, "Starting replay, reference time " + std::to_string(cfg_.referenceWallStart_));
    Load();
  }

  for(auto &e: bufferFileList_) {
    const time_t sd = e.wallTimeStart - currWallTime;

    if (sd>=0 && sd<=cfg_.wallTimeWindow_) {
      FileEntryForClient v;
      v.filename = e.filename;
      v.fsize = e.fsize;
      v.startDelay = sd;
      v.id = e.id;
//      MDLog::Log(MDLog::DEBUG, "getListOfNextFiles push back id=" + std::to_string(v.id) + " sd=" + std::to_string(sd));
      next.push_back(v);
    }
  }

  return next;
} 

void ListSource::removeClaimedEntriesInBuffer() {
  bufferFileList_.remove_if([&] (const bufferFileEntry &e) {
    if (claimed_.find(e.id) != claimed_.end()) return true;
    return false;
  });
}

void ListSource::removeOverdueClaimed() {
  std::list<uint64_t> toRem;
  const time_t now = time(0);

  for(const auto &cm: claimed_) {
    const uint64_t id=cm.first;
    const claimInfo &ci = cm.second;
    const time_t timetaken = now - (ci.claimTime + (time_t)ci.startDelay);
    if (ci.intendedduration == 0.0) {
      // currently we will not know the expected duraiton and it is set to 0.0 when not using mockoss
      continue;
    }
    if ((timetaken - ci.intendedduration) > ci.overrunabs*1.2 && (timetaken - ci.intendedduration)/ci.intendedduration > ci.overrunfactor*1.2) {
      MDLog::Log(MDLog::WARN, "Locally removing id=" + std::to_string(id) + " for filename=" + ci.be.filename + " lgidstr=" + ci.lginfo.lgidstr_ + " because of unsignaled overrun, intended_duration=" + std::to_string(ci.intendedduration) + " overrun=" + std::to_string(timetaken - ci.intendedduration));
      toRem.push_back(id);
      GRENotifyAssumeLostFid(id);
    }
  }
  for(const auto id: toRem) {
    claimed_.erase(id);
  }
}

void ListSource::removeOldEntriesInBuffer(time_t targetWallTime) {
  std::list<bufferFileEntry>::iterator itr;
  for(itr=bufferFileList_.begin(); itr != bufferFileList_.end(); ++itr) {
    if (itr->wallTimeStart >= targetWallTime) break;
    MDLog::Log(MDLog::WARN, "Skipping unclaimed id=" + std::to_string(itr->id) + " filename=" + itr->filename);
    smoothing_.accountLostFid(itr->id);
  }
  bufferFileList_.erase(bufferFileList_.begin(), itr);
}

time_t ListSource::original2wall(const time_t o) {
  time_t smoothed_original = o;
  double tScaling = cfg_.cline_.GetTimeScaling();

  double diff = smoothed_original - referenceOriginalStart_;
  diff /= tScaling;

  return cfg_.referenceWallStart_ + (time_t)diff;
}

void ListSource::notifyFileResult(const FileResult &fileresult, const int32_t rc, const std::string &msg, const LGInfo &li) {
  const uint64_t id = fileresult.id_;

  if (claimed_.find(id) == claimed_.end()) {
    MDLog::Log(MDLog::WARN, "Message for unknown id=" + std::to_string(id) +
                     ", probably a late result:" +
                     " lgidstr=" + li.lgidstr_ +
                     " result_rc=" + std::to_string(rc) +
                     " result_message=" + msg +
                     " result_summary=" + fileresult.msgString());
    return;
  }
  const claimInfo &ci = claimed_[id];
  float gre;
  GREMakeEstimateForEndingFid(id, li, gre);
  float delEff = 100.0;
  if (rc!=0) {
    delEff = 0.0;
  } else {
    delEff = 100.0 * (ci.intendedduration / (ci.intendedduration + fileresult.xrdreqcallbacktime_));
  }

  if (rc!=0) {
    MDLog::Log(MDLog::WARN, "Received failure for " + ci.be.filename + " id=" + std::to_string(id) +
                 " lgidstr=" + li.lgidstr_ +
                 " result_rc=" + std::to_string(rc) + " result_message=" + msg +
                 " result_summary=" + fileresult.msgString() +
                 " deliveryEff=" + std::to_string(delEff) + " globalRateEstimateMBps=" + std::to_string(gre));
  } else {
    MDLog::Log(MDLog::INFO, "Received success for " + ci.be.filename + " id=" + std::to_string(id) + 
                 " lgidstr=" + li.lgidstr_ +
                 " result_rc=" + std::to_string(rc) +
                 " result_summary=" + fileresult.msgString() +
                 " deliveryEff=" + std::to_string(delEff) + " globalRateEstimateMBps=" + std::to_string(gre));
  }

  // remove from bufferedList, if it is still there
  bufferFileList_.remove_if([&] (const bufferFileEntry &e) {
    if (e.id == id) return true;
    return false;
  });

  // remove from claimed
  claimed_.erase(id);
}

bool ListSource::notifyFileAssigned(const uint64_t id, const time_t claimTime, const FileResult &fileresult, const LGInfo &li, const float startDelay) {
  if (claimed_.find(id) != claimed_.end()) {
    return false;
  }
  bool cifound = false;
  claimInfo ci;
  for(const auto &e: bufferFileList_) {
    if (e.id == id) {
      ci.be = e;
      cifound = true;
      break;
    }
  }
  if (!cifound) {
    return false;
  }
  ci.claimTime = claimTime;
  ci.intendedduration = fileresult.intendedduration_;
  ci.startDelay = startDelay;
  ci.lginfo = li;
  ci.overrunfactor = fileresult.overrunfactor_;
  ci.overrunabs = fileresult.overrunabs_;
  claimed_[id] = ci;

  const uint64_t clen = ci.be.fsize * fileresult.lengthfactor_;
  smoothing_.accountClaimedFid(id, clen);

  const time_t now = time(0);
  while(assignRateQsum_ > 0 && now - assignRateQ_.back().first >= 300) {
    assignRateQsum_ -= assignRateQ_.back().second;
    assignRateQ_.pop_back();
  }
  const float assignRate = assignRateQsum_ / 300.0f;

  assignRateQ_.push_front(std::make_pair(now, clen));
  assignRateQsum_ += clen;

  uint64_t nrunning=0;
  for(const auto &cm: claimed_) {
   const claimInfo &ci = cm.second;
   const bool started = now > (ci.claimTime + (time_t)ci.startDelay);
   if (started) nrunning++;
 }

  MDLog::Log(MDLog::INFO, "Assigning id=" + std::to_string(id) + " originalStartTime=" + std::to_string(ci.be.originalStartTime) +
        " filename=" + ci.be.filename + " intended_duration=" + std::to_string(ci.intendedduration) +
        " allowed_overrun=" + std::to_string(ci.overrunfactor) + "/" + std::to_string(ci.overrunabs) +
        " approx_start=" + std::to_string(ci.startDelay - float(time(0)-ci.claimTime)) +
        " lgidstr=" + ci.lginfo.lgidstr_ + " dutycycle=" + std::to_string(ci.lginfo.dutycycle_) + " prev_nassigned_lg=" + std::to_string(ci.lginfo.nassigned_) +
        " nassigned_total=" + std::to_string(claimed_.size()) + "(" + std::to_string(nrunning) + ") assignRateFiveMinAvgMBs=" + std::to_string(assignRate/1E6));
  GRENotifyAssignedFid(ci.lginfo.lgidstr_, id, ci.claimTime + time_t(ci.startDelay));
  return true;
}


bool ListSource::finished() {
  if (bufferFileList_.empty() && !haveBufferedEntry_ && fd_.done() && claimed_.empty()) {
    return true;
  }
  return false;
}
