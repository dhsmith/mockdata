/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "common/Messages.hh"
#include "CoordConfig.hh"
#include "ListSource.hh"
#include "lg.grpc.pb.h"

#include "common/MDLog.hh"

#include <grpcpp/grpcpp.h>
#include <grpc/support/log.h>

#include <iostream>
#include <set>
#include <utility>

using grpc::Server;
using grpc::ServerAsyncResponseWriter;
using grpc::ServerAsyncReaderWriter;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerCompletionQueue;
using grpc::Status;

using loadgenerator::ConfigReply;
using loadgenerator::TransferReply;
using loadgenerator::TransferReport;
using loadgenerator::ConfigRequest;
using loadgenerator::LoadGenerator;
using loadgenerator::FileResultMsg;
using loadgenerator::LGInfoMsg;

class RpcHandler;
class TransferRpcHandler;

class CoordinatingGrpcServer {
public:
  CoordinatingGrpcServer(ListSource &src, CoordConfig &config) : src_(src), config_(config), shutdown_(false), shutdownrequested_(false), started_(false) { }
  ~CoordinatingGrpcServer();
  void Tick(bool &finished, const std::chrono::system_clock::time_point &deadline);
  void Start();
  void Stop() {
    if (started_ && !shutdown_) {
      shutdown_ = true;
      server_->Shutdown();
      //server_.reset();
      cq_->Shutdown();
    }
  };
  void RequestFinish() { shutdownrequested_ = true; }
  bool shutdownRequested() const { return shutdownrequested_; }
  ListSource *filelistsource() { return &src_; }
  CoordConfig *config() { return &config_; }
  void RegisterRpcTick(RpcHandler *h) { activehandlers_[h] = 1; }
  void RemoveRpcTick(RpcHandler *h);
  void receivedClaim(uint64_t id, TransferRpcHandler *h, TransferReport &r);
  void receivedResult(uint64_t id, TransferRpcHandler *h, TransferReport &r);
  void assignClaims();
  void distributeOffers(const std::list<FileEntryForClient> &l, TransferRpcHandler *handler);
  bool isOfferSendable(uint64_t id, TransferRpcHandler *handler);
  bool writeQuiet() const { return writequiet_; }

private:
  ListSource& src_;
  CoordConfig &config_;
  std::unique_ptr<ServerCompletionQueue> cq_;
  LoadGenerator::AsyncService service_;
  std::unique_ptr<Server> server_;
  bool shutdown_;
  bool shutdownrequested_;
  bool started_;
  std::map<RpcHandler *, int> activehandlers_;
  struct grpc_claim_info_s {
    TransferRpcHandler *h;
    FileResult fileresult;
    LGInfo lginfo;
    float startDelay;
    time_t claimtime;
  };
  std::map<time_t, std::map<uint64_t, std::vector<grpc_claim_info_s> > > claims_;
  bool writequiet_;

  // for offer distribution
  std::map<uint64_t, TransferRpcHandler *> offdist_id2handler_;
  std::set<TransferRpcHandler *> offdist_thandlers_;
};


class RpcHandler {
public:
  RpcHandler(LoadGenerator::AsyncService* service, ServerCompletionQueue* cq, CoordinatingGrpcServer *coord);
  virtual ~RpcHandler() { coordinatingserver_->RemoveRpcTick(this); }
  void Proceed(int val, bool ok);
  virtual void Tick() { }

  void *maketag(int val) { ++outstandingtags_; return new tagdata(val, this); }

  struct tagdata {
    int val_;
    RpcHandler *p_;
    tagdata(int val, RpcHandler *p) : val_(val), p_(p), destroyed_(false) { GPR_ASSERT(destroyed_ == false);}
    void recycle(int &val, RpcHandler *&p) {
      val = val_;
      p = p_;
      p->outstandingtags_--;
      destroyed_ = true;
      delete this;
    }
    bool destroyed_;
  };

  int outstandingtags_;

protected:
  // The means of communication with the gRPC runtime for an asynchronous
  // server.
  LoadGenerator::AsyncService* service_;

  // The producer-consumer queue where for asynchronous server notifications.
  ServerCompletionQueue* cq_;

  // Context for the rpc, allowing to tweak aspects of it such as the use
  // of compression, authentication, as well as to send metadata back to the
  // client.
  ServerContext ctx_;

  CoordinatingGrpcServer *coordinatingserver_;

  bool firstprocessincomplete_;
  bool processingfinished_;

  virtual void Process(int val, bool ok)=0;
  virtual void clone()=0;
};

class ConfigRpcHandler : public RpcHandler {
public:
  ConfigRpcHandler(LoadGenerator::AsyncService* service, ServerCompletionQueue* cq, CoordinatingGrpcServer *coord);
protected:
  ConfigRequest request_;
  ServerAsyncResponseWriter<ConfigReply> responder_;
  void Process(int val, bool ok);
  void clone();
  bool finishsent_;
};

class TransferRpcHandler : public RpcHandler {
public:
  TransferRpcHandler(LoadGenerator::AsyncService* service, ServerCompletionQueue* cq, CoordinatingGrpcServer *coord);
  void claimResult(uint64_t id);
  std::string lgidstr_;

protected:
  TransferReport report_;
  void Tick();
  ServerAsyncReaderWriter<TransferReply, TransferReport> rwstream_;
  enum TagMeta { READ, WRITE, FINISH };
  bool readoutstanding_;
  bool writeoutstanding_;
  bool finishoutstanding_;
  bool clientwritesdone_;
  bool writesdead_;
  bool firstreaddone_;
  std::list<uint64_t> claim_results_;
  std::set<uint64_t> seen_;

  void queueOfferSend();

  void Process(int val, bool ok);
  void clone();
};
