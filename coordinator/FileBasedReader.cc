/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FileBasedReader.hh"
#include "common/MDLog.hh"

#include <vector>
#include <sstream>

bool FileBasedReader::LoadOne(bufferFileEntry &e) {
  if (eof_) {
    return 0;
  }
  openFile();
  if (!inpstream_.is_open()) {
    eof_ = true;
    std::cerr << "Could not open " << filelistfn_ << std::endl;
    return 0;
  }

  std::vector<std::string> tokens;
  bool valid = false;
  do {
    std::string line;
    std::getline(inpstream_, line);

    tokens.clear();
    if (line.size()==0 || line[0] == '#') continue;

    std::stringstream ss(line);
    std::string tok;
    while(std::getline(ss, tok, ' ')) {
      if (tok.empty()) continue;
      tokens.push_back(tok);
    }

    if (tokens.size() != 3) {
      MDLog::Log(MDLog::WARN, "Skipping line from filelist, wrong number of tokens found: " + line);
      continue;
    }

    e.originalStartTime = stoi(tokens[0]);
    e.filename = tokens[1];
    e.fsize = stoull(tokens[2]);

    if (e.originalStartTime < latest_timestamp_) {
      MDLog::Log(MDLog::WARN, "Skipping line from filelist, out of order timestamp: " + line);
      continue;
    }

    if (e.originalStartTime < skipuntil_) {
      continue;
    }

    latest_timestamp_ = e.originalStartTime;

    valid = true;

  } while(!valid && !inpstream_.eof());

  if (inpstream_.eof()) {
    eof_ = true;
    closeFile();
  }

  if (valid) {
    return 1;
  }

  return 0;
}

bool FileBasedReader::done() const {
  return eof_;
}

void FileBasedReader::openFile() {
  if (eof_ || inpstream_.is_open()) {
    return;
  }

  inpstream_.open(filelistfn_);
  latest_timestamp_ = 0;
  return;
}

void FileBasedReader::closeFile() {
  if (!inpstream_.is_open()) {
    return;
  }
  inpstream_.close();
  return;
}

void FileBasedReader::finishEarly() {
  closeFile();
  eof_ = true;
}
