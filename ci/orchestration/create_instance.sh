#!/bin/bash

# mockdata registry secret name
mockdatareg_secret='mockdataregsecret'

# By default to not use systemd to manage services inside the containers
usesystemd=0

usage() { cat <<EOF 1>&2
Usage: $0 -n <namespace> [-a <additional_k8_resources>]\
      [-p <gitlab pipeline ID> | -b <build tree base> -B <MockData build tree subdir> \
      [-S]

Options:
  -S    Use systemd to manage services inside containers
  -a    additional kubernetes resources added to the kubernetes namespace
EOF
exit 1
}

die() { echo "$@" 1>&2 ; exit 1; }

while getopts "n:a:p:b:B:S" o; do
    case "${o}" in
	a)
            additional_resources=${OPTARG}
            test -f ${additional_resources} || error="${error}File ${additional_resources} does not exist\n"
            ;;
	n)
            instance=${OPTARG}
            ;;
        p)
            pipelineid=${OPTARG}
            ;;
	b)
            buildtree=${OPTARG}
            ;;
        S)
            usesystemd=1
            ;;
	B)
            mockdatabuildtreesubdir=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${instance}" ]; then
    usage
fi

if [ ! -z "${pipelineid}" -a ! -z "${buildtree}" ]; then
    usage
fi

# everyone needs poddir temporary directory to generate pod yamls
poddir=$(mktemp -d)

if [ -z "${buildtree}" ]; then
     # We are going to run with repository based images (they have rpms embedded)
    COMMITID=$(git log -n1 | grep ^commit | cut -d\  -f2 | sed -e 's/\(........\).*/\1/')
    if [ -z "${pipelineid}" ]; then
      echo "Creating instance for latest image built for ${COMMITID} (highest PIPELINEID)"
      imagetag=$(../ci_helpers/list_images.sh 2>/dev/null | grep ${COMMITID} | sort -n | tail -n1)
    else
      echo "Creating instance for image built on commit ${COMMITID} with gitlab pipeline ID ${pipelineid}"
      imagetag=$(../ci_helpers/list_images.sh 2>/dev/null | grep ${COMMITID} | grep ^${pipelineid}git | sort -n | tail -n1)
    fi
    if [ "${imagetag}" == "" ]; then
      echo "commit:${COMMITID} has no docker image available in gitlab registry, please check pipeline status and registry images available."
      exit 1
    fi
    echo "Creating instance using docker image with tag: ${imagetag}"

    cp pod-* ${poddir}
    sed -i ${poddir}/pod-* -e "s/\(^\s\+image:[^:]\+:\).*/\1${imagetag}/"

    if [ ! -z "${error}" ]; then
        echo -e "ERROR:\n${error}"
        exit 1
    fi
else
    # We need to know the subdir as well
    if [ -z "${mockdatabuildtreesubdir}" ]; then
      usage
    fi
    # buildtree logic
    echo "Build tree not supported"
    exit 1
fi

if [ $usesystemd == 1 ] ; then
    echo "Using systemd to start services on some pods"
    for podname in datasource xcache loadgenerator; do
        sed -i "/^\ *command:/d" ${poddir}/pod-${podname}*.yaml
    done
fi

ns=$(kubectl get namespace ${instance} --no-headers --output=go-template={{.metadata.name}} 2>/dev/null)
if [ -z "${ns}" ]; then
  echo -n "Creating ${instance} instance "
  kubectl create namespace ${instance} || die "FAILED"
else
  echo "Removing any existing, cm, pods, pvc or svc resources from ${instance}"
  kubectl delete --all pods --namespace=${instance}
  kubectl delete --all svc --namespace=${instance}
  kubectl delete --all pvc --namespace=${instance}
  kubectl delete --all cm --namespace=${instance}
  echo "Waiting 60 seconds for delete to complete"
  sleep 60
fi

# The mockdata registry secret must be copied in the instance namespace to be usable
kubectl get secret ${mockdatareg_secret} &> /dev/null
if [ $? -eq 0 ]; then
  echo "Copying ${mockdatareg_secret} secret in ${instance} namespace"
  kubectl get secret ${mockdatareg_secret} -o yaml | grep -v '^ *namespace:' | kubectl --namespace ${instance} create -f -
fi

kubectl --namespace ${instance} create configmap buildtree --from-literal=base=${buildtree} --from-literal=mockdata_subdir=${mockdatabuildtreesubdir}

if [ ! -z "${additional_resources}" ]; then
  kubectl --namespace ${instance} create -f ${additional_resources} || die "Could not create additional resources described in ${additional_resources}"
  kubectl --namespace ${instance} get pods
fi

echo "Requesting an unused log volume"
kubectl create -f ./pvc_logs.yaml --namespace=${instance}

echo "Creating services in instance"

for service_file in *svc\.yaml; do
  kubectl create -f ${service_file} --namespace=${instance}
done

echo "Creating pods in instance"

echo "Launching pods"

for podname in datasource xcache loadgenerator; do
  kubectl create -f ${poddir}/pod-${podname}.yaml --namespace=${instance}
done

echo -n "Waiting for other pods"
for ((i=0; i<240; i++)); do
  echo -n "."
  # exit loop when all pods are in Running state
  kubectl get pods --namespace=${instance} | grep -v init | tail -n+2 | grep -q -v Running || break
  sleep 1
done

if [[ $(kubectl get pods --namespace=${instance} | grep -v init | tail -n+2 | grep -v Running) ]]; then
  echo "TIMED OUT"
  echo "Some pods have not been initialized properly:"
  kubectl get pods --namespace=${instance}
  exit 1
fi
echo OK

echo -n "Waiting for xcache to be Ready"
for ((i=0; i<300; i++)); do
  echo -n "."
  kubectl --namespace=${instance} exec xcache -- bash -c 'test -f /var/log/xrootd/pfc/xrootd.log && grep -q "initialization completed" /var/log/xrootd/pfc/xrootd.log' && break
  sleep 1
done
kubectl --namespace=${instance} exec xcache -- bash -c 'grep -q "initialization completed" /var/log/xrootd/pfc/xrootd.log' || die "TIMED OUT"
echo OK


echo "Instance ${instance} successfully created:"
kubectl get pods --namespace=${instance}

exit 0
