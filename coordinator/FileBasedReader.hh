/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "Reader.hh"

#include <fstream>

class FileBasedReader : public Reader {
public:
  FileBasedReader() : pos_(0), eof_(false) { }
  bool LoadOne(bufferFileEntry &e);
  bool done() const;
  void finishEarly();
  void SetFilename(const std::string &fn) { filelistfn_ = fn; }
  void SetSkipUntil(const time_t ts) { skipuntil_ = ts; }

private:
  void openFile();
  void closeFile();

private:
  off_t pos_;
  bool eof_;
  std::ifstream inpstream_;
  time_t latest_timestamp_;
  std::string filelistfn_;
  time_t skipuntil_;
};
