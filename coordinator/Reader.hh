/*
 * MockData - XRootD test traffic generator
 *
 * (C) Copyright 2019 CERN. This software is distributed under the
 * terms of the GNU General Public Licence version 3 (GPL Version 3),
 * copied verbatim in the file “COPYING”. In applying this licence,
 * CERN does not waive the privileges and immunities granted to it by
 * virtue of its status as an Intergovernmental Organization or submit
 * itself to any jurisdiction.
 *
 * Contact: david.smith@cern.ch         
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <cstdint>
#include <string>
#include <time.h>

struct bufferFileEntry {
  uint64_t id;
  std::string filename;
  uint64_t fsize;
  time_t originalStartTime;
  time_t wallTimeStart;
};

class Reader {
public:
  Reader() { };
  virtual ~Reader() { };
  virtual bool LoadOne(bufferFileEntry &e) = 0;
  virtual void SetFilename(const std::string &fn) = 0;
  virtual void SetSkipUntil(const time_t ts) = 0;
  virtual void finishEarly() = 0;
  virtual bool done() const = 0;
};
