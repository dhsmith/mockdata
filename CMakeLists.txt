#
# MockData - XRootD test traffic generator
#
# (C) Copyright 2019 CERN. This software is distributed under the
# terms of the GNU General Public Licence version 3 (GPL Version 3),
# copied verbatim in the file “COPYING”. In applying this licence,
# CERN does not waive the privileges and immunities granted to it by
# virtue of its status as an Intergovernmental Organization or submit
# itself to any jurisdiction.
#
# Contact: david.smith@cern.ch         
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

cmake_minimum_required (VERSION 2.6)

project(mockdata)

set (BUILD_CMDLINE_ONLY FALSE)

execute_process(
  COMMAND ${CMAKE_C_COMPILER} -dumpversion
  OUTPUT_VARIABLE GCC_VERSION)

if (CMAKE_COMPILER_IS_GNUCC)
  if (GCC_VERSION VERSION_EQUAL 4.4.7)
    message(STATUS "Detected gcc = 4.4.7 - build cmdline only with c++0x")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
    set (BUILD_CMDLINE_ONLY TRUE)
  endif (GCC_VERSION VERSION_EQUAL 4.4.7)
  if (GCC_VERSION VERSION_GREATER 4.4.7)
    message(STATUS "Detected gcc > 4.4.7 - switching on support for c++1y")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y")
  endif (GCC_VERSION VERSION_GREATER 4.4.7)
endif (CMAKE_COMPILER_IS_GNUCC)

set (GCC_VERSION_GE_4_8_0 FALSE)
if (CMAKE_COMPILER_IS_GNUCC AND ((GCC_VERSION VERSION_GREATER 4.8.0) OR (GCC_VERSION VERSION_EQUAL 4.8.0)))
  set (GCC_VERSION_GE_4_8_0 TRUE)
endif (CMAKE_COMPILER_IS_GNUCC AND ((GCC_VERSION VERSION_GREATER 4.8.0) OR (GCC_VERSION VERSION_EQUAL 4.8.0)))

# Generate the compilation variables, if needed
if (NOT DEFINED SKIP_UNIT_TESTS)
  message (STATUS "Setting SKIP_UNIT_TESTS to the value of 0")
  message (STATUS "Override with -DSKIP_UNIT_TESTS:STRING=1")
  set(SKIP_UNIT_TESTS 0)
else (NOT DEFINED SKIP_UNIT_TESTS)
  message (STATUS "Already set: SKIP_UNIT_TESTS=${SKIP_UNIT_TESTS}")
endif (NOT DEFINED SKIP_UNIT_TESTS)

#Load version information in all cases.
include(cmake/MockDataVersions.cmake)

IF(DEFINED PackageOnly)
  message (STATUS "Running CMake in package-only mode")
   set(COMPILE_PACKAGING "1")
ELSE(DEFINED PackageOnly)
  message (STATUS "Running in full configuration mode.")
  message (STATUS "Override with -DPackageOnly:Bool=true")
  # Generate the compilation variables, if needed
  if (NOT DEFINED COMPILE_PACKAGING)
    message (STATUS "Setting COMPILE_PACKAGING to the value of 1")
    message (STATUS "Override with -DCOMPILE_PACKAGING:STRING=0")
    set(COMPILE_PACKAGING 1)
  else (NOT DEFINED COMPILE_PACKAGING)
    message (STATUS "Already set: COMPILE_PACKAGING=${COMPILE_PACKAGING}")
  endif (NOT DEFINED COMPILE_PACKAGING)

  IF(NOT CMAKE_BUILD_TYPE STREQUAL "")
    # If the user specifies -DCMAKE_BUILD_TYPE on the command line, take their definition and dump it in the cache
    message(STATUS "Setting build type to ${CMAKE_BUILD_TYPE} as requested.")
    SET(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE} CACHE STRING "Choose the type of build.")
  ELSE()
    # log choosen default (RelWithDebInfo) and set it
    message(STATUS "Setting build type to 'RelWithDebInfo' as none was specified.")
    message (STATUS "Override with -DCMAKE_BUILD_TYPE:STRING=Debug")
    set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose the type of build." FORCE)
    # Set the possible values of build type for cmake-gui
    # this command is not yet available in SLC6's cmake 2.6
    # set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
  ENDIF(NOT CMAKE_BUILD_TYPE STREQUAL "")

  set(CMAKE_DISABLE_SOURCE_CHANGES ON)
  set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
  list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
  include(GNUInstallDirs)
  set(CMAKE_INSTALL_PREFIX "/")

  include_directories(${PROJECT_SOURCE_DIR})
  include_directories(${CMAKE_BINARY_DIR})

   find_package(XROOTD REQUIRED)

   find_package(YamlCpp REQUIRED)

   add_subdirectory(third_party/grpc ${CMAKE_CURRENT_BINARY_DIR}/grpc EXCLUDE_FROM_ALL)
   message(STATUS "Using gRPC via add_subdirectory.")
   # After using add_subdirectory, we can now use the grpc targets directly from
   # this build.

   add_subdirectory (common)
   add_subdirectory (oss)
   add_subdirectory (coordinator)
   add_subdirectory (loadgenerator)
   add_subdirectory (randomreader)

  #Generate version information
  configure_file(${PROJECT_SOURCE_DIR}/version.hpp.in
    ${CMAKE_BINARY_DIR}/version.h)
ENDIF(DEFINED PackageOnly)

################################################################################
# Packaging step (replacing the maketar)
# See http://www.vtk.org/Wiki/CMakeUserUseRPMTools
################################################################################
if (${COMPILE_PACKAGING} STREQUAL "1")
  include(cmake/UseRPMToolsEnvironment.cmake)
  set(CPACK_SOURCE_PACKAGE_FILE_NAME
   "${PROJECT_NAME}-${MOCKDATA_VERSION}-${MOCKDATA_RELEASE}")
  message (STATUS
    "Setting package file name to: ${CPACK_SOURCE_PACKAGE_FILE_NAME}")
  set(CPACK_SOURCE_IGNORE_FILES "/.git/")
  include(CPack)
  include(cmake/UseRPMTools.cmake)
  if (RPMTools_FOUND)
    RPMTools_ADD_RPM_TARGETS(
      ${PROJECT_NAME} ${PROJECT_NAME}.spec.in)
  endif (RPMTools_FOUND)
endif (${COMPILE_PACKAGING} STREQUAL "1")
