#!/bin/bash

usage() { cat <<EOF 1>&2
Usage: $0 -n <namespace>
EOF
exit 1
}

while getopts "n:" o; do
    case "${o}" in
        n)
            NAMESPACE=${OPTARG}
            ;;
	*)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${NAMESPACE}" ]; then
    usage
fi

if [ ! -z "${error}" ]; then
    echo -e "ERROR:\n${error}"
    exit 1
fi

echo "Preparing namespace for the tests"
./prepare_tests.sh -n ${NAMESPACE}

echo
echo "Launching loadgenerator_t1.sh on loadgenerator pod"
kubectl -n ${NAMESPACE} cp loadgenerator_t1.sh loadgenerator:/root/loadgenerator_t1.sh
kubectl -n ${NAMESPACE} exec loadgenerator -- bash /root/loadgenerator_t1.sh || exit 1

exit 0
